![](/assets/teaser.jpg)


# K-surfaces: Bézier-Splines that Interpolate at Gaussian Curvature Extrema
Source code for the [K-surfaces paper](https://dl.acm.org/doi/10.1145/3618383).

The source code is divided in a training and evaluation part of the networks with python (see `src/`) and an interactive visualization tool build with c++ (see `src_visual_tool/`).
We have tested the code for the visualization tool on linux and windows. Mac users could get some troubles with the CUDA version of LibTorch.

  
## The visualization tool
The visualization tool provides an interactive interface to evaluate the surface models. By default, all torchscript modules in the `trained_models/` folder are available in the tool.
We provide the following models:

| Model name | Description |
| ----------- | ----------- |
| K-Surface.pt | The final model of the paper. |
| BezierModule_MxN.pt | These are single Bézier patches with a control grid of MxN. | 
| BezierSpline_interpolating.pt | This is a Bézier Spline interpolating at (0.5,0.5). | 


### Build the visualization tool
The visualization tool should be build using cmake and depends mainly on [libigl](https://libigl.github.io/) and [LibTorch](https://pytorch.org/). The required version of libigl (and Eigen) is automatically fechted during the cmake build.
LibTorch for CUDA 11.3 can be downloaded from [pytorch.org](https://pytorch.org/) (we used Version 1.11.0+cu113). Please refer to [Installing C++ Distributions of PyTorch](https://pytorch.org/cppdocs/installing.html) to make LibTorch available to cmake. You may also need to install [NVIDIA cuDNN](https://developer.nvidia.com/cudnn) for the LibTorch CUDA version.


## Training and data generation with python
In `src` you find the code to train your own models. This project contains several python packages. The most important are:
- shapes - sampling and methods for curves, surfaces, uv coordinates etc.
- network - pytorch networks, datasets which use the sampler to generate train/evaluation data, surface/curve data transformations, a trainer class, etc.

### Install the python dependencies
All python dependencies can be installed with anaconda:
```
conda env update -n k_surfaces --file environment.yaml
conda activate k_surfaces
```
This will create a conda environment `k_surfaces` and install torch, numpy, matplotlib, tensorboard, etc.

### Train your own models

In the `main.py`, these packages are used to create a training configuration (for a definition of the config see `global_config.py`) and to train a model. For training, update the `config` object in `main_train.py` and execute `python ./src/main_train.py`.

There is no need to generate your own training data as a training dataset is automatically downloaded if you start the training process. If you do want to create your own training data, then update the configuration in the `main_data_gen.py` script and execute `python ./src/main_data_gen.py`.


### tensorboard

The Trainer class supports tensorboard and stores the tensorboard files into `runs/`. The command to start tensorboard is:
```
tensorboard --logdir runs/
```


### How to export torchscript models for the visualization tool
This is only required if you train your own model or make changes to existing models. To make an pytorch model available in the visualization tool, it must be exported as a [torchscript](https://pytorch.org/docs/stable/jit.html) module and placed in the `trained_models/` folder. See `utils.other.save_model ` and `networks.layers.VisToolModule` for all requirements to the pytorch model. 

## Cite this project
If you use this project or the ideas from the paper please cite
```
@article{k_surfaces,
author = {Djuren, Tobias and Kohlbrenner, Maximilian and Alexa, Marc},
title = {K-Surfaces: B\'{e}zier-Splines Interpolating at Gaussian Curvature Extrema},
year = {2023},
issue_date = {December 2023},
publisher = {Association for Computing Machinery},
address = {New York, NY, USA},
volume = {42},
number = {6},
issn = {0730-0301},
url = {https://doi.org/10.1145/3618383},
doi = {10.1145/3618383},
month = {dec},
articleno = {210},
numpages = {13},
keywords = {b\'{e}zier patches, interactive surface modeling, b\'{e}zier splines, gaussian curvature}
}
```

## Todos
- [ ] Improve the installation process
- [ ] Release the Schelling model
