import torch
from shapes.other.Grids import PointLine2D

class CurveBase():
  '''Abstract base for all curves.'''

  def __init__(self, ctrl_len, t_sampling="uniform", ctrl_settings={"normal": 0.2}, ctrl_prob=None, device="cpu") -> None:
    '''
    Input:
      - ctrl_len - number of control points
      - t_sampling - "uniform" for equally spaced t parameter sampling or "rand" for random t samples
      - ctrl_settings - settings to generate control points. See shapes.other.PointLine2D. This can also be a list of settings
        from which a random choice is drawn for each generated grid.
      - ctrl_prob - list of probabilities for the random choice of ctrl_settings. None for equal probabilities.
    '''
    assert t_sampling in ['uniform', 'rand'], f"t sampling of type '{t_sampling}' is not supported"
    self.ctrl_len = ctrl_len
    self.t_sampling = t_sampling
    self.device = device

    # grid sampling properties
    if type(ctrl_settings) != list:
      self.ctrl_settings = [ctrl_settings]
    else:
      self.ctrl_settings = ctrl_settings
    self.ctrl_samplers = []
    for i in range(len(self.ctrl_settings)):
      ctrl_sampler = PointLine2D(ctrl_len, settings=self.ctrl_settings[i], device=device)
      self.ctrl_samplers.append(ctrl_sampler)
    if ctrl_prob == None:
      self.ctrl_prob = torch.full((len(self.ctrl_samplers),), 1./len(self.ctrl_samplers))
    else:
      self.ctrl_prob = torch.tensor(ctrl_prob)