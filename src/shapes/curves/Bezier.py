import torch
from .Polynom import Polynom
from ..util import cubic_roots


class Bezier(Polynom):
  '''Bezier curve algorithms.'''
  def __init__(self, ctrl_len, t_sampling="uniform", ctrl_settings={ "normal": 0.2 }, ctrl_prob=None, device="cpu") -> None:
    super().__init__(ctrl_len, t_sampling=t_sampling, ctrl_settings=ctrl_settings, ctrl_prob=ctrl_prob, device=device)
    assert ctrl_len in [2, 3, 4, 5, 6], f"BezierCurve of degree {ctrl_len} not implemented"


  @staticmethod
  def basis_matrix(ctrl_len: int, device="cpu") -> torch.Tensor:
    return basis_matrix(ctrl_len, device=device)


  @staticmethod
  def max_curvature(ctrl_pts: torch.Tensor, params=False, eps=1e-10) -> torch.Tensor:
    return max_curvature(ctrl_pts, params=params, eps=eps)


  @staticmethod
  def target_max_curvature(ctrl_pts: torch.Tensor, target: torch.Tensor = None) -> torch.Tensor:
    return target_max_curvature(ctrl_pts, target = target)


  @staticmethod
  def connect(first: torch.Tensor, second: torch.Tensor, inplace=True, eps=1e-10):
    return connect(first, second, inplace=inplace, eps=eps)




#############
## functional
#############

def basis_matrix(ctrl_len: int, device="cpu") -> torch.Tensor:
  '''Bernstein matrix of shape (ctrl_len x ctrl_len). Coefficients of the Bernstein polynomials.'''
  if ctrl_len == 2:
    basis_matrix = torch.tensor([
      [1., -1.],
      [0., 1.]
    ], device=device)

  elif ctrl_len == 3:
    basis_matrix = torch.tensor([
      [1., -2., 1.],
      [0., 2., -2.],
      [0., 0., 1.]
    ], device=device)

  elif ctrl_len == 4:
    basis_matrix = torch.tensor([
      [1., -3., 3., -1.],
      [0., 3., -6., 3.],
      [0., 0., 3., -3.],
      [0., 0., 0., 1.]
    ], device=device)

  elif ctrl_len == 5:
    basis_matrix = torch.tensor([
      [1., -4., 6., -4., 1.],
      [0., 4., -12., 12., -4.],
      [0., 0., 6., -12., 6.],
      [0., 0., 0., 4., -4.],
      [0., 0., 0., 0., 1.]
    ], device=device)

  elif ctrl_len == 6:
    basis_matrix = torch.tensor([
      [1., -5., 10., -10., 5., -1.],
      [0., 5., -20., 30., -20., 5.],
      [0., 0., 10., -30., 30., -10.],
      [0., 0., 0., 10., -20., 10.],
      [0., 0., 0., 0., 5., -5.],
      [0., 0., 0., 0., 0., 1.]
    ], device=device)

  else:
    raise NotImplementedError
  return basis_matrix


def max_curvature(ctrl_pts: torch.Tensor, params=False, eps=1e-10) -> torch.Tensor:
  '''
  Calculates the points with local maximum curvature magnitude for given curve ctrl_pts.
  See Yan et al. 2017 - kappa-Curves.
  Only curves of degree 3 are supported.\\
  in:
    - ctrl_pts - control points tensor (curves to evaluate x 3 (==ctrl_len) x 3)
    - params - whether to return the parameter values instead of the curve points
    - eps - numerical stability constant

  out:
    - curve points tensor of shape (curves to evaluate x 3) or parameter tensor (curves to evaluate) if params is set
  '''
  assert ctrl_pts.shape[1] == 3, "max_curvature: Only Bezier curves with degree 3 are supported."
  c0, c1, c2 = ctrl_pts[:, 0], ctrl_pts[:, 1], ctrl_pts[:, 2]
  num = torch.einsum("ni,ni->n", c0-c1, c0 - 2*c1 + c2)
  de_num = torch.pow(c0 - 2*c1 + c2, 2).sum(dim=1)
  t = num / (de_num + eps)
  if params:
    return t

  pts = eval(ctrl_pts, t.unsqueeze(dim=1))
  return pts.squeeze(dim=1)


def target_max_curvature(ctrl_pts: torch.Tensor, target: torch.Tensor = None) -> torch.Tensor:
  '''
  Adjusts the mid ctrl point of the given ctrl_pts so that the curve interpolates a curvature extrema at target location.
  3D version of Yan et al. 2017 - kappa-Curves
  Only curves of degree 3 are supported.\\
  in:
    - ctrl_pts - control points tensor (curves to evaluate x 3 (==ctrl_len) x 3)
    - target - where the curvature extrema should be (curves to evaluate x 3). If not set, then ctrl_pts[:,1] is used as target.
  
  out:
    - modified ctrl_pts tensor (same shape as input)
  '''
  ctrl_pts.shape[1] == 3, "other ctrl_len than 3 not supported yet"
  c0, c2 = ctrl_pts[:, 0], ctrl_pts[:, 2]
  if target:
    pt = target
  else:
    pt = ctrl_pts[:, 1]
  a = torch.linalg.norm(c2-c0, dim=1)**2
  b = torch.einsum("ni,ni->n", 3*(c2-c0), c0-pt)
  c = torch.einsum("ni,ni->n", 3*c0 -2*pt -c2, c0-pt)
  d = -torch.linalg.norm(c0-pt, dim=1)**2

  t = cubic_roots(a, b, c, d)
  t = torch.gather(t, 1, (t-0.5).abs().min(dim=1).indices.unsqueeze(dim=1))

  new_c1 = (pt - (1-t)**2 *c0 - t**2*c2) / (2*t*(1-t))
  return new_c1


def connect(first: torch.Tensor, second: torch.Tensor, inplace=True, eps=1e-10) -> torch.Tensor:
  '''
  Connects two bezier curves of degree 3 by repositioning the last ctrl point of the first curve
  and the first ctrl point of the second curve. The construction is (magnitude) G2 continuous.

  in:
    - first - ctrl pts of the first curve (n x 3 (=ctrl_len) x 3)
    - second - ctrl pts of the second curve (n x 3 (=ctrl_len) x 3)
    - inplace - modifies the first and second curves
    - eps - numerical stability constant

  out:
    - last ctrl pt from the first (== first ctrl pt from the last) tensor of shape (n x 3)
  '''
  assert first.shape[1] == 3, "other ctrl_len than 3 not supported yet"
  assert second.shape[1] == 3, "other ctrl_len than 3 not supported yet"
  f0, f1 = first[:, 0], first[:, 1]
  s1, s2 = second[:,1], second[:,2]
  tria0 = torch.linalg.norm(torch.cross(f1-f0, f1-s1), dim=1).sqrt()
  tria1 = torch.linalg.norm(torch.cross(s1-f1, s1-s2), dim=1).sqrt()
  t = tria0 / (tria0 + tria1 + eps)
  pt = (1 - t) * f1 + t * s1

  if inplace:
    first[:,2] = pt
    second[:,0] = pt
  return pt

