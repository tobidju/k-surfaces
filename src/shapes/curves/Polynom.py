import torch
from .CurveBase import CurveBase

class Polynom(CurveBase):
  '''Base for all polynomial curves.'''
  def __init__(self, ctrl_len, t_sampling="uniform", ctrl_settings={ "normal": 0.2 }, ctrl_prob=None, device="cpu") -> None:
    super().__init__(ctrl_len, t_sampling, ctrl_settings, ctrl_prob, device)


  def sample(self, number, samples, ctrl_pts=None):
    '''
    Generates #number curves each with #samples samples\\
    If ctrl_pts are set, then the curve samples are generated using the ctrl_pts tensor.\\
    out:
      - control point tensor (number x ctrl_len x 3) for #number curves
      - t samples (number x samples) for #number curves
      - curve points at t samples (number x samples x 3) for each curve
    '''
    if ctrl_pts == None:
      ctrl_pts_list = []
      choices = torch.multinomial(self.ctrl_prob, number, replacement=True)
      n_choices = torch.bincount(choices)
      for i, n in enumerate(n_choices):
        if n == 0:
          continue
        _ctrl_pts = self.ctrl_samplers[i].sample(n)
        ctrl_pts_list.append(_ctrl_pts)
      _ctrl_pts = torch.cat(ctrl_pts_list)
    else:
      _ctrl_pts = ctrl_pts.to(self.device)

    if self.t_sampling == "uniform":
      t = torch.linspace(0, 1, samples, device=self.device)
      t = t.unsqueeze(dim=0).expand(number, -1)
    else:
      t = torch.rand((number, samples), device=self.device)

    b = self.basis(self.ctrl_len, t)
    curv_points = torch.einsum('cnu,cud->cnd', b, _ctrl_pts)
    return _ctrl_pts, t, curv_points


  def basis(self, t: torch.Tensor, derivative=0) -> torch.Tensor:
    bmat = self.basis_matrix(self.ctrl_len, self.device)
    return basis(self.ctrl_len, t, bmat, derivative=derivative)


  def monomials(self, t: torch.Tensor, derivative=0) -> torch.Tensor:
    return monomials(self.ctrl_len, t, derivative=derivative)


  @staticmethod
  def basis_matrix(ctrl_len: int, device="cpu") -> torch.Tensor:
    '''
    Implement this for derived classes (e.g. Beziers or Catmull-Roms)!
    This should return a matrix of shape (ctrl_len x ctrl_len) with coefficients of the basis polynomials.
    '''
    return basis_matrix(ctrl_len, device=device)


  @classmethod
  def eval(cls, ctrl_pts: torch.Tensor, t: torch.Tensor) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_pts.size(1), ctrl_pts.device)
    return eval(ctrl_pts, t, bmat)


  @classmethod
  def normal(cls, ctrl_pts: torch.Tensor, t: torch.Tensor) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_pts.size(1), ctrl_pts.device)
    return normal(ctrl_pts, t, bmat)


  @classmethod
  def curvature(cls, ctrl_pts: torch.Tensor, t: torch.Tensor) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_pts.size(1), ctrl_pts.device)
    return curvature(ctrl_pts, t, bmat)




#############
## functional
#############

def eval(ctrl_pts: torch.Tensor, t: torch.Tensor, bmat) -> torch.Tensor:
  '''
  Evaluates all curves from ctrl_pts for given ts.\\
  in:
    - ctrl_pts - control points tensor (curves to evaluate x ctrl_len x 3)
    - t - t parameter tensor (n) if us are equal for each curve or (curves to evaluate x n)\\
  out:
    - curve points tensor of shape (curves to evaluate x n x 3)
  '''
  ctrl_len = ctrl_pts.size(1)
  bs = basis(ctrl_len, t, bmat)

  if t.dim() == 1:
    curv_pts = torch.einsum("nu,cud->cnd", bs, ctrl_pts)
  else:
    curv_pts = torch.einsum("cnu,cud->cnd", bs, ctrl_pts)
  return curv_pts


def basis(ctrl_len: int, t: torch.Tensor, bmat, derivative=0) -> torch.Tensor:
  '''
  Bernstein basis at positions t. Or the n derivative of the bernstein basis at t if derivative = n.
  t can be of arbitrary dimension. The output will have dimension of t +1 with the basis values in the last dimension.
  '''
  monoms = monomials(ctrl_len, t, derivative=derivative)
  b = torch.einsum("...v,uv->...u", monoms, bmat)
  return b


def monomials(ctrl_len: int, t: torch.Tensor, derivative=0) -> torch.Tensor:
  '''
  Monomials up to degree ctrl_length-1 evaluated at t, e.g. [[1, t, t**2, t***3], ...].
  Or their derivatives if derivative >0.
  t can be of arbitrary dimension. The output will have dimension of t +1 with the monomial values in the last dimension.
  '''
  device = t.device
  t_basis = t.unsqueeze(dim=-1).expand([*t.shape] + [ctrl_len])
  t_exp = torch.cat([torch.zeros(derivative, device=device), torch.arange(0, ctrl_len-derivative, device=device)])
  
  monoms = torch.pow(t_basis, t_exp)
  for d in range(derivative):
    monoms = monoms * (torch.arange(0,ctrl_len, device=device) - d)
  return monoms


def basis_matrix(ctrl_len: int, device="cpu") -> torch.Tensor:
  '''
  Identity. Each Ctrl point is the coefficient for a single monomial.
  '''
  return torch.eye(ctrl_len, device=device)


def normal(ctrl_pts: torch.Tensor, t: torch.Tensor, bmat) -> torch.Tensor:
  '''
  Calculates the normal (direction of acceleration) for given curves and t locations.
  in:
    - ctrl_pts - control points tensor (curves to evaluate x ctrl_len x 3)
    - t - t locations of n normals along the curves (curves to evaluate x n)\\
  out:
    - normal vector tensor of shape (curves to evaluate x n x 3)
  '''
  ctrl_len = ctrl_pts.size(1)
  b_r_t = basis(ctrl_len, t, bmat, derivative=1)
  b_r_tt = basis(ctrl_len, t, bmat, derivative=2)
  r_t = torch.einsum("cnu,cud->cnd", b_r_t, ctrl_pts)
  r_tt = torch.einsum("cnu,cud->cnd", b_r_tt, ctrl_pts)
  cr = torch.linalg.cross(r_tt, r_t)
  num = torch.linalg.cross(r_t, cr)
  de_num = torch.linalg.norm(r_t, dim=-1) * torch.linalg.norm(cr, dim=-1)
  norm = num/de_num.unsqueeze(dim=-1) # TODO devision by zero
  return norm


def curvature(ctrl_pts: torch.Tensor, t: torch.Tensor, bmat) -> torch.Tensor:
  '''
  Calculates curvature for given curves at n locations for parameter t.
  in:
    - ctrl_pts - control points tensor (curves to evaluate x ctrl_len x 3)
    - t - locations at which the curvature is to be calculated (curves to evaluate x n) or (n) if ts are equal for all curves\\
  out:
    - curvature values (curves to evaluate x n)
  '''
  ctrl_len = ctrl_pts.size(1)
  b_r_t = basis(ctrl_len, t, bmat, derivative=1)
  b_r_tt = basis(ctrl_len, t, bmat, derivative=2)
  if t.dim() == 1:
    r_t = torch.einsum("nu,cud->cnd", b_r_t, ctrl_pts)
    r_tt = torch.einsum("nu,cud->cnd", b_r_tt, ctrl_pts)
  else:
    r_t = torch.einsum("cnu,cud->cnd", b_r_t, ctrl_pts) #TODO Debug
    r_tt = torch.einsum("cnu,cud->cnd", b_r_tt, ctrl_pts)
  num = torch.linalg.norm(torch.linalg.cross(r_t, r_tt), dim=-1)
  de_num = torch.linalg.norm(r_t, dim=-1).pow(3)
  return num / de_num #TODO devision by zero
