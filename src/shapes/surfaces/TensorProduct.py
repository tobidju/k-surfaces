import torch
from shapes.other.Coordinates import Coords
from shapes.other.Grids import Grid

class TensorProduct():
  '''
  Abstract base class for polynomial tensor product surfaces.
  '''
  def __init__(self, ctrl_len: int, uv_sampling="grid", ctrl_settings={"normal": 0.2}, ctrl_prob=None, device="cpu") -> None:
    '''
    Input:
      - ctrl_len - length of control points
      - uv_sampling - "rand" for random uv samples or "grid" for uv grid samples
      - ctrl_settings - settings to generate control grids. See shapes.other.Grids. This can also be a list of settings
        from which a random choice is drawn for each generated grid.
      - ctrl_prob - list of probabilities for the random choice of ctrl_settings. None for equal probabilities.
    '''
    assert uv_sampling in ['grid', 'rand'], f"uv sampling of type '{uv_sampling}' is not supported"
    self.ctrl_len = ctrl_len
    self.uv_sampler = Coords(0, 1, device)
    self.uv_sampling = uv_sampling

    self.device = device

    # grid sampling properties
    if type(ctrl_settings) != list:
      self.ctrl_settings = [ctrl_settings]
    else:
      self.ctrl_settings = ctrl_settings
    self.ctrl_samplers = []
    for i in range(len(self.ctrl_settings)):
      ctrl_sampler = Grid(ctrl_len, settings=self.ctrl_settings[i], device=device)
      self.ctrl_samplers.append(ctrl_sampler)
    if ctrl_prob == None:
      self.ctrl_prob = torch.full((len(self.ctrl_samplers),), 1./len(self.ctrl_samplers))
    else:
      self.ctrl_prob = torch.tensor(ctrl_prob)


  def sample(self, number: int, sample_length: int, ctrl_grids: torch.Tensor=None) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
    '''
    Generates #number surfaces with (sample_length**2) uv samples each \\
    If control_grids are set, then the surface samples are generated using the control_grids tensor.

    out:
      - control point tensor (number x ctrl_len x ctrl_len x 3) for #number surfaces
      - uv points (number x (sample_length**2) x 2) for #number surfaces
      - surface points at uv coordinates (number x (sample_length**2) x 3) for each surface
    '''
    if ctrl_grids == None:
      ctrl_grids_list = []
      choices = torch.multinomial(self.ctrl_prob, number, replacement=True) # calc how many grids from each sampler
      n_choices = torch.bincount(choices)
      for i, n in enumerate(n_choices):
        if n == 0:
          continue
        ctrl_grids = self.ctrl_samplers[i].sample(n)
        ctrl_grids_list.append(ctrl_grids)
      ctrl_grids = torch.cat(ctrl_grids_list)

      if len(self.ctrl_samplers) > 1: # shuffle grids from different samplers
        ctrl_grids = ctrl_grids[torch.randperm(number, device=self.device)]
    else:
      ctrl_grids = ctrl_grids.to(self.device)

    if self.uv_sampling == "grid":
      uv_pts = self.uv_sampler.uniformGrid(sample_length)
      uv_pts = uv_pts.unsqueeze(dim=0).expand(number, -1, -1)
    else:
      uv_pts = self.uv_sampler.uniformSample(sample_length**2 * number)
      uv_pts = uv_pts.view(number, sample_length**2, 2)

    basis_u = self.basis(uv_pts[...,0])
    basis_v = self.basis(uv_pts[...,1])
    surf_points = torch.einsum('snu,suvd,snv->snd', basis_u, ctrl_grids, basis_v)

    return ctrl_grids, uv_pts, surf_points


  def basis(self, t: torch.Tensor, derivative=0) -> torch.Tensor:
    bmat = self.basis_matrix(self.ctrl_len, self.device)
    return basis(self.ctrl_len, t, bmat, derivative=derivative)


  def basis_uv(self, uv: torch.Tensor, d_u=0, d_v=0) -> torch.Tensor:
    bmat = self.basis_matrix(self.ctrl_len, self.device)
    return basis_uv(self.ctrl_len, uv, bmat, d_u=d_u, d_v=d_v)


  def monomials(self, t: torch.Tensor, derivative=0) -> torch.Tensor:
    return monomials(self.ctrl_len, t, derivative=derivative)


  @staticmethod
  def basis_matrix(ctrl_len: int, device="cpu") -> torch.Tensor:
    '''
    Implement this for derived classes!
    This should return a matrix of shape (ctrl_len x ctrl_len) with
    coefficients of the tensor product basis polynomials.
    '''
    raise NotImplementedError


  @classmethod
  def eval(cls, ctrl_grids: torch.Tensor, uv: torch.Tensor) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_grids.size(1), ctrl_grids.device)
    return eval(ctrl_grids, uv, bmat)

  
  @classmethod
  def jacobian(cls, ctrl_grids: torch.Tensor, uv: torch.Tensor) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_grids.size(1), ctrl_grids.device)
    return jacobian(ctrl_grids, uv, bmat)


  @classmethod
  def hessian(cls, ctrl_grids: torch.Tensor, uv: torch.Tensor) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_grids.size(1), ctrl_grids.device)
    return hessian(ctrl_grids, uv, bmat)


  @classmethod
  def normal(cls, ctrl_grids: torch.Tensor, uv: torch.Tensor, normalized=True, eps=1e-10) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_grids.size(1), ctrl_grids.device)
    return normal(ctrl_grids, uv, bmat, normalized=normalized, eps=eps)


  @classmethod
  def fundamental_I(cls, ctrl_grids: torch.Tensor, uv: torch.Tensor) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
    bmat = cls.basis_matrix(ctrl_grids.size(1), ctrl_grids.device)
    return fundamental_I(ctrl_grids, uv, bmat)

  
  @classmethod
  def fundamental_II(cls, ctrl_grids: torch.Tensor, uv: torch.Tensor, normalized=True, eps=1e-10) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
    bmat = cls.basis_matrix(ctrl_grids.size(1), ctrl_grids.device)
    return fundamental_II(ctrl_grids, uv, bmat, normalized=normalized, eps=eps)


  @classmethod
  def gaussian_curvature(cls, ctrl_grids: torch.Tensor, uv: torch.Tensor, eps=1e-10) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_grids.size(1), ctrl_grids.device)
    return gaussian_curvature(ctrl_grids, uv, bmat, eps=eps)


  @classmethod
  def principal_curvature(cls, ctrl_grids: torch.Tensor, uv: torch.Tensor, eps=1e-6) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_grids.size(1), ctrl_grids.device)
    return principal_curvature(ctrl_grids, uv, bmat, eps=eps)


  @classmethod
  def mean_curvature(cls, ctrl_grids: torch.Tensor, uv: torch.Tensor, eps=1e-10) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_grids.size(1), ctrl_grids.device)
    return mean_curvature(ctrl_grids, uv, bmat, eps=eps)


  @classmethod
  def d_normal(cls, ctrl_grids: torch.Tensor, uv: torch.Tensor, eps=1e-10) -> torch.Tensor:
    bmat = cls.basis_matrix(ctrl_grids.size(1), ctrl_grids.device)
    return d_normal(ctrl_grids, uv, bmat, eps=eps)




#############
## functional
#############

def basis(ctrl_len: int, t: torch.Tensor, bmat, derivative=0) -> torch.Tensor:
  '''
  Basis at positions t. Or the n derivative of the basis at t if derivative = n.
  t can be of arbitrary dimension. The output will have dimension of t +1 with the basis values in the last dimension.

  in:
    - ctrl_len
    - t values of shape (... x 1)
  
  out:
    - basis values at t of shape (... x ctrl_len)
  '''
  monoms = monomials(ctrl_len, t, derivative=derivative)
  b = torch.einsum("...v,uv->...u", monoms, bmat)
  return b


def basis_uv(ctrl_len: int, uv: torch.Tensor, bmat, d_u=0, d_v=0) -> torch.Tensor:
  '''
  Matrix filled with basis functions for given uv (... x 2). This is like basis(), but for u and v dimensions instead of t.
  
  in:
    - uv values of shape (... x 2)
    - d_u - derivative in u
    - d_v - derivative in v
  
  out:
    - basis values at t of shape (... x ctrl_len x ctrl_len)
  '''
  basis_u = basis(ctrl_len, uv[:,0], bmat, derivative=d_u)
  basis_v = basis(ctrl_len, uv[:,1], bmat, derivative=d_v)
  bases_uv = torch.einsum("nu,nv->nuv", basis_u, basis_v)
  return bases_uv


def monomials(ctrl_len: int, t: torch.Tensor, derivative=0) -> torch.Tensor:
  '''
  Monomials up to degree ctrl_len-1 evaluated at t, e.g. [[1, t, t**2, t***3], ...].
  Or their derivatives if derivative >0.
  t can be of arbitrary dimension. The output will have dimension of t +1 with the monomial values in the last dimension.
  '''
  device = t.device
  t_basis = t.unsqueeze(dim=-1).expand([*t.shape] + [ctrl_len])
  t_exp = torch.cat([torch.zeros(derivative, device=device), torch.arange(0, ctrl_len-derivative, device=device)])
  
  monoms = torch.pow(t_basis, t_exp)
  for d in range(derivative):
    monoms = monoms * (torch.arange(0, ctrl_len, device=device) - d)
  return monoms


def jacobian(ctrl_grids: torch.Tensor, uv: torch.Tensor, bmat) -> torch.Tensor:
  '''
  Jacobians evaluated at uv and all ctrl_grids. These are the tangent vectors in u and v.

  in:
    - ctrl_grids - control grid tensor (s x ctrl_len x ctrl_len x 3)
    - uv - uv coordinate tensor (s x n x 2)
  
  out:
    - Jacobians tensor of shape (s x n x 3 x 2)
  '''
  ctrl_len = ctrl_grids.size(1)
  basis_u = basis(ctrl_len, uv[...,0], bmat)
  basis_v = basis(ctrl_len, uv[...,1], bmat)
  b_grad_u = basis(ctrl_len, uv[...,0], bmat, derivative=1)
  b_grad_v = basis(ctrl_len, uv[...,1], bmat, derivative=1)

  grad_u = torch.einsum("snu,suvd,snv->snd", b_grad_u, ctrl_grids, basis_v)
  grad_v = torch.einsum("snu,suvd,snv->snd", basis_u, ctrl_grids, b_grad_v)
  return torch.stack([grad_u, grad_v], dim=3)


def hessian(ctrl_grids: torch.Tensor, uv: torch.Tensor, bmat) -> torch.Tensor:
  '''
  Hessian tensors evaluated at uv and all ctrl_grids.

  in:
    - ctrl_grids - s control grids (s x ctrl_len x ctrl_len x 3)
    - uv - n uv coordinates for s surfaces (s x n x 2)
  
  out:
    - Hessian tensor of shape (s x n x 3 x 2 x 2)
  '''
  ctrl_len = ctrl_grids.size(1)
  basis_u = basis(ctrl_len, uv[...,0], bmat)
  basis_v = basis(ctrl_len, uv[...,1], bmat)
  b_r_u = basis(ctrl_len, uv[...,0], bmat, derivative=1)
  b_r_v = basis(ctrl_len, uv[...,1], bmat, derivative=1)
  b_r_vv = basis(ctrl_len, uv[...,1], bmat, derivative=2)
  b_r_uu = basis(ctrl_len, uv[...,1], bmat, derivative=2)

  r_uu = torch.einsum("snu,suvd,snv->snd", b_r_uu, ctrl_grids, basis_v)
  r_vv = torch.einsum("snu,suvd,snv->snd", basis_u, ctrl_grids, b_r_vv)
  r_uv = torch.einsum("snu,suvd,snv->snd", b_r_u, ctrl_grids, b_r_v)

  hes = torch.stack([r_uu, r_uv, r_uv, r_vv], dim=2)
  return hes.view(-1, uv.shape[1], 2, 2, 3).permute((0,1,4,2,3)) # TODO not suitable for tracing


def normal(ctrl_grids: torch.Tensor, uv: torch.Tensor, bmat, normalized=True, eps=1e-10) -> torch.Tensor:
  '''
  Computes the normal for all control_grids at uv positions.
  (Normals can be inaccurate at the corners of a bezier patch because the u and v tangents can be colinear.)

  in:
    - ctrl_grids - control grid tensor (s x ctrl_len x ctrl_len x 3)
    - uv - uv coordinate tensor (s x n x 2)
  
  out:
    - normals of shape (s x n x 3)
  '''
  jacobians = jacobian(ctrl_grids, uv, bmat)
  norm = torch.cross(jacobians[...,0], jacobians[...,1], dim=2)
  if normalized:
    norm = norm / (torch.linalg.norm(norm, dim=2).unsqueeze(dim=2) + eps)
  return norm


def fundamental_I(ctrl_grids: torch.Tensor, uv: torch.Tensor, bmat) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
  '''
  Returns the coefficients E, F, G of the first fundamental form evaluated for all ctrl_grids at uv.

  in:
    - ctrl_grids - control grid tensor (s x ctrl_len x ctrl_len x 3)
    - uv - uv coordinate tensor (n x 2)
  
  out:
    - coefficient tuple (E, F, G). Each with shape (s, n)
  '''
  jacobians = jacobian(ctrl_grids, uv, bmat)
  EG = (jacobians**2).sum(dim=2)
  F = torch.einsum("snd,snd->sn", jacobians[...,0], jacobians[...,1])
  return EG[...,0], F, EG[...,1]


def fundamental_II(ctrl_grids: torch.Tensor, uv: torch.Tensor, bmat, normalized=True, eps=1e-10) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
  '''
  Returns the coefficients L, N, M of the second fundamental form evaluated for all ctrl_grids at uv.

  in:
    - ctrl_grids - control grid tensor (s x ctrl_len x ctrl_len x 3)
    - uv - uv coordinate tensor (s x n x 2)
    - normalized - whether to use normals with unit length
  
  out:
    - coefficient tuple (L, N, M). Each with shape (s, n)
  '''
  ctrl_len = ctrl_grids.size(1)
  normals = normal(ctrl_grids, uv, bmat, normalized=normalized, eps=eps)

  basis_u = basis(ctrl_len, uv[...,0], bmat)
  basis_v = basis(ctrl_len, uv[...,1], bmat)
  b_r_u = basis(ctrl_len, uv[...,0], bmat, derivative=1)
  b_r_v = basis(ctrl_len, uv[...,1], bmat, derivative=1)
  b_r_uu = basis(ctrl_len, uv[...,0], bmat, derivative=2) # bug?
  b_r_vv = basis(ctrl_len, uv[...,1], bmat, derivative=2)

  r_uu = torch.einsum("snu,suvd,snv->snd", b_r_uu, ctrl_grids, basis_v)
  r_vv = torch.einsum("snu,suvd,snv->snd", basis_u, ctrl_grids, b_r_vv)
  r_uv = torch.einsum("snu,suvd,snv->snd", b_r_u, ctrl_grids, b_r_v)
  L = torch.einsum("snd,snd->sn", r_uu, normals)
  M = torch.einsum("snd,snd->sn", r_uv, normals)
  N = torch.einsum("snd,snd->sn", r_vv, normals)
  return L, M, N


def gaussian_curvature(ctrl_grids: torch.Tensor, uv: torch.Tensor, bmat, eps=1e-10) -> torch.Tensor:
  '''
  Calculates the gaussian curvature for all ctrl_grids at uv coordinates.

  in:
    - ctrl_grids - control grid tensor (s x ctrl_len x ctrl_len x 3)
    - uv - uv coordinate tensor (s x n x 2)
  
  out:
    - curvature value tensor of shape (s x n)
  '''
  E, F, G = fundamental_I(ctrl_grids, uv, bmat)
  L, M, N = fundamental_II(ctrl_grids, uv, bmat, eps=eps)
  K = (L*N - M**2) / (E*G - F**2)
  return K


def mean_curvature(ctrl_grids: torch.Tensor, uv: torch.Tensor, bmat, eps=1e-10) -> torch.Tensor:
  '''
  Calculates the gaussian curvature for all ctrl_grids at uv coordinates.

  in:
    - ctrl_grids - control grid tensor (s x ctrl_len x ctrl_len x 3)
    - uv - uv coordinate tensor (s x n x 2)
  
  out:
    - curvature value tensor of shape (s x n)
  '''
  E, F, G = fundamental_I(ctrl_grids, uv, bmat)
  L, M, N = fundamental_II(ctrl_grids, uv, bmat, eps=eps)
  K = (L*G - 2*M*F + N*E) / (2*(E*G - F**2))
  return K # maybe sign is wrong


def principal_curvature(ctrl_grids: torch.Tensor, uv: torch.Tensor, bmat, eps=1e-6) -> torch.Tensor:
  '''
  Calculates the principal curvature values for all ctrl_grids at uv coordinates.

  in:
    - ctrl_grids - control grid tensor (s x ctrl_len x ctrl_len x 3)
    - uv - uv coordinate tensor (s x n x 2)

  out:
    - minimum curvature value tensor of shape (s x n x 2). The curvatures in the last dimension are in ascending order.
  '''
  E, F, G = fundamental_I(ctrl_grids, uv, bmat)
  L, M, N = fundamental_II(ctrl_grids, uv, bmat, eps=eps)
  K = (L*N - M**2) / (E*G - F**2) # gauss curvature
  H = (L*G - 2*M*F + N*E) / (2*(E*G - F**2)) # mean curvature #TODO validate sign
  root = torch.sqrt(H.pow(2) - K + eps)
  K_max = H + root
  K_min = H - root
  return torch.stack([K_min, K_max], dim=2)


def d_normal(ctrl_grids: torch.Tensor, uv: torch.Tensor, bmat, eps=1e-10) -> torch.Tensor:
  '''
  Derivatives of the normal for points at given uv positions.

  in:
    - ctrl_grids - control grid tensor (s x ctrl_len x ctrl_len x 3)
    - uv - uv coordinate tensor (s x n x 2)

  out:
    - derivatives of the normal in u and v direction of shape (s x n x 3 x 2)
  '''
  jacobians = jacobian(ctrl_grids, uv, bmat)
  E, F, G = fundamental_I(ctrl_grids, uv, bmat)
  L, M, N = fundamental_II(ctrl_grids, uv, bmat, eps=eps)

  # weingarten equations
  de_num = E*G - F**2
  du_n = ((F*M - G*L) / de_num).unsqueeze(dim=2) * jacobians[...,0] + ((F*L - E*M) / de_num).unsqueeze(dim=2) * jacobians[...,1]
  dv_n = ((F*N - G*M) / de_num).unsqueeze(dim=2) * jacobians[...,0] + ((F*M - E*N) / de_num).unsqueeze(dim=2) * jacobians[...,0]

  return torch.stack([du_n, dv_n], dim=3)


def eval(ctrl_grids: torch.Tensor, uv: torch.Tensor, bmat) -> torch.Tensor:
  '''
  Evaluates all surfaces from control_grids for given uv.

  in:
    - ctrl_grids - control grid tensor (surfaces to evaluate x ctrl_len x ctrl_len x 3)
    - uv - uv coordinate tensor (n x 2) if uvs are equal for each surface or (surfaces to evaluate x n x 2)
  
  out:
    - surface points tensor of shape (surfaces to evaluate x n x 3)
  '''
  ctrl_len = ctrl_grids.size(1)
  basis_u = basis(ctrl_len, uv[...,0], bmat)
  basis_v = basis(ctrl_len, uv[...,1], bmat)

  if uv.dim() == 2:
    surf_pts = torch.einsum("nu,suvd,nv->snd", basis_u, ctrl_grids, basis_v)
  else:
    surf_pts = torch.einsum("snu,suvd,snv->snd", basis_u, ctrl_grids, basis_v)
  return surf_pts
