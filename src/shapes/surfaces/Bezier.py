import torch
from shapes.surfaces.TensorProduct import TensorProduct
from shapes.curves.Bezier import Bezier as BezierCurve
from shapes.other.Coordinates import uniformGrid

class Bezier(TensorProduct):
  '''
  Bezier surface algorithms. This is much faster than e.g. the python geomdl library!
  '''
  def __init__(self, ctrl_len, uv_sampling="grid", ctrl_settings={"normal": 0.2}, ctrl_prob=None, device="cpu") -> None:
    '''
    See base class for parameter description.
    '''
    super().__init__(ctrl_len, uv_sampling=uv_sampling, ctrl_settings=ctrl_settings, ctrl_prob=ctrl_prob, device=device)
    assert ctrl_len in [2, 3, 4, 5, 6], f"BezierSampler of degree {ctrl_len} not implemented"


  def find_max_approx_curv(self, ctrl_grids: torch.Tensor, resolution=1024, only_mid=False) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor]: # WIP
    return find_max_approx_curv(ctrl_grids, resolution=resolution, only_mid=only_mid)
  
  
  @staticmethod
  def find_max_local_gauss(ctrl_grids: torch.Tensor, resolution=1024) -> tuple[torch.Tensor, torch.Tensor]:
    return find_max_local_gauss(ctrl_grids, resolution=resolution)
  
  
  @staticmethod
  def basis_matrix(ctrl_len: int, device="cpu") -> torch.Tensor:
    return basis_matrix(ctrl_len, device=device)


  @staticmethod
  def curves(ctrl_grids: torch.Tensor, u: torch.Tensor=None, v: torch.Tensor=None) -> torch.Tensor:
    return curves(ctrl_grids, u=u, v=v)

  @staticmethod
  def interpolate_at_uv(ctrl_grids: torch.Tensor, uv: torch.Tensor) -> torch.Tensor:
    return interpolate_at_uv(ctrl_grids, uv)


#############
## functional
#############

def basis_matrix(ctrl_len: int, device="cpu") -> torch.Tensor:
  '''Bernstein matrix of shape (ctrl_len x ctrl_len). Coefficients of the Bernstein polynomials.'''
  if ctrl_len == 2:
    basis_matrix = torch.tensor([
      [1., -1.],
      [0., 1.]
    ], device=device)

  elif ctrl_len == 3:
    basis_matrix = torch.tensor([
      [1., -2., 1.],
      [0., 2., -2.],
      [0., 0., 1.]
    ], device=device)

  elif ctrl_len == 4:
    basis_matrix = torch.tensor([
      [1., -3., 3., -1.],
      [0., 3., -6., 3.],
      [0., 0., 3., -3.],
      [0., 0., 0., 1.]
    ], device=device)

  elif ctrl_len == 5:
    basis_matrix = torch.tensor([
      [1., -4., 6., -4., 1.],
      [0., 4., -12., 12., -4.],
      [0., 0., 6., -12., 6.],
      [0., 0., 0., 4., -4.],
      [0., 0., 0., 0., 1.]
    ], device=device)

  elif ctrl_len == 6:
    basis_matrix = torch.tensor([
      [1., -5., 10., -10., 5., -1.],
      [0., 5., -20., 30., -20., 5.],
      [0., 0., 10., -30., 30., -10.],
      [0., 0., 0., 10., -20., 10.],
      [0., 0., 0., 0., 5., -5.],
      [0., 0., 0., 0., 0., 1.]
    ], device=device)

  else:
    raise NotImplementedError
  return basis_matrix


def curves(ctrl_grids: torch.Tensor, u: torch.Tensor=None, v: torch.Tensor=None) -> torch.Tensor:
  '''
  Extracts the parameter curves (Bezier curves) for fixed u or v parameters.\\
  in:
    - control_grids - control grid tensor (surfaces to evaluate x ctrl_len x ctrl_len x 3)
    - u - fixed u parameter values (surfaces to evaluate x n). The curve parameter is v. (function argument v must not be set)
    - v - or fixed v parameter values (surfaces to evaluate x n). The curve parameter is u. (function argument u must not be set)

  out:
    - ctrl pts of parameter curves (surfaces to evaluate x n x ctrl_len x 3)
  '''
  assert not (u != None and v != None), "You can not set u and v at the same time."
  assert not (u == None and v == None), "You must set either u or v."
  
  ctrl_len = ctrl_grids.size(1)
  if (v != None):
    t = v.repeat_interleave(ctrl_len, dim=0)
    ctrls = ctrl_grids.flatten(end_dim=1)
  else:
    t = u.repeat_interleave(ctrl_len, dim=0)
    ctrls = ctrl_grids.transpose(1,2).flatten(end_dim=1)

  curv_pts = BezierCurve.eval(ctrls, t)
  curv_pts = curv_pts.view(ctrl_grids.shape[0],3,-1,3).transpose(1,2) # this is not jit traceable
  return curv_pts


def interpolate_at_uv(ctrl_grids: torch.Tensor, uv: torch.Tensor) -> torch.Tensor:
  '''
  Adjusts the center ctrl of ctrl_grids to interpolate the previous ctrl_grids[:,1,1] = p at given uv.
  Only for Bezier surfaces with ctrl_len == 3.

  in:
    - ctrl_grids - s ctrl grids (s x 3 (=ctrl_len) x 3 (=ctrl_len) x 3)
    - uv - uv coordinates of the interpolated point (s x 2) or n variants of uv coordinates (s x n x 2)

  out:
    - new ctrl_grids of a Bezier surface with s(uv) = p of shape (s x 3 x 3 x 3) or only the mids (s x n x 3) TODO (s x n x 3 x 3 x 3)
  '''
  ctrl_len = ctrl_grids.size(1)
  assert ctrl_len == 3, "interpolate_at_uv: only Beziers with ctrl_len == 3 supported"
  p = ctrl_grids[:,1,1].clone()
  new_ctrls = ctrl_grids.clone()
  new_ctrls[:,1,1] = 0.

  if uv.dim() == 2:
    b = Bezier.eval(new_ctrls, uv.unsqueeze(1)).squeeze(dim=1)
    new_ctrls[:, 1, 1] = (p - b) / (4*uv[...,0]*(1-uv[...,0]) *uv[...,1]*(1-uv[...,1])).unsqueeze(dim=1)
  else:
    b = Bezier.eval(new_ctrls, uv)
    return (p.unsqueeze(1) - b) / (4*uv[...,0]*(1-uv[...,0]) *uv[...,1]*(1-uv[...,1])).unsqueeze(dim=2) # TODO dirty
  return new_ctrls


def find_max_local_gauss(ctrl_grids: torch.Tensor, resolution=1024, min_clip=0.1, max_clip=0.9) -> tuple[torch.Tensor, torch.Tensor]:
  '''
  Extracts the largest (magnitude) local gauss curvature extrema by grid search in the uv space.

  in:
    - ctrl_grids - s ctrl grids (s x ctrl_len x ctrl_len x 3) #TODO s > 1 not tested
    - resolution - number of samples of the gauss function in each dimension
    - min_clip - all found found minima/maxima with uv < min_clip are not considered as local minima/maxima
    - max_clip - all found found minima/maxima with uv < max_clip are not considered as local minima/maxima

  out:
    - max/min value of gaussian curvature of shape (s). Will be NaN if no local max/min is found.
    - uv value at local gaussian curvature magnitude maxima. Will be NaN if no local max/min is found.
  '''
  n = ctrl_grids.size(0)
  res = resolution

  uv = uniformGrid(res, 0., 1.).expand(n, -1, -1)
  curv = Bezier.gaussian_curvature(ctrl_grids, uv)
  idx = curv.argmax(1)
  max_uvs = uv[0, idx]
  max_curv = curv[torch.arange(0,n), idx]

  # filtering
  mask = ~max_uvs.isnan()
  mask_u = torch.logical_and(mask[...,0], max_uvs[...,0] > min_clip)
  mask_u = torch.logical_and(mask_u, max_uvs[...,0] < max_clip)
  mask_v = torch.logical_and(mask[...,1], max_uvs[...,1] > min_clip)
  mask_v = torch.logical_and(mask_v, max_uvs[...,1] < max_clip)
  mask = torch.logical_and(mask_u, mask_v)
  max_uvs[~mask] = torch.nan
  max_curv[~mask] = torch.nan

  return max_curv, max_uvs



###########
# WIP methods that do not work well yet
###########

# def polynom(self, ctrl_grids: torch.Tensor) -> torch.Tensor:
#   bcoeff = self.basis_matrix().T.unsqueeze(0) + self.basis_matrix().unsqueeze(1)
#   coeff = bcoeff.unsqueeze(3) * ctrl_grids.unsqueeze(3)
#   return coeff


def find_max_approx_curv(ctrl_grids: torch.Tensor, resolution=1024, only_mid=False) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
  '''
  Method based on grid search for finding points of 3x3 Bezier surfaces that lie on approximate local
  curvature maxima/minima of the surface edges and gaussian curvature maxima/minima of the surface (mid point).
  Only for Bezier surfaces with ctrl len of 3.

  in:
    - ctrl_grids - s ctrl grids (s x 3 x 3 x 3)
    - resolution - resolution of the rasterization step
    - only_mid - whether to search only for gauss curvature maxima/minima for the mid point (edge points stay the same)

  out:
    - a batch of 3x3 point matrices of found max/min pts of shape (s x 3 x 3 x 3)
    - max/min uv value of the gauss curvature max/min (s x 2)
    - loss values for the found gauss curvature max/min (s)
  '''
  assert ctrl_grids.size(1) == 3, "other degrees than 3 not implemented"

  s = ctrl_grids.shape[0]
  device = ctrl_grids.device

  uv_samples = uniformGrid(resolution, 0.2, 0.8, device=device)
  new_ctrls = ctrl_grids.clone()
  if not only_mid:
    new_ctrls[:, 0, 1] = BezierCurve.target_max_curvature(ctrl_grids[:, 0])
    new_ctrls[:, 2, 1] = BezierCurve.target_max_curvature(ctrl_grids[:, 2])
    new_ctrls[:, 1, 0] = BezierCurve.target_max_curvature(ctrl_grids[:, :, 0])
    new_ctrls[:, 1, 2] = BezierCurve.target_max_curvature(ctrl_grids[:, :, 2])
  new_ctrls[:, 1, 1] = 0.

  # calc all possible ctrl grids
  b = Bezier.eval(new_ctrls, uv_samples)
  num = (ctrl_grids[:, None, 1, 1] - b)
  de_num = (4*uv_samples[...,0]*(1-uv_samples[...,0]) *uv_samples[...,1]*(1-uv_samples[...,1]))
  sample_ctrls = new_ctrls.repeat(resolution**2, 1, 1, 1, 1).transpose(0,1)
  sample_ctrls[:, :, 1, 1] = num / de_num.unsqueeze(dim=1)

  # calc max curvature n u direction for every possible ctrl grid
  sample_ctrls_flat = sample_ctrls.flatten(end_dim=1)
  uv_expand = uv_samples.repeat(s, 1)
  curv_pts_u = Bezier.curves(sample_ctrls_flat, v=uv_expand[:, None, 1]).squeeze(dim=1)
  max_u = BezierCurve.max_curvature(curv_pts_u, params=True)

  # calc max curvature in v direction for every possible ctrl grid
  curv_pts_v = Bezier.curves(sample_ctrls_flat, u=uv_expand[:, None, 0]).squeeze(dim=1)
  max_v = BezierCurve.max_curvature(curv_pts_v, params=True)

  loss = torch.pow(max_u - uv_expand[:,0], 2) + torch.pow(max_v - uv_expand[:,1], 2)
  loss = loss.view(s, resolution**2)

  mins = loss.min(dim=1)
  new_ctrls[:, 1, 1] = sample_ctrls[torch.arange(0,s), mins.indices, 1, 1]
  best_uv = uv_samples[mins.indices]

  return new_ctrls, best_uv, mins.values

