import torch

class Coords():
  def __init__(self, min, max, device="cpu") -> None:
    self.min = min
    self.max = max
    self.device = device

  def uniformGrid(self, length: int, connectivity=False, embed3d=False) -> torch.Tensor:
    return uniformGrid(length, self.min, self.max, connectivity=connectivity, embed3d=embed3d, device=self.device)

  def uniformSample(self, number: int) -> torch.Tensor:
    return uniformSample(number, self.min, self.max, device=self.device)




#############
## functional
#############

def uniformGrid(length: int, min = 0., max = 1., connectivity=None, embed3d=False, device="cpu") -> torch.Tensor:
  '''
  Returns length*length grid points from [min, max]^2 as tensor with shape ((length*length) x 2).
  If connectivity is 'trias' or 'quads', this method also returns triangle or quad indices to the grid point tensor.
  If embed3d is true the grid is embedded in 3D by setting all z to 0.\\
  out:
    - uv points - tensor of shape (length**2 x 2) or (length**2 x 3) if embed3d is True
    - connectivity - optional output tensor of shape ((length-1)**2 * 2 = number of triangles x 3)
  '''
  grid_axis = torch.linspace(min, max, length, device=device)
  grid = torch.meshgrid(grid_axis, grid_axis, indexing='xy')
  if embed3d:
    zeros = torch.zeros(length**2, device=device)
    grid = torch.stack([grid[1].flatten(), grid[0].flatten(), zeros]).T
  else:
    grid = torch.stack([grid[1].flatten(), grid[0].flatten()]).T

  if connectivity in ['trias', 'quads']:
    indices = torch.arange(0, length**2).view(length, length)
    top_left = indices[:-1, :-1].flatten()
    top_right = top_left + 1
    bottom_left = indices[1:, :-1].flatten()
    bottom_right = bottom_left + 1
    if connectivity == 'triangle':
      upper_tris = torch.vstack([top_left, bottom_left, top_right]).T
      lower_tris = torch.vstack([top_right, bottom_left, bottom_right]).T
      faces = torch.cat([upper_tris, lower_tris])
    else:
      faces = torch.vstack([top_left, bottom_left, bottom_right, top_right]).T
    return grid, faces
  return grid


def uniformSample(number: int, min: int, max: int, device="cpu") -> torch.Tensor:
  '''
  Returns number samples drawn from the [min, max[^2 square as tensor with shape (number x 2)
  '''
  return torch.rand(number, 2, device=device) * (max - min) + min

