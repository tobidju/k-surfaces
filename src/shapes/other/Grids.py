import torch
import math
import shapes.other.Coordinates as Coordinates
from global_config import debug_options


class Grid():
  def __init__(self, ctrl_len: int, settings={}, device="cpu") -> None:
    '''
    Sampling of control grids (ctrl_len x ctrl_len x 3) which is used for surface sampling.
    All samples are centered and scaled to fit in the unit sphere.
    settings is a dictionary with desired sampling methods as keys. The following keys are supported:

    set at most one of the following keys:
      - grid_prior: bool - the prior to which the noise, etc. is added to, is a xy grid (default)
      - parabol_prior: float s - quadratic paraboloid prior. s determines the maximum scale of the parabolid axis
      - saddle_prior: float s - saddle (hyperbolic paraboloid) prior. s determines the maximum scale of the parabolid axis
      - no_prior: bool - uniform random control points sampled in the unit sphere but centered

    set any of the following keys:
      - uniform: (x,y,z) or float s - centered uniform noise with scale s or (x,y,z) applied to the prior
      - normal: (x,y,z) or float s - gauss noise (variance s or (x,y,z)) applied to the prior
      - bumps: int - limits the number of ctrl points that are displaced with the previous uniform or normal gauss noise
      - select_normal: ((u,v), float s) or ((u,v), (x,y,z)) - displaces the (u,v) ctrl point with additional normal gauss noise with variance s or (x,y,z)
      - select_uniform: ((u,v), float s) or ((u,v), (x,y,z)) - displaces the (u,v) ctrl point with additional centered uniform noise of scale s or (x,y,z)
      - rotated: bool - random rotation of the ctrl grid
    '''
    defaults = {
      "grid_prior": True,
      "parabol_prior": 0.,
      "saddle_prior": 0.,
      "no_prior": False,

      "uniform": None,
      "normal": None,
      "bumps": 0,
      "select_normal": None,
      "select_uniform": None,
      "rotated": False
    }
    self.settings = defaults | settings
    assert not (self.settings["bumps"] > 0
      and self.settings["normal"] == None
      and self.settings["uniform"] == None), "CtrlGridSampler: if 'bumps'>0 then a 'uniform' or 'normal' value must be set"
    
    default_grid = Coordinates.uniformGrid(ctrl_len, -.5, .5, embed3d=True, device=device)
    self.default_grid = default_grid.view(ctrl_len, ctrl_len, 3)
    self.ctrl_len = ctrl_len
    self.device = device

    # unify settings
    if type(self.settings["normal"]) == float:
      self.settings["normal"] = torch.tensor(self.settings["normal"], device=device).expand(ctrl_len, ctrl_len, 3)
    elif type(self.settings["normal"]) == tuple:
      self.settings["normal"] = torch.tensor([[self.settings["normal"]]], device=device).expand(ctrl_len, ctrl_len, -1)

    if type(self.settings["uniform"]) == tuple:
      self.settings["uniform"] = torch.tensor(self.settings["uniform"], device=device)

    if self.settings["select_uniform"] != None and type(self.settings["select_uniform"][1]) == tuple:
      self.settings["select_uniform"] = list(self.settings["select_uniform"])
      self.settings["select_uniform"][1] = torch.tensor(self.settings["select_uniform"][1], device=device)

    if self.settings["select_normal"] != None and type(self.settings["select_normal"][1]) == tuple:
      self.settings["select_normal"] = list(self.settings["select_normal"])
      self.settings["select_normal"][1] = torch.tensor(self.settings["select_normal"][1], device=device)

    # prior precalculations
    if self.settings["parabol_prior"] != 0.:
      xy = Coordinates.uniformGrid(self.ctrl_len, -1., 1., device=device)
      offset = self.settings["parabol_prior"] * xy**2
      self.offset = offset.sum(dim=1)

    if self.settings["saddle_prior"] != 0.:
      xy = Coordinates.uniformGrid(self.ctrl_len, -1., 1., device=device)
      offset = self.settings["saddle_prior"] * xy**2
      offset[:,1] *= -1
      self.offset = offset.sum(dim=1)


  def sample(self, number: int) -> torch.Tensor:
    '''Sample number control grids (number x ctrl_len x ctrl_len x 3)'''
    
    if debug_options["use_dbg_ctrl_grid"]:
      assert debug_options["dbg_ctrl_grid"].shape[0] == self.ctrl_len, "CtrlGridSampler: dbg ctrl grid has the wrong ctrl len"
      return debug_options["dbg_ctrl_grid"].repeat(number,1,1,1).to(self.device)

    # priors
    if not self.settings["no_prior"]:
      # base grid
      ctrl_grids = self.default_grid.repeat(number,1,1,1)
    else:
      ctrl_grids = torch.randn(number, self.ctrl_len, self.ctrl_len, 3, device=self.device)
      ctrl_grids /= torch.linalg.norm(ctrl_grids, dim=3).unsqueeze(3)
      d = torch.rand(number, self.ctrl_len, self.ctrl_len, 1, device=self.device).pow(1/3.)
      ctrl_grids *= d

    if self.settings["parabol_prior"] != 0. or self.settings["saddle_prior"] != 0.:
      offset = self.offset * 2 * (torch.rand(number, 1, device=self.device) -.5)
      ctrl_grids[...,2] += offset.view(number, self.ctrl_len, self.ctrl_len)

    # modification of the prior
    if self.settings["uniform"] != None:
      ctrl_grids += self.settings["uniform"] * (torch.rand(number, self.ctrl_len, self.ctrl_len, 3, device=self.device) -.5)

    if self.settings["normal"] != None:
      mean = torch.zeros((number, self.ctrl_len, self.ctrl_len, 3), device=self.device)
      std = self.settings["normal"].expand(number,-1,-1,-1)
      ctrl_grids += torch.normal(mean, std)

    if self.settings["bumps"] > 0:
      new_ctrl_grids = self.default_grid.repeat(number,1,1,1)
      selct = torch.randint(0, self.ctrl_len, (number * self.settings["bumps"], 2), device=self.device)
      idx = torch.arange(0, number, dtype=torch.int, device=self.device)
      idx = idx.repeat_interleave(self.settings["bumps"]).tolist()
      new_ctrl_grids[idx, selct[:,0], selct[:,1]] = ctrl_grids[idx, selct[:,0], selct[:,1]]
      ctrl_grids = new_ctrl_grids

    if self.settings["select_uniform"] != None:
      ((u,v), s) = self.settings["select_uniform"]
      ctrl_grids[:,u,v] += s * (torch.rand(number,3, device=self.device) -.5)

    if self.settings["select_normal"] != None:
      ((u,v), s) = self.settings["select_normal"]
      mean = torch.zeros((number, 3), device=self.device)
      ctrl_grids[:,u,v] += torch.normal(mean, s)

    # centering and scaling
    ctrl_grids -= ctrl_grids.view(number, -1, 3).mean(dim=1).view(number, 1, 1, 3)
    ctrl_grids /= torch.linalg.norm(ctrl_grids, dim=3).flatten(start_dim=1).max(dim=1).values[:,None, None, None]

    if self.settings["rotated"]:
      # see Ozols 2009 - How to generate a random unitary matrix - sec. 2.2.2
      mean = torch.zeros((number, 3,3), device=self.device)
      Q, R = torch.linalg.qr(torch.normal(mean, 1))
      ctrl_grids = torch.einsum("nuvd,ndi->nuvi", ctrl_grids, Q)

    return ctrl_grids



class PointLine2D():
  def __init__(self, ctrl_len: int, settings={}, device="cpu") -> None: # TODO? update settings API to match the newer Grid class settings
    '''
    Sampling of control points in 2D (ctrl_len x 2)
    settings is a dictionary with desired sampling methods as keys. The following keys are supported:
      - normal: (x,y) or float - gauss noise (variances) applied to the points
      - bumps: int - (if >0) limits the number of ctrl points that are displaced with the previous normal gauss noise
      - paraboloid: float - max scale of a quadratic parabola prior of the points (default==0. -> flat)
      - rotated: bool - random rotation of the ctrl points
      - rand: bool - uniform random control points sampled in [0, 1)^2 but centered
    '''
    defaults = {
      "normal": None,
      "bumps": 0,
      "paraboloid": 0.,
      "rotated": False,
      "rand": False
    }
    self.settings = defaults | settings
    assert not (self.settings["bumps"] > 0 and self.settings["normal"] == None), "CtrlPts2DSampler: if 'bumps'>0 then a 'normal' value must be set"
    
    x = torch.linspace(0, 1, ctrl_len, device=device)
    self.default_pts = torch.vstack([x, torch.zeros(ctrl_len, device=device)]).T
    self.ctrl_len = ctrl_len
    self.device = device

    if self.settings["normal"]:
      if type(self.settings["normal"]) != tuple:
        self.settings["normal"] = [self.settings["normal"]] * 2
      self.settings["normal"] = torch.tensor([self.settings["normal"]], device=device).expand(ctrl_len, -1)

    # parabola config
    if self.settings["paraboloid"] != 0.:
      x.unsqueeze_(dim=0)
      self.parabol = self.settings["paraboloid"] * (x - 0.5)**2


  def sample(self, number: int) -> torch.Tensor:
    '''Sample number control points (number x ctrl_len x 2)'''
    if not self.settings["rand"]:
      # base ctrl pts
      ctrl_pts = self.default_pts.repeat(number,1,1)
    else:
      ctrl_pts = torch.rand(number, self.ctrl_len, 2)

    if self.settings["normal"] != None or self.settings["bumps"] > 0:
      mean = torch.zeros((number, self.ctrl_len, 2), device=self.device)
      std = self.settings["normal"].expand(number,-1,-1)
      ctrl_pts += torch.normal(mean, std)

    if self.settings["bumps"] > 0:
      new_ctrl_pts = self.default_pts.repeat(number,1,1)
      selct = torch.randint(0, self.ctrl_len, (number * self.settings["bumps"],), device=self.device)
      idx = torch.arange(0, number, dtype=torch.int, device=self.device)
      idx = idx.repeat_interleave(self.settings["bumps"]).tolist()
      new_ctrl_pts[idx, selct] = ctrl_pts[idx, selct]
      ctrl_pts = new_ctrl_pts

    if self.settings["paraboloid"] != 0.:
      offset = self.parabol * torch.rand(number, 1, device=self.device)
      ctrl_pts[...,1] += offset

    # centering
    ctrl_pts -= ctrl_pts.mean(dim=1).view(number, 1, 2)

    if self.settings["rotated"]:
      angle = torch.rand(number) * 2 * math.pi
      cos = torch.cos(angle)
      sin = torch.sin(angle)
      rots = torch.vstack([cos, -sin, sin, cos]).T.view(number, 2, 2)
      ctrl_pts = torch.einsum("nud,njd->nuj", ctrl_pts, rots)

    return ctrl_pts