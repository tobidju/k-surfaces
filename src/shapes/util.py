import torch
from .other.Coordinates import uniformGrid

def cuberoot(value: torch.Tensor) -> torch.Tensor:
  '''Calculates the cuberoot of value tensor (because the torch pow function returns wrong results for negative cube roots)'''
  return value.sign() * value.abs().pow(1/3)


def cubic_roots(a: torch.Tensor, b: torch.Tensor, c: torch.Tensor, d: torch.Tensor) -> torch.Tensor:
  '''
  Finds the roots of a batch of n cubic equations: ax^3 + bx^2 + cx + d = 0

  in:
    - a, b, c, d - coefficients, each of shape (n)

  out:
    - solution tensor of shape (n x 3). If a cubic equation has less than 3 roots, multiple roots are repeated.
  '''
  # TODO solve square and linears if not cubic
  p = (3*a*c -b**2)/(3*a**2)
  q = (2*b**3 -9*a*b*c +27*a**2*d)/(27*a**3)
  # three real roots
  q.unsqueeze_(dim=1); p.unsqueeze_(dim=1)
  b_unsq = b.unsqueeze(dim=1)
  a_unsq = a.unsqueeze(dim=1)
  k = torch.tensor([[0,1,2]], device=a.device)
  t_multi = 2* torch.sqrt(-p/3) * torch.cos(1./3 * torch.arccos(3*q / (2.*p) *torch.sqrt(-3/p)) -2*torch.pi*k/3.) -b_unsq/(3*a_unsq)
  # single root
  t_single = cuberoot(-q/2 + torch.sqrt(q**2/4 + p**3/27)) + cuberoot(-q/2 -torch.sqrt(q**2/4 + p**3/27)) - b_unsq/(3*a_unsq) # float precision error up to 0.2! (choose double?)
  # select root
  t = torch.where(t_multi.isnan(), t_single.expand(-1,3), t_multi)
  return t


def localMaxima2D(maps, rounding=8, percept_rad=1, boundary=True, min=0., max=1.) -> tuple[torch.Tensor, torch.Tensor]:
  '''
  Extracts local maxima of discrete 2D height maps.
  The coordinates of the maximas are scaled to [min,max].

  in:
    - maps - 2D tensors with (batch x resolution x resolution) values
    - rounding - number of decimals to round all maps before extrema search
    - percept_rad - radius of the perceptual field for finding extremas
    - boundary - whether to allow local maxima on the boundary of the 2D height map
    - min - scales the output coordinates to [min, max]
    - max - scales the output coordinates to [min, max]

  out:
    - max values - a tuple with #batch tensors of shape (#number of found max)
    - max coordinates - a tuple with #batch tensors of shape (#number of found min x 2)
  '''
  n = maps.shape[0]
  res = maps.shape[1]
  device = maps.device

  # local max search
  if boundary:
    padding = percept_rad
  else:
    padding = 0
  maps = torch.round(maps, decimals=rounding) # truncate rounding error (because max_pool2d returns the first in a window if all values are equal)
  w_size = percept_rad * 2 + 1
  pool_vals, pool_idx = torch.nn.functional.max_pool2d(maps, w_size, stride=1, padding=padding, return_indices=True)

  # idx extraction
  if boundary:
    idx_target = torch.arange(0, res**2, device=device).view(res, res)
  else: # we need cropping
    idx_target = torch.arange(res * percept_rad, res**2 - res*percept_rad, device=device)
    idx_target = idx_target.view(res - 2 * percept_rad, res)[:, percept_rad : res-percept_rad]

  idx_wide = pool_idx == idx_target
  vals = pool_vals[idx_wide]
  idx = idx_wide.nonzero() + (percept_rad - padding)
  
  # [min,max] conversion
  uvs = idx[:,1:] / (res-1) * (max - min) + min

  # batches to list
  idx_chunks = torch.cumsum(torch.histogram(idx[:,0].cpu().float(), n+1, range=(0,n)).hist, 0)
  uvs_list = torch.tensor_split(uvs, idx_chunks[:-1].long())
  vals_list = torch.tensor_split(vals, idx_chunks[:-1].long())

  return vals_list[1:], uvs_list[1:]
