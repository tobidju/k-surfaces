####
# splitted loading and saving to avoid circular imports since we need loading in global_config.py
###

import torch


def load_ctrl_grids(path, center=False, normalize=False, device="cpu"):
  '''
  Loads ctrl grids which are saved with save_ctrl_grids.
  Returns a tensor of shape (n x ctrl_len x ctrl_len x 3).
  '''
  ctrls = load_tensor(path)

  if ctrls.dim() == 3:
    ctrls.unsqueeze_(dim=0)
  assert ctrls.dim() == 4, f"Can not load ctrls. Wrong number of dimensions: {ctrls.dim()}"

  if center:
    number = ctrls.shape[0]
    ctrls -= ctrls.view(number, -1, 3).mean(dim=1).view(number, 1, 1, 3)
  if normalize:
    ctrls /= ctrls.abs().flatten(start_dim=1).max(dim=1).values[:,None,None,None]

  ctrls = ctrls.detach() # some tensor are not stored detached
  return ctrls.to(device)


def load_tensor(path):
  '''
  Loads a torch tensor which is saved with torch.save (python) or torch::save (c++).
  Tensors saved with torch::save are jit modules and can not be loaded with torch.load.
  '''
  try:
    data = torch.jit.load(path) # if data was saved with torch::save in c++
    data = list(data.parameters())[0]
  except RuntimeError:
    data = torch.load(path) # if the data was saved with torch.save in python
  return data
