####
# splitted loading and saving to avoid circular imports since we need loading in global_config.py
###

import torch
from network.layers import VisToolModule

  
def save_model(config, path):
  '''
  Helper to save a surface model (config) in torch script for the visualization tool.
  Model must be tracable (or decorated with @torch.jit.script)
  '''
  model = config['model'].to("cpu")
  name = config['name']
  assert isinstance(model, VisToolModule), "A model for the visualization tool must inherit from network.layers.VisToolModule"
  ctrl_len = int(model.ctrl_len.item())

  model.eval()
  X = (torch.randn((16**2, 2)), torch.randn((16**2, ctrl_len**2 * 3)))
  script_module = torch.jit.trace(model, [X])
  
  script_module.save(path + name + ".pt")
  print(f"Saved model {name + '.pt'}")



def save_ctrl_grids(ctrl, path):
  '''
  Saves given ctrl grids as .pt-file.
  ctrl can be a single grid (ctrl_len x ctrl_len x 3) or multiple grids (n x ctrl_len x ctrl_len x 3)
  '''
  assert ctrl.dim() == 3 or ctrl.dim() == 4, f"Can not save ctrls. Wrong number of dimensions: {ctrl.dim()}."
  ctrls = ctrl.detach()
  if ctrl.dim() == 3:
    ctrls = ctrl.unsqueeze(dim=0)
  torch.save(ctrls, path)

