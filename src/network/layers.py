import torch
import torch.nn as nn
import torch.nn.init as init
import torch.nn.functional as F
import math
import numpy as np
from torch.autograd import Variable

class VisToolModule(nn.Module):
  '''
  Every model which should be supported by the visualization tool should inherit from this module.
  This class adds all the required attributes to the model class, so that they are saved during tracing.

  Furthermore, a visualization tool model must:
    - be traceable (can be changed by @torch.jit.script decorators)
    - the forward function should take a tuple of two tensors (uvs (n x 2) and flatted ctrl grids (n x ctrl_len**2 *3)) as input
      and should return a single tensor (shape is determined by the pred_mode)
  '''
  def __init__(self, ctrl_len, pred_mode) -> None:
    '''
    in:
      - ctrl_len - control length
      - pred_mode - whether the model predicts (pred_mode = 0) surface points (n x 3), (=1) bezier control grids (n x ctrl_len x ctrl_len x 3)
                    or (=2) curvature magnitude max uv coordinate (n x 2)
    '''
    super().__init__()
    self.register_buffer('ctrl_len', torch.tensor(ctrl_len, dtype=torch.int))
    self.register_buffer('pred_mode', torch.tensor(pred_mode, dtype=torch.int))



class Lipschitz(nn.Module):
  '''
  This is a lipschitz layer as described by Liu 2022 - Learning Smooth Neural Functions via Lipschitz Regularization.

  in:
    - in_size - size of input
    - out_size - size of output
  '''
  def __init__(self, in_size, out_size, device=None, dtype=None) -> None:
    super().__init__()
    factory = {'device': device, 'dtype': dtype}
    self.w = nn.Parameter(torch.empty((out_size, in_size), **factory))
    self.b = nn.Parameter(torch.empty((out_size), **factory))
    self.c = nn.Parameter(torch.ones((1), **factory)) # softplus of this is the lipschitz constant
    self.splus = nn.Softplus()

    # torch default init for linear layers
    init.kaiming_uniform_(self.w, a=math.sqrt(5))
    fan_in, _ = init._calculate_fan_in_and_fan_out(self.w)
    bound = 1 / math.sqrt(fan_in) if fan_in > 0 else 0
    init.uniform_(self.b, -bound, bound)


  def forward(self, x: torch.Tensor) -> torch.Tensor:
    if self.training:
      w_norm = self.normalization(self.w, self.lip_const())
      return F.linear(x, w_norm, self.b)
    return F.linear(x, self.w, self.b)


  def lip_const(self) -> torch.Tensor:
    return self.splus(self.c)


  def normalization(self, w, c) -> torch.Tensor:
    abs_sum = w.abs().sum(dim=1)
    scale = torch.minimum(torch.ones_like(abs_sum), c/abs_sum)
    return w * scale.unsqueeze(1)
  

  def train(self, mode=True):
    super().train(mode)
    if mode == False:
      with torch.no_grad():
        self.w[...] = self.normalization(self.w, self.lip_const())
