#############################
# Preprocessors to transform the datasets before each epoch
# all preprocessors get the ctrl_grids, uvs tensors from the dataloader as input
# all preprocessors return a transformed (ctrl_grids, uvs) tuple
#############################

import torch

def randomRotation(ctrl, uv=None):
  '''
  Random rotation applied to the control grids.
  '''
  n = ctrl.size(0)
  if (n > 1000):
    dev = 'cpu' # QR decomposition on GPU is super slow for a huge number of ctrls
  else:
    dev = ctrl.device

  # see Ozols 2009 - How to generate a random unitary matrix - sec. 2.2.2s
  mean = torch.zeros((n, 3,3), device=dev)
  Q, R = torch.linalg.qr(torch.normal(mean, 1))
  Q = Q.to(ctrl.device)
  ctrl = torch.einsum("nuvd,ndi->nuvi", ctrl, Q)
  return ctrl, uv
