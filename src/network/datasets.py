########################
# Datasets to load the generated data
#######################

import os
import torch
from shapes.surfaces.Bezier import Bezier
from global_config import paths, debug_options
from utils.loading import load_tensor
import urllib.request
import urllib.parse
from tqdm import tqdm, trange
import zipfile


class CurvDataset(torch.utils.data.IterableDataset):
  '''
  Batch iterator over all ctrl_grids and uv coordinates in the grid_path and uv_path folder.
  '''
  def __init__(self, path, batchsize=None, shuffle=True, normalize=True, limit=None, filter=False,download=True, epoch_preprocessors=[], device="cpu") -> None:
    '''
    Note if use_dbg_ctrl_grid is set in the global debug_options, then all control grids are replaced with the dbg_ctrl_grid.

    in:
      - path - Path to a folder with control grid files (each file in 'path/ctrls/' stores a n ctrl grid tensor of shape n x 3 x 3 x 3)
              and a folder with uv files (each file 'path/uvs/' stores a list of n uv tensors of shape m x 2 with m curvature maxima/minima)
      - batchsize - If batchsize is set to None, then all samples will be put into a single batch.
      - shuffle - Whether all loaded data should be shuffled before each epoch.
      - normalize - normalize data (centering and scaling)
      - limit - Limits the loaded data from files to limit. If limit is set to None, all data is loaded
      - filter - if true, only the closest extremum to (0.5, 0.5) is used
      - download - If set to True, the big 1.5M is downloaded to the './data_samples/' directory if path does not exist.
      - epoch_preprocessors - List of preprocessing functions which are applied to the data before each epoch (see network.preprocessors).
      - device - Returns and loads the processed data to this device before each epoch.
    '''
    # download data if not found
    down_url = paths["dataset_url"]
    if not os.path.isdir(path) and download:
      print(f"Dataset not found in {path}. Downloading 1M dataset from {down_url} to './data_samples/'")
      filename = os.path.basename(urllib.parse.urlparse(down_url).path)
      filename = os.path.splitext(filename)[0]

      # start download
      class DownloadProgressBar(tqdm): # helper for downloading
        def update_to(self, b=1, bsize=1, tsize=None):
          if tsize is not None:
              self.total = tsize
          self.update(b * bsize - self.n)

      with DownloadProgressBar(unit='B', unit_scale=True, miniters=1, desc=down_url.split('/')[-1]) as t:
        urllib.request.urlretrieve(down_url, filename=os.path.join(f"./data_samples/{filename}.zip"), reporthook=t.update_to)

      # unzip data
      with zipfile.ZipFile(f"./data_samples/{filename}.zip") as zf:
        for member in tqdm(zf.infolist(), desc='Extracting ', leave=False):
          zf.extract(member, "./data_samples/")
        print(f"Extracted to './data_samples/{filename}'")
      
    if not os.path.isdir(path):
      raise Exception("Could not found data. Did you specify a path in './data_samples/'?")

    # data loading
    grid_path = os.path.join(path, "ctrls")
    uv_path = os.path.join(path, "uvs")
    grid_files = [f for f in os.listdir(grid_path) if os.path.isfile(os.path.join(grid_path, f))]
    uv_files = [f for f in os.listdir(uv_path) if os.path.isfile(os.path.join(uv_path, f))]
    n_shapes = 0
    ctrl_grids, self.param_list = [], []

    for i in trange(len(grid_files), desc=f"Loading data samples from files... ({path})", leave=False):
      grids = load_tensor(os.path.join(grid_path, grid_files[i]))
      ctrl_grids.append(grids)
      self.param_list += load_tensor(os.path.join(uv_path, uv_files[i]))
      n_shapes += grids.size(0)
      if limit and n_shapes >= limit:
        n_shapes = int(limit)
        break
    print(f"Loaded dataset from ({path}): {n_shapes:.2e} samples")


    # data formatting/normalizing
    self.b_ctrl_grids = torch.cat(ctrl_grids)[:n_shapes]
    self.b_ctrl_grids = self.b_ctrl_grids.detach().cpu()
    
    self.param_list = self.param_list[:n_shapes]
    self.params = []
    b_ctrl_grids = []
    ctrl_grids = []
    for i, uv in tqdm(enumerate(self.param_list), desc="Processing tensor lists:", leave=False):
      uv_clean = uv.detach().cpu()
      if filter:
        uv_id = torch.linalg.vector_norm(uv_clean - torch.tensor([[.5,.5]]), dim=1).argmin()
        # uv_id = Bezier.gaussian_curvature(self.b_ctrl_grids[None,i], uv_clean[None])[0].abs().argmax()
        uv_clean = uv_clean[None, uv_id]

      self.params.append(uv_clean)
      ctrls = self.b_ctrl_grids[i].repeat(uv_clean.size(0),1,1,1)
      b_ctrl_grids.append(ctrls.clone())
      ctrls[:,1,1] = Bezier.eval(ctrls, uv_clean.unsqueeze(1)).squeeze(1)
      ctrl_grids.append(ctrls)

    self.params = torch.vstack(self.params)
    self.b_ctrl_grids = torch.vstack(b_ctrl_grids)
    self.ctrl_grids = torch.vstack(ctrl_grids)


    n_shapes = self.b_ctrl_grids.size(0)
    if not filter:
      print(f"Increased dataset size to {n_shapes:.2e}")

    if normalize:
      ctrl_pts = self.ctrl_grids.view(-1, 9, 3)
      ctrl_pts = ctrl_pts - ctrl_pts.mean(dim=1).unsqueeze(dim=1)
      ctrl_pts  =  ctrl_pts / torch.linalg.norm(ctrl_pts, dim=2).max(dim=1).values[:, None, None]
      self.ctrl_grids = ctrl_pts.view(-1, 3, 3, 3)

    if debug_options["use_dbg_ctrl_grid"]:
      self.ctrl_grids = debug_options["dbg_ctrl_grid"].repeat(n_shapes,1,1,1)

    self.ctrl_grids, self.params = self.ctrl_grids.to(device), self.params.to(device)

    self.n_shapes = n_shapes
    self.shuffle = shuffle
    self.batchsize = batchsize if batchsize else self.n_shapes
    self.e_preprocess = epoch_preprocessors
    self.device = device
    self.path = path
    self.filter = filter
    pass


  def __iter__(self):
    self.current = 0

    # apply epoch preprocessors
    ctrls = self.ctrl_grids.clone()
    params = self.params.clone()
    for p in self.e_preprocess:
      ctrls, params = p(ctrls, params)

    ctrls = ctrls.flatten(start_dim=1)

    # shuffle
    if self.shuffle:
      idx = torch.randperm(self.n_shapes)
      self.param_feat = params[idx]
      self.ctrl_feat = ctrls[idx]
    else:
      self.param_feat = params
      self.ctrl_feat = ctrls  

    return self


  def __next__(self):
    if self.current >= self.n_shapes:
      raise StopIteration
    last = self.current + self.batchsize
    if last >= self.n_shapes: # like drop_last=False
      param_batch = self.param_feat[self.current :]
      ctrl_batch = self.ctrl_feat[self.current :]
    else:
      param_batch = self.param_feat[self.current : last]
      ctrl_batch = self.ctrl_feat[self.current : last]
    self.current += self.batchsize

    return (None, ctrl_batch), param_batch


  def getAllData(self, bezier=False) -> tuple[torch.Tensor, torch.Tensor]:
    '''
    Returns all ctrls and params (not preprocessed by epoch_preprocessors).
    If bezier is set, the bezier controls and a list of tensors with all gaussian curvature max/min are returned instead (not necessarily on the device).
    '''
    if bezier:
      return self.b_ctrl_grids, self.param_list
    return self.ctrl_grids, self.params
  

  def __len__(self) -> int:
    return self.n_shapes

