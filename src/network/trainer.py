import time, shutil, os
from datetime import datetime
from tqdm import trange, tqdm
import pandas as pd
import torch
from torch.utils.tensorboard import SummaryWriter
from global_config import device, default_config, paths

class Trainer():
  def __init__(self, config) -> None:
    self.config = config
    self.model = config["model"].to(device)
    self.loss_fn = config["loss_fn"]
    self.optimizer = config["optimizer"]
    self.epoch_start = config["epoch_start"]
    self.epochs = self.epoch_start + config["epochs"]
    self.name = config["name"]
    self.tb_save_rate = 1 # tensorboard save rate for model specific stats in epochs

    self.train_loader = config["train_loader"]
    self.eval_loader = config["eval_loader"]
    tb_path = paths["tensorboard"] +  self.name + datetime.now().strftime("_%Y.%m.%d-(%H-%M-%S)")
    # if os.path.isdir(tb_path):
    #   shutil.rmtree(tb_path) # remove previous tb data
    self.writer = SummaryWriter(log_dir = tb_path)

    if config["preload"]:
      pre_model = torch.jit.load(config["preload"])
      state_dict = pre_model.state_dict()
      result = self.model.load_state_dict(state_dict, strict=False)
      if len(result.missing_keys):
        print(f"Warning: Missing keys in preload model for {self.name}!")
      if len(result.unexpected_keys):
        print(f"Warning: Unexpected keys in preload model for {self.name}!")

    # add model graph to tensorboard
    ctrl_len = int(self.model.ctrl_len.item())
    self.model.eval()
    X = (torch.randn((16**2, 2), device=device), torch.randn((16**2, ctrl_len**2 * 3), device=device))
    #self.writer.add_graph(self.model, [X])


  def train(self):

    model_name = self.config["name"]
    journal = pd.DataFrame({"epoch":[], "train loss":[], "eval loss": []})
    start_time = time.time()
    eval_loss = float('inf')
    train_loss = float('inf')
    epoch_iter = trange(self.epoch_start, self.epochs, desc="Train loss: inf.")

    try:
      for t in epoch_iter:
        batch = -1
        train_loss = 0.

        self.model.train()
        for batch, (X, Y) in tqdm(enumerate(self.train_loader), leave=False, desc="Current batch"):

          # Compute prediction error
          pred = self.model(X, reduce=False)
          loss = self.loss_fn(pred, Y)

          # Backpropagation
          if loss.requires_grad: # only if the loss contains gradient information
            self.optimizer.zero_grad()
            loss.backward()

            self.optimizer.step()
            train_loss += loss.item()
        train_loss = train_loss/(batch+1) if not batch == -1 else float('inf')
        epoch_iter.set_description(f"Train loss: {train_loss:.2e}.") # cmd line description

        # evaluation
        if not self.eval_loader == None:
          self.model.eval()
          eval_loss = torch.tensor([0.], device=device)
          for batch, (X, Y) in enumerate(self.eval_loader):
            eval_pred = self.model(X, reduce=False)
            eval_loss += self.loss_fn(eval_pred, Y)
          eval_loss /= batch+1
          progress = pd.DataFrame({"epoch": [t], "train loss": [train_loss], "eval loss": [eval_loss.item()]})
          self.writer.add_scalar("Loss/eval", eval_loss, t)
        else:
          progress = pd.DataFrame({"epoch": [t], "train loss": [train_loss]})
        journal = pd.concat([journal, progress], ignore_index=True)

        # tensor board
        self.writer.add_scalar("Loss/train", train_loss, t)
        if t % self.tb_save_rate == 0:
          if self.config["add_tb_stats_fn"]: # model specific stats
            self.config["add_tb_stats_fn"](self.writer, t)
          try:
            self.model.add_tb_stats(self.writer, t) # model specific stats
          except AttributeError as e:
            if "add_tb_stats" not in e.args[0]:
              raise

    except KeyboardInterrupt:
      print(f"Keyboard Interrupt: Skipping {self.epochs - t} epochs for model '{self.name}' and continue with other models, evaluation and saving...\n")
    
    # after training
    if device == "cuda":
      torch.cuda.synchronize()
    delta_time = time.time() - start_time

    self.model.eval()
    eval_loss = None if eval_loss == float('inf') else eval_loss.item()
    if eval_loss:
      print(f"Training done! Final eval loss: {eval_loss:.2e} time: {delta_time/60:.2f} min")
    else:
      print(f"Training done! Last train loss: {train_loss:.2e} time: {delta_time/60:.2f} min")

    # add a parameter summary
    self.writer.add_hparams({
      'Train data': self.train_loader.dataset.path,
      'Eval data': self.eval_loader.dataset.path,
      'Train data size': len(self.train_loader.dataset),
      'Eval data size': len(self.eval_loader.dataset)
    }, {
      'Final/train loss': train_loss,
      'Final/eval loss': eval_loss,
      'Final/Train time (s)': delta_time
    }, run_name='summary')

    stats = pd.DataFrame({"name":[model_name], "time": [delta_time], "eval loss": [eval_loss]})
    self.writer.close()
    return stats, journal


class All_trainer():
  def __init__(self, configs) -> None:
    configs = list(map(lambda c: default_config|c, configs))
    self.configs = configs
    self.batch_journal = pd.DataFrame({"epoch":[], "train loss":[]})
    self.model_journal = pd.DataFrame({"name":[], "time": [], "eval loss":[]})

  def train_all(self):
    for i, config in enumerate(self.configs):
      trainer = Trainer(config)
      if (not config["epochs"] or config["epochs"] == 0):
        continue

      model_name = config["name"]
      print(f"\n------------ Starting training of model {i + 1}/{len(self.configs)} ({model_name}) with {len(config['train_loader']):.0e} Datasamples ------------")
      stats, journal = trainer.train()

      # book keeping
      journal.insert(0, "name", model_name)
      self.batch_journal = pd.concat([self.batch_journal, journal], ignore_index=True)
      self.model_journal = pd.concat([self.model_journal, stats], ignore_index=True)
    print("-------------- All training done. ------------------------")

    return self.model_journal, self.batch_journal