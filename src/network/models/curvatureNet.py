import torch
import torch.nn as nn
from ..layers import VisToolModule, Lipschitz
from shapes.curves.Bezier import Bezier as BezierCurv
from shapes.surfaces.Bezier import Bezier as BezierSurf


class CurvatureNet(VisToolModule):
  '''
  Our model for uv prediction as described in the Paper
  in:
    - (uv, ctrl_feat) - tuple of uv points (n x 2) and control features for each uv point (n x ctrl_len**2 x 3)

  out:
    - ctrl_grids - tensor of predicted bezier control grids (n x ctrl_len x ctrl_len x 3)
    - or surface points of shape (n x 3) (if pred_bezier_ctrls == False)
  '''
  def __init__(self, ctrl_len=3, hidden_sizes=[256, 128], out=5, lipschitz=True):
    assert ctrl_len == 3, "other ctrl_len than 3 not supported yet"
    super().__init__(ctrl_len, 2)
    input_size = (ctrl_len**2) * 3
    self.liplayers = [] # list of all lipschitz layer

    # construct network from hidden_sizes
    sizes = [input_size] + hidden_sizes + [out]
    layers = []
    for i in range(len(hidden_sizes)+1):
      if lipschitz:
        layer = Lipschitz(sizes[i], sizes[i+1])
        self.liplayers.append(layer)
      else:
        layer = nn.Linear(sizes[i], sizes[i+1])
      layers.append(layer)
      if i < len(hidden_sizes):
        layers.append(nn.ReLU())
    layers.append(nn.Sigmoid())
    self.net = nn.Sequential(*layers)

    # save frame permutations as parameters (to move them to the right device with .to())
    perm = torch.tensor([[1,1,1],
                        [1,1,-1],
                        [1,-1,1],
                        [-1,1,1],
                        [1,-1,-1],
                        [-1,1,-1],
                        [-1,-1,1],
                        [-1,-1,-1]]).unsqueeze(0).unsqueeze(2)
    self.perm = torch.nn.parameter.Parameter(perm, requires_grad=False)


  def get_frames(self, ctrl_pts):
    cov = torch.bmm(ctrl_pts.transpose(1,2), ctrl_pts) # assume centered ctrl_pts
    val, vec = torch.linalg.eigh(cov) # this syncs with cpu
    frames = vec.unsqueeze(1) * self.perm
    return frames


  def forward(self, x : tuple[torch.Tensor, torch.Tensor], normalize=True, reduce=True):
    _, ctrl_feat = x
    ctrl_pts = ctrl_feat.view(-1, 9, 3)

    # preprocessing
    if normalize:
      ctrl_pts_center = ctrl_pts - ctrl_pts.mean(dim=1).unsqueeze(dim=1)
      ctrl_pts_scale  =  ctrl_pts_center / torch.linalg.norm(ctrl_pts_center, dim=2).max(dim=1).values[:, None, None]
      ctrl_pts = ctrl_pts_scale

    # extract frames
    frames = self.get_frames(ctrl_pts)
    ctrl_frames = torch.einsum('bpd,bfdi->bfpi', ctrl_pts, frames)

    # prediction
    ctrl_frames_t = ctrl_frames.view(-1, 8, 3, 3, 3).transpose(2,3)
    feats = torch.stack([ctrl_frames.flatten(start_dim=2), ctrl_frames_t.flatten(start_dim=2)], dim=2)
    uv_frame = self.net(feats)

    if reduce:
      uv = uv_frame.mean(1)
      return uv[:,:,2] # we use the 2 component as main component (not 0)

    return uv_frame.mean(1)

