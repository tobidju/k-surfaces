import torch
import torch.nn as nn
from ..layers import VisToolModule
from shapes.surfaces.Bezier import Bezier as BezierSurf
from shapes.curves.Bezier import Bezier as BezierCurv
from shapes.other.Coordinates import uniformGrid
from global_config import debug_options
from shapes.util import localMaxima2D


class GridSearchCurvature(VisToolModule):
  '''
  This global gridsearch model calculates the uv coordinate at the interpolated max curvature position by grid search
  (it tries resolution^2 samples and takes the best according the u/v max curve curvature loss or squared gaussian curvature
  if true_gauss is set).
  This model can only process a single input ctrl_feature (it takes the first and ignores the rest).
  Outputs a uv coordinate if pred_uv==True otherwise a Bezier control grid.
  If return_error is set, then the model returns a tuple (uv or grid, loss) where loss is the approximated loss at uv or
  (if true_gauss is also set) the gaussian curvature value.
  Only a ctrl_len of 3 is supported.

  in:
    - (uv, ctrl_feat) - tuple of uv coordinates (n x 2) and control features for each uv coordinates (n x ctrl_len**2 x 3)

  out:
    - uv or ctrl_grid - tensor with predicted uv coordinate (1 x 2) or ctrl grid (1 x 3 x 3 x 3)
  '''
  def __init__(self, ctrl_len=3, resolution=512, batch_size=32, pred_uv=True, local=False, min=0.01, max=0.99):
    assert ctrl_len == 3, "other ctrl_len than 3 not supported yet"
    pred_mode = 2 if pred_uv else 1
    super().__init__(ctrl_len, pred_mode)

    uv_samples = uniformGrid(resolution, min, max).expand([batch_size, -1, -1])
    self.batch_size = batch_size
    self.uv_samples = nn.parameter.Parameter(data=uv_samples, requires_grad=False)
    self.res = resolution
    self.pred_uv = pred_uv
    self.local = local
    self.min = min; self.max = max


  def forward(self, x : tuple[torch.Tensor, torch.Tensor]):
    assert self.training == False, "This is a grid search model. You can't train it."
    uv, ctrl_feat = x
    ctrl_grids = ctrl_feat.view(-1, 3, 3, 3)

    curv_vals = BezierSurf.gaussian_curvature(ctrl_grids, self.uv_samples).pow(2)
    curv_vals = curv_vals.view(-1, self.res, self.res).pow(2)

    if self.local:# local variant
      sq_max_curv, max_uvs = localMaxima2D(curv_vals, min=self.min, max=self.max, boundary=False)
      return max_uvs

    else:
      abs_curv_val, idx = curv_vals.abs().max(dim=0)
    
    if self.pred_uv: # return best uv coordinate
      prediction = self.uv_samples[idx]

    return prediction



class NewtonModel_Autograd(VisToolModule):
  '''
  This model calculates the uv coordinate at the interpolated max curvature position by #steps Newton steps starting at (.5, .5) or given start_uv using autograd.
  This model can only process a single input ctrl_feature (it takes the first and ignores the rest).
  The model uses the approximated Gaussian curvature loss as default or the squared Gaussian curvature if true_gauss is set.
  If return_error is set, then the model returns a tuple (uv, loss) where loss is the approximated loss at uv or (if true_gauss is set) the length
  of the gradient of the gaussian curvature function at uv. If one of the hessians during the steps is not positive definite the
  optimization stops and the loss value will be Nan.
  Only a ctrl_len of 3 is supported. This model can not be converted to a jit model (utils.saving.save_model is not possible).

  in:
    - (uv, ctrl_feat) - tuple of uv points (n x 2) and control features for each uv point (n x ctrl_len**2 x 3)

  out:
    - uv - tensor with predicted uv coordinate (1 x 2)
  '''
  def __init__(self, steps=3) -> None:
    super().__init__(3, 2)
    self.steps = steps
    self.dummy_param = torch.nn.parameter.Parameter(torch.tensor([0.]), requires_grad=False)


  def forward(self, x : tuple[torch.Tensor, torch.Tensor], start_uv: torch.Tensor=None):
    '''If start_uv of shape (2) is given, then start_uv is used as initial uv instead of (.5, .5).'''

    assert self.training == False, "This is a newton model. You can't train it."
    eval_uv, ctrls = x
    ctrl_grid = ctrls[0].view(1,3,3,3).clone()
    if start_uv == None:
      uv = torch.tensor([[0.5,0.5]], device=ctrls.device) # init uv
    else:
      uv = start_uv.unsqueeze(0).to(ctrls.device)

    def loss_fn(uv: torch.Tensor) -> torch.Tensor:
      curv = BezierSurf.gaussian_curvature(ctrl_grid, uv.unsqueeze(0)).squeeze()
      loss = -curv.pow(2)
      return loss

    # newton steps
    fail = False
    for _ in range(self.steps):
      jacobian = torch.autograd.functional.jacobian(loss_fn, uv)
      hessian = torch.autograd.functional.hessian(loss_fn, uv)
      if hessian.isnan().any(): # torch issue https://github.com/pytorch/pytorch/issues/37499
        fail = True
        break
      
      eig_vals = torch.linalg.eigvals(hessian.squeeze()).real
      if (eig_vals <= 0).any(): # hessian is not pd (add nd?)
        fail = True
        break
      update = torch.linalg.solve(hessian.squeeze(), jacobian.T).T
      uv = uv - update

    if torch.logical_or(uv < 0., uv > 1.).any(): # filter outside of uv space
      fail = True

    if fail: # e.g. non-pd hessian
      return torch.tensor(torch.nan)
    return uv
