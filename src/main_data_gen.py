######
# main script to generate the test, eval and train data
# strongly recommended to run this with cuda!
#####

import torch
from global_config import device, paths, debug_options
import shapes.surfaces.Bezier as BezierSurf
import shapes.other.Coordinates as Coords
import shapes.util
from network.models import GridSearchCurvature, NewtonModel_Autograd
from tqdm import trange

######## sampler settings
sampler_grid = [
  {"normal": (.2, .2, .3), "select_normal": ((1,1), (0.5, 0.5, 0.5)), "rotated":False},
  ]


######## generate data
surface = BezierSurf.Bezier(3, device="cpu", ctrl_settings=sampler_grid)
out_path = "./data_samples/" # where to store the samples
iterations = 1e6 # number of search rounds
surfs = 4 # surfs in each round
save_size = 2048 # should be bigger than surfs
save_nr_offset = 0

print("\n-------  Starting data generation ---------")
found = 0
discarded = 0
saves = 0
found_uvs, found_ctrls = [], []
grid_resolution = 1024
grid_search = GridSearchCurvature(resolution=grid_resolution, batch_size=surfs, local=True).to(device).eval()
newton_search = NewtonModel_Autograd(steps=3).cpu().eval()
search_iter = trange(int(iterations), desc="Found samples: 0")
try:
  for i in search_iter:
    ctrl_grids, _, _ = surface.sample(surfs, 1)
    
    # find solution by gridsearch
    grid_uvs = grid_search((None, ctrl_grids.flatten(start_dim=1).to(device)))

    # improve and filter solution with newton
    newton_grids, newton_uvs, dismissed_grids = [], [], []
    for j in range(surfs):
      if grid_uvs[j].numel() == 0: # we did not found any by grid search
        dismissed_grids.append(ctrl_grids[j])
        continue
      
      uvs = []
      grid_uvs_j = grid_uvs[j].cpu()
      for grid_uv in grid_uvs_j:
        uv = newton_search((None, ctrl_grids[j].view(1,-1)), start_uv=grid_uv)

        # filter (i.e. out of [0,1] or cusp region)
        if ~uv.isnan().all():
          uvs.append(uv)

      if len(uvs) > 0: # if any uv found
        newton_uv = torch.cat(uvs)

        # collect maxima
        newton_uvs.append(newton_uv)
        newton_grids.append(ctrl_grids[j])
      else:
        dismissed_grids.append(ctrl_grids[j])

    # collect all found samples
    if len(newton_grids) > 0:
      if len(found_ctrls) > 0 :
        found_ctrls = torch.cat([found_ctrls, torch.stack(newton_grids)])
      else:
        found_ctrls = torch.stack(newton_grids)
      found_uvs = found_uvs + newton_uvs
      found += len(newton_grids)
      discarded += surfs - len(newton_grids)

    # save results if we found enough
    if found > (saves + 1) * save_size:
      torch.save(found_ctrls[:save_size], f"{out_path}ctrls_{(saves + save_nr_offset):04d}.pt")
      torch.save(found_uvs[:save_size], f"{out_path}uvs_{(saves + save_nr_offset):04d}.pt")
      saves += 1
      found_ctrls = found_ctrls[save_size:]
      found_uvs = found_uvs[save_size:]

    search_iter.set_description(f"Found samples: {found:.2e}.")
      
except KeyboardInterrupt:
  print(f"\nKeyboardInterrupt: Stopping generation.")

print(f"\n-------  Data generation finished. Found {found} samples. Discarded {discarded} samples. ---------")
