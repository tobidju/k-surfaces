import warnings
import torch
import matplotlib
from utils.loading import load_ctrl_grids

matplotlib.rcParams.update({'font.size': 16})

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

paths = {
  # save paths
  "summary": "./summary_out/",
  "model_save": "./trained_models/",
  "tensorboard" : "./runs/",
  # load paths
  "train_data": "./data_samples/dataset_1.5M/train/",
  "eval_data": "./data_samples/dataset_1.5M/eval/",
  "test_data": "./data_samples/dataset_1.5M/test/",
  "dataset_url": "https://tubcloud.tu-berlin.de/s/f6fnaDDkPEK3wGy/download/dataset_1.5M.zip"
}

# default model config entries if not overwritten
default_config = {
  "name": "no_name", # name of the model used in plots, console and file names
  "model": None, # a pytorch nn.Module to train/evaluate
  "loss_fn": torch.nn.MSELoss(), # loss function used for training
  "optimizer": None, # optimizer
  "train_loader": None, # pytorch Dataloader for train data
  "eval_loader": None, # pytorch Dataloader for evaluation data (could be None if there is no evaluation data)
  "epochs": 0, # remaining epochs to train (set to 0 to skip training)
  "epoch_start": 0, # offset epoch at which the training begins (relevant for tensorboard/continue training)
  "preload": None, # path to a torchscript .pt-file to initialize the parameters before training/evaluation # TODO preload for optimizer
  "add_tb_stats_fn": False # callback after each epoch to add model specific stats to tensorboard (False if there is no function)
}

debug_options = {
  "show_all_warnings": False, # ignore irrelevant warnings
  "use_dbg_ctrl_grid": False, # if True every loaded and sampled ctrl grid will always replaced by the following dbg_ctrl_grid

  ## Some useful ctrl_grids for debugging. See data_samples/demo_ctrl_grids/ for demo ctrl grid definitions 
  # "dbg_ctrl_grid": load_ctrl_grids("your_test_case.pt", device=device)[0],
}

if not debug_options["show_all_warnings"]:
  warnings.filterwarnings("ignore", message="Note that order of the arguments: ceil_mode and return_indices will changeto match the args list in nn.MaxPool2d.") # we do not depend on this
  # ignore some torch.jit TraceWarnings
  # the following cases occur in some models but are not relevant for the trace
  warnings.filterwarnings("ignore", message="Converting a tensor to a Python boolean might cause the trace to be incorrect.")
  warnings.filterwarnings("ignore", message="Converting a tensor to a Python number might cause the trace to be incorrect.")
  warnings.filterwarnings("ignore", message="torch.tensor results are registered as constants in the trace.")

if debug_options["use_dbg_ctrl_grid"]:
  warnings.warn("Debug mode! All grids of shapes.other.Grids are replaced with the dbg_ctrl_grid!")
