######
# main script to train the network and/or save models for the visualization tool
#####

import torch
from torch import nn
from torch.utils.data import DataLoader
import seaborn as sns
import math
from global_config import device, paths, debug_options
from network.trainer import All_trainer
import network.models as models
import network.datasets as datasets
import network.preprocessors as prep
import utils.saving
import utils.loading


######## data setup
train_data = datasets.CurvDataset(paths["train_data"], batchsize=128, limit=1e4, filter=True, epoch_preprocessors=[prep.randomRotation], device=device)
eval_data = datasets.CurvDataset(paths["test_data"], batchsize=128, limit=1e3, filter=True, shuffle=False, device=device)


######## model configs
configs = []


model_curvatureNet1 = models.CurvatureNet(hidden_sizes=[256, 128], lipschitz=True, out=5)

def multi_loss(X,Y): # loss function as used in the paper
  main_uv = X[:,:,2] # we use the 2 component as main component (not 0)
  # top left
  X[:,0,0] -= 0.5* main_uv[:,0]
  X[:,1,0] += 0.5* main_uv[:,1]

  # top right
  X[:,:,1] += 0.5* main_uv

  # bottom left
  X[:,:,3] -= 0.5* main_uv

  # bottom right
  X[:,0,4] += 0.5* main_uv[:,0]
  X[:,1,4] -= 0.5* main_uv[:,1]

  dist = torch.linalg.vector_norm(X-Y.unsqueeze(2), dim=1)
  idx = dist.argmin(dim=1)
  u = torch.gather(X[:,0], 1, idx[:,None])
  v = torch.gather(X[:,1], 1, idx[:,None])
  uv = torch.cat([u,v], dim=1)
  lip_consts = [l.lip_const() for l in model_curvatureNet1.liplayers]
  return nn.functional.mse_loss(uv,Y) + 1e-6 * math.prod(lip_consts)


configs.append({
  "name": "My-own-K-Surface",
  "model": model_curvatureNet1,
  "loss_fn": multi_loss,#
  "optimizer": torch.optim.Adam(model_curvatureNet1.parameters(), lr=1e-3, betas=(0.9, 0.999)),
  "train_loader": DataLoader(train_data, batch_size=None), # no automatic batching
  "eval_loader": DataLoader(eval_data, batch_size=None),
  "epochs": 100,
  # "preload": paths['model_save'] + 'K-Surface.pt' # uncomment to preload a trained model
})


########## train all
trainer = All_trainer(configs)
model_journal, global_journal = trainer.train_all()
model_journal.to_pickle(paths["summary"] + "model_journal.pkl")
global_journal.to_pickle(paths["summary"] + "global_journal.pkl")


########### save torch script
for config in configs:
  if config['epochs'] > 0:
    utils.saving.save_model(config, paths['model_save'])
