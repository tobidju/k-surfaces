#include "state.h"
#include <filesystem>
#include <iostream>

State::State() : device(torch::kCPU) {
	device = torch::Device(torch::cuda::is_available() ? torch::kCUDA : torch::kCPU);

	// collect all trained model paths
	using namespace std;
	vector<filesystem::path> model_paths;
	vector<filesystem::path> spline_paths;
	copy(filesystem::directory_iterator(model_path), filesystem::directory_iterator(), std::back_inserter(model_paths));
	sort(model_paths.begin(), model_paths.end());
	//reverse(model_paths.begin(), model_paths.end());
	for (const auto& entry : model_paths) {
		stringstream nameStream;
		nameStream << entry.filename();
		string name = nameStream.str();
		model_names.push_back(name.substr(1, name.length() - 2));
	}

	// collect all spline templates
	copy(filesystem::directory_iterator(spline_template_path), filesystem::directory_iterator(), std::back_inserter(spline_paths));
	sort(spline_paths.begin(), spline_paths.end());
	for (const auto& entry : spline_paths) {
		stringstream nameStream;
		nameStream << entry.filename();
		string name = nameStream.str();
		spline_template_names.push_back(name.substr(1, name.length() - 6));
	}
}