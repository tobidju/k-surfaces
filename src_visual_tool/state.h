#pragma once
#include <torch/torch.h>
#include <Eigen/Core>
#include <vector>
#include <string>
#include "shapes/surface/BezierSurf.h"


enum color_meaning {bezier_error, gauss_curv, mean_curv, true_gauss_curv, true_mean_curv, u_curv, v_curv, abs_min_curv, image_texture};
enum line_meaning {princ_curv, line_curv_maxs, normals, tangents};


struct State {
  State();

  // model settings
  const std::string model_path = "trained_models/";
  std::vector<std::string> model_names;
  int selct_model = 0;
  int ctrl_len = -1; // will be updated on model load
  int pred_mode = -1; // 0 - model predicts surf pts, 1 - model predicts bezier ctrls, 2 - model predicts a uv coord
  int iterations = 2; // global local iterations for pred_mode == 2
  bool interpolate_boundary = true; // interpolate boundary at 1D curvature extrema
  torch::Device device;

  // settings for model mesh generation
  int sample_len = 16;
  torch::Tensor uv_samples;
  Eigen::MatrixXi faces;
  torch::Tensor in_ctrl_pts; // controlled by ui and input to networks
  Eigen::MatrixXi in_ctrl_connect;
  torch::Tensor out_ctrl_pts; // predicted by networks
  Eigen::MatrixXi out_ctrl_connect;
  int n_patches = 1; // number of patches in out_ctrl_pts
  std::vector<std::string> ctrl_template_names = {"Paraboloid", "High diagonal", "Shift first v row", "Twisted", "Saddle", "90 deg X rotation", "90 deg Y rotation", "90 deg Z rotation", "Transposed"};
  int selct_ctrl_templ = -1;
  const std::string spline_template_path = "spline_templates/";
  std::vector<std::string> spline_template_names; // all file names of .obj quad meshes in the spline_template_folder
  int selct_spline = -1;
  bool auto_center = false;

  // settings for uv texture coordinate generation (only for spline surfaces, pred_mode==2)
  Eigen::MatrixXi in_uv_faces; // uv face indices loaded from spline_templates
  Eigen::MatrixXd in_uv_coords;
  Eigen::MatrixXi out_uv_faces; // new uv face indices for generated surface
  Eigen::MatrixXd out_uv_coords;


  // mesh stats display settings
  color_meaning selct_col_meaning = color_meaning::gauss_curv;
  float color_scale = 1.;
  line_meaning selct_line_meaning = line_meaning::princ_curv;
  bool color_patches = false; // whether to color each patch differently
  Eigen::RowVector3f default_mesh_color = Eigen::RowVector3f(0.9,0.9,0.9);


  // text statistics to display
  bool sync_cuda = true; // whether to sync cuda before time measuring 
  int total_time = -1; // time for surface generation
  int local_time = -1;
  int global_time = -1;
  int stats_time = -1;
  int param_count = -1; // number of trainable parameters of current model
};
