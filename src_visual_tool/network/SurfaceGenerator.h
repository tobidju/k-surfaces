#pragma once
#include <igl/opengl/glfw/Viewer.h>
#include <torch/torch.h>
#include <Eigen/Core>
#include <vector>
#include "../shapes/surface/BezierSurf.h"
#include "Model.h"

typedef torch::jit::script::Module Module;
namespace network {

	class SurfaceGenerator {
	public:
		SurfaceGenerator(igl::opengl::glfw::Viewer* viewer);
		void updateSurface();
		void clear(bool keep_ctrls = false); // if keep_ctrls == true, the ctrls should not be cleared
		bool processSplineGrid(const torch::Tensor V, const torch::Tensor F); // assigns and generates ctrls and index tensors for quad mesh (returns true on success)
		int getCtrlLen(int idx); // ctrl len from a given module idx
		int getPredictionMode(int idx); // whether the given module at idx predicts surf pts, bezier ctrls or a uv coord
		torch::Tensor getCtrlsAsGrids(bool input_ctrls = false); // returns the out_ctrls as grids (usefull if pred_mode == 2), if input_ctrls == true all out_ctrl with corresponding in_ctrls are replaced with in_ctrls
		void setCtrlsFromGrids(torch::Tensor ctrl_grids, bool also_input_ctrls = false); // sets out_ctrls from grids, if also_input_ctrls == true all corresponding input_ctrls will also be set the same points
		void setImageTexture( // see state and updateTextureCoordinates() to set texture coordinates before
			const Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic>& R,
			const Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic>& G,
			const Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic>& B,
			const Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic>& A);

	private:
		// V are the predicted/calculated vertices
		void updateMeshStats(Eigen::MatrixXd &V);
		void updateNormals();
		void updateTextureCoordinates();
		torch::Tensor generate(); // returns new surface points
		void setup(bool keep_ctrls = false);
		torch::Tensor generateColors(int n, float S = 1., float L = 0.8); // generates n colors with n different hues as tensor of shape (n x 3)

		igl::opengl::glfw::Viewer* viewer;
		std::vector<std::shared_ptr<Model>> models;
		shapes::surface::BezierSurf util_surf = NULL;
		const Eigen::RowVector3d default_mesh_color = Eigen::RowVector3d(0.9, 0.9, 0.9);
		const Eigen::RowVector3d line_colorA = Eigen::RowVector3d(0.9, 0.1, 0.1);
		const Eigen::RowVector3d line_colorB = Eigen::RowVector3d(0.1, 0.1, 0.9);
		Eigen::MatrixXd mesh_colors; // different generated colors for each patch
		torch::Tensor pred_bezier_ctrl_grid; // if the model predicts bezier ctrls, the ctrls are stored here for stats calculation

		// precomputed index tensors for prediction mode 2
		torch::Tensor edge_ctrls; // which of state.out_ctrl_pts are (non boundary) edge ctrl pts
		std::map<int, torch::Tensor> corner_ctrls; // which of state.out_ctrl_pts are corner ctrls with a given dependency value
		torch::Tensor center_ctrls; // which of state.out_ctrl_pts are center ctrls
		torch::Tensor edge_dps_half; // (#edge ctrls x 4) adjacent center ctrls and corner ctrls in out_ctrl_pts for each edge ctrl in edge_ctrls
		torch::Tensor edge_dps_quarter; // (#edge ctrls x 4) surrounding edge controls in out_ctrl_pts for each edge ctrl in edge_ctrls
		std::map<int, torch::Tensor> corner_dps; // (#neighbors x #dependencies) ajacent edge ctrls in out_ctrl_pts for each corner ctrl in corner_ctrls for a given dependency value
		torch::Tensor const_ctrls; // which of state.out_ctrl_pts should stay constant
		torch::Tensor patches; // (#patches x 9) indices of state.out_ctrl_pts belonging to the same patch (ordered like centers)
		torch::Tensor controllable; // which of state.out_ctrl_pts can be controlled by user e.g. are the state.in_ctrl_pts
		torch::Tensor kappa_boundary_0; // boundary ctrls used as 0 ctrl pt for kappa boundary behavior
		torch::Tensor kappa_boundary_1;
		torch::Tensor kappa_boundary_2;
		torch::Tensor kappa_boundary; // stack of kappa_boundary_0-2
		torch::Tensor pre_A; // precomputed system matrix for the global step
		torch::Tensor pre_B; // ctrl pts tensor of AX=B

		torch::Tensor previous_tmp; // helper for measuring ctrl change
		torch::Tensor uv_tmp; // helper for uv storage for evaluation
	};

}
