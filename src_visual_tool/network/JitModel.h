#pragma once
#include <torch/torch.h>
#include "Model.h"

namespace network {
	class JitModel : public Model {
	public:
		JitModel(std::string path);
		torch::Tensor predict(torch::Tensor& uv, torch::Tensor& ctrl_feat) override;

		void to(const torch::Device& device) override {
			jitModule.to(device);
			auto params = jitModule.parameters();
			for (auto p : jitModule.parameters()) {
				//std::cout << p << std::endl;
			}
		}

	private:
		torch::jit::script::Module jitModule;
		std::vector<torch::jit::IValue> inputs;
		std::string path;
	};
}
