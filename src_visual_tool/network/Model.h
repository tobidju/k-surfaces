#pragma once
#include <torch/torch.h>


namespace network {
	class Model {
	public:
		virtual torch::Tensor predict(torch::Tensor& uv, torch::Tensor& ctrl_feat) = 0;
		virtual void to(const torch::Device&) { };

		int getCtrlLen() { // ctrl len for input and output
			return ctrl_len;
		};
		int getPredictionMode() { // whether this model predicts surf pts, bezier ctrls or a uv coord
			return pred_mode;
		};
		int getParamCount() { // number of trainable parameters
			return param_count;
		};

	protected:
		int pred_mode = -1;
		int ctrl_len = -1;
		int param_count = 0;
	};
}
