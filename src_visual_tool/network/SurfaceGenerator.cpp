#include "SurfaceGenerator.h"
#include <cmath>
#include <chrono>
#include <igl/principal_curvature.h>
#include <igl/avg_edge_length.h>
#include <igl/edges.h>
#include "../state.h"
#include "../shapes/surface/BezierSurf.h"
#include "../shapes/uv/uv.h"
#include "../utils/utils.h"
#include "JitModel.h"

extern struct State state;
using namespace network;

SurfaceGenerator::SurfaceGenerator(igl::opengl::glfw::Viewer* viewer) : viewer{ viewer } {

  // load all models
  for (auto const& model_name : state.model_names) {
    std::shared_ptr<JitModel> model(new JitModel(state.model_path + model_name));
    model->to(state.device);
    models.push_back(model);
  }

  // viewer data creation
  viewer->append_mesh(); // new mesh for a additional surface visualization (e.g. bezier with same ctrl pts)
  // and add a new dummy mesh for out_ctrl_pts overlays
  // (libigl only allows one overlay per mesh and only displays the overlay if the mesh is visible (even if show_overlay is set))
  viewer->append_mesh();
  // a third mesh (dummy) for curvature direction overlays
  viewer->append_mesh();
  viewer->selected_data_index = 0;

  // initial viewer values setup
  viewer->core().set(viewer->data(0).show_texture, false); // color visualization
  viewer->core().set(viewer->data(1).is_visible, false); // additional surface visualization
  // for stats visualized by lines
  viewer->core().set(viewer->data(3).show_overlay, false);
  viewer->data(3).line_width = 2.;

  setup();
}


void SurfaceGenerator::updateSurface() {
  torch::Tensor prediction = generate();
  Eigen::MatrixXd prediction_eig = utils::torchToEigen<double>(prediction);
  viewer->data(0).set_vertices(prediction_eig);
  updateNormals();
  bool show_texture = viewer->core().is_set(viewer->data(0).show_texture);
  if (!show_texture) {
    if (state.color_patches) {
      viewer->data(0).set_colors(mesh_colors);
    }
    else {
      viewer->data(0).set_colors(state.default_mesh_color.cast<double>());
    }
  }
  
  // set mesh stats
  updateMeshStats(prediction_eig);
}


void SurfaceGenerator::updateNormals() {
  if (state.pred_mode == 2) {
    torch::Tensor ctrl_grids = state.out_ctrl_pts.index({ patches }).to(state.device);
    auto normals = util_surf.normal(ctrl_grids, state.uv_samples.unsqueeze(0).expand({ state.n_patches, -1, -1 }));
    normals = normals.flatten(0, 1);
    auto normals_eig = utils::torchToEigen<double>(normals);
    viewer->data(0).set_normals(normals_eig);
  }
  else {
    viewer->data(0).compute_normals();
  }
}


torch::Tensor SurfaceGenerator::generateColors(int n, float S, float L) {
  torch::Tensor rgb;
  auto H = torch::arange(0., 360., 360./n);
  float a = L < 0.5 ? L : 1 - L;
  a = S * a;

  for (int v : {0, 8, 4}) {
    auto k = (v + H / 30.) % 12;
    auto chanel = torch::where(k - 3 < 9 - k, k - 3, 9 - k);
    chanel.clamp_(-1., 1.);
    chanel = L - a * chanel;
    if (v > 0) rgb = torch::vstack({ rgb,chanel });
    else rgb = chanel;
  }
  rgb.transpose_(0, 1);
  rgb.index_put_({ torch::randperm(n) }, rgb.clone());
  return rgb;
}


torch::Tensor SurfaceGenerator::generate() {
  using namespace std::chrono;
  auto total_start = high_resolution_clock::now();
  if (state.device == torch::kCUDA && state.sync_cuda) {
    torch::cuda::synchronize();
  }

  auto cur_model = models[state.selct_model];
  torch::Tensor prediction, ctrl_feat;
  int local_time = 0, global_time = 0;

  if (state.pred_mode < 2) { // no multiple global local iterations
    // create inputs
    if (state.pred_mode == 1) { // predicts bezier ctls
      ctrl_feat = state.in_ctrl_pts.flatten(0, 1).unsqueeze(0);
    } 
    else { // predicts surf pts
      ctrl_feat = state.in_ctrl_pts.flatten(0, 1).expand({ (int)pow(state.sample_len, 2), -1 });
    }

    auto local_start = high_resolution_clock::now();
    prediction = cur_model->predict(state.uv_samples, ctrl_feat);
    auto local_stop = high_resolution_clock::now();
    local_time = duration_cast<microseconds>(local_stop - local_start).count();

    // calc surf pts if we predicted bezier ctrl pts
    if (state.pred_mode == 1) {
      pred_bezier_ctrl_grid = prediction;
      prediction = prediction.flatten(0, 2).to(state.device); // some analytic models always use cpu
      state.out_ctrl_pts = prediction;
      prediction = util_surf.eval(prediction, state.uv_samples);
    }
  }

  else { // multiple global local iterations if we predict uv coords
    using namespace torch::indexing;
    torch::Tensor new_ctrls;
    int n_ctrls = state.out_ctrl_pts.size(0);
    int n_centers = center_ctrls.size(0);
    torch::Tensor B = pre_B.index_put({ controllable }, state.in_ctrl_pts);
    prediction = torch::full({ n_centers, 2 }, .5f, torch::kCPU); // initial uvs

    torch::Tensor p_boundary, boundary_A;
    if (state.interpolate_boundary) {
      p_boundary = B.index({ kappa_boundary_1 });
      boundary_A = torch::zeros({ kappa_boundary_0.size(0), n_ctrls });
    }
    //previous_tmp = state.out_ctrl_pts.clone(); // helper code to measure ctrl change
    //std::cout << "---------" << std::endl;

    for (int i = 0; i < state.iterations + 1; i++) { // +1 iteration for init
      // global step -- connect patches
      auto global_start = high_resolution_clock::now();
      torch::Tensor center_A = torch::zeros({ n_centers, n_ctrls });
      torch::Tensor basis = util_surf.basis_uv(prediction.to(torch::kCPU)).flatten(1); // global step is particularly faster on CPU (even with copying back and forth)
      center_A.scatter_(1, patches, basis);
      torch::Tensor A = pre_A.index_put({ center_ctrls }, center_A); // pre_A contains all except the equations for the centers

      if (state.interpolate_boundary) {
        torch::Tensor c0 = state.out_ctrl_pts.index({ kappa_boundary_0 });
        torch::Tensor c2 = state.out_ctrl_pts.index({ kappa_boundary_2 });

        torch::Tensor t = util_surf.curve.target_max_curvature(c0, c2, p_boundary);
        torch::Tensor boundary_basis = util_surf.basis(t);
        boundary_A.scatter_(1, kappa_boundary, boundary_basis);
        A.index_put_({ kappa_boundary_1 }, boundary_A);
      }

      new_ctrls = torch::linalg::solve(A, B);


      auto global_stop = high_resolution_clock::now();
      global_time += duration_cast<microseconds>(global_stop - global_start).count();

      // helper code to measure ctrl change
      //int64_t arr[1] = { 1 };
      //c10::IntArrayRef dim_arr = c10::IntArrayRef(arr, 1); // why does vector_norm not accept int like other torch func?
      //std::cout << torch::linalg::vector_norm((previous_tmp - new_ctrls), 2, dim_arr, false, c10::nullopt).mean().item().toFloat() << std::endl;
      //previous_tmp = new_ctrls.clone();
      

      // local step -- predict uvs
      if (i < state.iterations) {
        auto local_start = high_resolution_clock::now();
        if (state.device == torch::kCUDA && state.sync_cuda) {
          torch::cuda::synchronize();
        }
        // create inputs
        new_ctrls.index_put_({ center_ctrls }, state.in_ctrl_pts.index({ Slice(-state.n_patches) })); // set mids to interpolated
        new_ctrls = new_ctrls.to(state.device);
        ctrl_feat = new_ctrls.index({ patches }).flatten(1);

        // predict uvs
        prediction = cur_model->predict(state.uv_samples, ctrl_feat); // the state.uv_samples are not used by the model in the paper
        auto local_stop = high_resolution_clock::now();
        if (state.device == torch::kCUDA && state.sync_cuda) {
          torch::cuda::synchronize();
        }
        local_time += duration_cast<microseconds>(local_stop - local_start).count();

        //std::cout << (prediction - uv_tmp).pow(2).sum().item().toFloat() << std::endl;
      }
    }

    //std::cout << "----" << std::endl;
    // evaluate final spline at uv
    //state.uv_tmp = prediction.clone();
    torch::Tensor ctrl_grids = new_ctrls.index({ patches }).to(state.device);
    prediction = util_surf.eval(ctrl_grids, state.uv_samples).flatten(0, 1);

    state.out_ctrl_pts = new_ctrls;
    pred_bezier_ctrl_grid = ctrl_grids;
  }

  // timings
  if (state.device == torch::kCUDA && state.sync_cuda) {
    torch::cuda::synchronize();
  }
  auto total_stop = high_resolution_clock::now();
  state.total_time = duration_cast<microseconds>(total_stop - total_start).count();
  if (state.pred_mode == 2) {
    local_time /= state.iterations;
    global_time /= (state.iterations + 1); // we have done one more global because of init
  }
  state.local_time = local_time;
  state.global_time = global_time;

  return prediction;
}


int SurfaceGenerator::getCtrlLen(int idx) {
  return models[idx]->getCtrlLen();
}


int SurfaceGenerator::getPredictionMode(int idx) {
  return models[idx]->getPredictionMode();
}


torch::Tensor SurfaceGenerator::getCtrlsAsGrids(bool input_ctrls) {
  torch::Tensor ctrls;
  if (state.pred_mode < 2) {
    if (input_ctrls) ctrls = state.in_ctrl_pts;
    else ctrls = state.out_ctrl_pts;
    return ctrls.view({ -1, state.ctrl_len, state.ctrl_len, 3 });
  }

  if (input_ctrls) {
    ctrls = state.out_ctrl_pts.index_put({ controllable }, state.in_ctrl_pts);
    ctrls =  ctrls.index({ patches });
  }
  else {
    ctrls = state.out_ctrl_pts.index({ patches });
  }
  return ctrls.view({ -1, state.ctrl_len, state.ctrl_len, 3 });
}


void SurfaceGenerator::setCtrlsFromGrids(torch::Tensor ctrl_grids, bool also_input_ctrls) {
  if (state.pred_mode < 2) {
    state.out_ctrl_pts = ctrl_grids.view({ -1, 3 });
    if (also_input_ctrls) {
      state.in_ctrl_pts = ctrl_grids.view({ -1,3 });
    }
    return;
  }

  auto out_device = state.out_ctrl_pts.device();
  state.out_ctrl_pts.index_put_({ patches }, ctrl_grids.view({-1, state.ctrl_len*state.ctrl_len, 3}).to(out_device));
  if (also_input_ctrls) {
    state.in_ctrl_pts = state.out_ctrl_pts.index({ controllable });
  }
}


void SurfaceGenerator::clear(bool keep_ctrls) {
  // backup some settings from data 0 and 1 (e.g. we dont want to clear that the color visualisation is visible if we change the mesh)
  // Unfortunately in libigl we can not clear the mesh only
  bool color_vis_visible = viewer->core().is_set(viewer->data(0).show_texture);
  bool second_surf_visible = viewer->core().is_set(viewer->data(1).is_visible);

  viewer->data(0).clear(); // predicted surface data
  viewer->data(1).clear(); // additional surface (e.g. a bezier with the same ctrl pts) visualization
  viewer->data(3).clear_edges(); // dummy mesh for curvature direction overlays

  // restore some viewer values
  viewer->core().set(viewer->data(0).show_texture, color_vis_visible); // color visualization
  viewer->core().set(viewer->data(1).is_visible, second_surf_visible); // additional surface visualization

  // remove old subdivided texture coordinates
  state.out_uv_coords = Eigen::MatrixXd(0, 0);
  state.out_uv_faces = Eigen::MatrixXi(0, 0);

  // timings
  state.total_time = -1;
  state.local_time = -1;
  state.global_time = -1;
  state.stats_time = -1;

  state.param_count = -1;

  setup(keep_ctrls);
}


void SurfaceGenerator::setup(bool keep_ctrls) {
  int new_ctrl_len = models[state.selct_model]->getCtrlLen();
  int new_pred_mode = models[state.selct_model]->getPredictionMode();
  state.param_count = models[state.selct_model]->getParamCount();

  if (new_pred_mode == 2 && new_ctrl_len != 3) {
    throw std::invalid_argument("Model not supported: Bezier splines with ctrl_len != 3 not implemented.");
  }
  if (!keep_ctrls) {
    // initialize new in and out ctrl_pts
    if (new_pred_mode == 2) { // if we predict uv coords for splines (pred_mode == 2) we have to precompute some index tensors
      torch::Tensor simple_quad_V = shapes::uv::uniformGrid(2, -.5, .5, true);
      torch::Tensor simple_quad_F = torch::tensor({ {0,2,3,1} });
      processSplineGrid(simple_quad_V, simple_quad_F);
    }
    else if (new_pred_mode < 2) { // new single patch
      torch::Tensor ctrl_pts = shapes::uv::uniformGrid(new_ctrl_len, -.5, .5, true);
      state.in_ctrl_pts = ctrl_pts.to(state.device);
      state.in_ctrl_connect = utils::torchToEigen<int>(shapes::uv::gridEdges(new_ctrl_len));
      state.out_ctrl_connect = state.in_ctrl_connect;
      state.n_patches = 1;
    }
  }

  // setup uv samples
  state.uv_samples = shapes::uv::uniformGrid(state.sample_len).to(state.device);
  torch::Tensor faces = shapes::uv::gridFaces(state.sample_len);
  int n_samples = state.uv_samples.size(0);
  torch::Tensor offsets = torch::arange(0, n_samples * state.n_patches, n_samples).unsqueeze(1).unsqueeze(2);
  faces = faces.expand({ state.n_patches, -1, -1 }) + offsets; // repeat faces indices for each patch
  state.faces = utils::torchToEigen<int>(faces.flatten(0,1));

  state.ctrl_len = new_ctrl_len;
  state.pred_mode = new_pred_mode;
  util_surf = shapes::surface::BezierSurf(state.ctrl_len);

  // calculate subdivided uv texture coordinates
  if (state.pred_mode == 2 && state.in_uv_coords.size() > 0) {
    updateTextureCoordinates();
  }

  // first prediction
  torch::Tensor prediction = generate();
  Eigen::MatrixXd prediction_eig = utils::torchToEigen<double>(prediction);
  viewer->data(0).set_mesh(prediction_eig, state.faces);
  if (state.out_uv_coords.size() > 0) {
    viewer->data(0).set_uv(state.out_uv_coords);
  }
  if (mesh_colors.rows() != state.sample_len * state.sample_len * state.n_patches) { // keep the colors for the patches if size similar
    auto colors = generateColors(state.n_patches);
    colors = colors.repeat_interleave(state.sample_len * state.sample_len, 0);
    mesh_colors = utils::torchToEigen<double>(colors);
  }

  if (state.color_patches) {
    viewer->data(0).set_colors(mesh_colors);
  }
  else {
    viewer->data(0).set_colors(state.default_mesh_color.cast<double>());
  }
  updateNormals();

  // further viewer data setup for the second surface visualization
  viewer->data(1).set_mesh(prediction_eig, state.faces); // we use prediction_eig as dummy to create this additional surface
  viewer->data(1).show_faces = false;

  // correct invalid display settings for the new model and update mesh stats
  if ((state.ctrl_len != 3 || state.pred_mode == 0) && state.selct_line_meaning == line_meaning::line_curv_maxs) {
    state.selct_line_meaning = line_meaning::princ_curv; // stats like line_curv_maxs can only calculated for ctrl_len == 3 if the model pred bezier
  }
  if (state.pred_mode != 2 && state.selct_line_meaning == line_meaning::tangents) {
    state.selct_line_meaning = line_meaning::princ_curv;
  }
  if (state.pred_mode == 0 &&
    (state.selct_col_meaning == color_meaning::u_curv ||
      state.selct_col_meaning == color_meaning::v_curv ||
      state.selct_col_meaning == color_meaning::abs_min_curv)) {
    state.selct_col_meaning = color_meaning::bezier_error;
  }
  if (state.pred_mode == 2 && (state.selct_col_meaning == color_meaning::bezier_error)) {
    state.selct_col_meaning = color_meaning::gauss_curv;
  }
  if (state.pred_mode == 0 && state.selct_col_meaning == color_meaning::true_gauss_curv) {
    state.selct_col_meaning = color_meaning::gauss_curv;
  }
  if (state.pred_mode == 0 && state.selct_col_meaning == color_meaning::true_mean_curv) {
    state.selct_col_meaning = color_meaning::mean_curv;
  }
  if (state.pred_mode > 0 && state.selct_col_meaning == color_meaning::gauss_curv) {
    state.selct_col_meaning = color_meaning::true_gauss_curv;
  }
  if (state.pred_mode > 0 && state.selct_col_meaning == color_meaning::mean_curv) {
    state.selct_col_meaning = color_meaning::true_mean_curv;
  }
  if (state.selct_col_meaning == color_meaning::image_texture) {
    state.selct_col_meaning = color_meaning::gauss_curv;
  }
  updateMeshStats(prediction_eig);
}


bool SurfaceGenerator::processSplineGrid(const torch::Tensor V, const torch::Tensor F) {
  // corner and center ctrl pts
  torch::Tensor corners = V;
  int n_corners = corners.size(0);
  auto faces = F.to(torch::kLong);
  torch::Tensor centers = corners.index({ faces }).sum(1) / 4.;
  int n_centers = centers.size(0);

  // edge ctrl pts
  torch::Tensor visits = torch::zeros({ n_corners, n_corners }); // undirected edge visits
  torch::Tensor connectivity = torch::full({ n_corners, n_corners }, -1); // half edge connectivity matrix (entries are adjacent centers)
  for (int j = 0; j < n_centers; j++) {
    auto face = F.index({ j });
    for (int i = 0; i < 4; i++) {
      int start = face.index({ i }).item().toInt();
      int end = face.index({ (i + 1) % 4 }).item().toInt();

      std::initializer_list<at::indexing::TensorIndex> idx;
      if (start < end) idx = { start, end };
      else idx = { end, start };
      int vs = visits.index(idx).item().toInt();
      visits.index_put_(idx, vs + 1);

      connectivity.index_put_({ start, end }, j);
    }
  }
  if ((visits > 2).any().item().toBool()) {
    std::cout << "Input mesh is non-manifold." << std::endl;
    return false;
  }
  using namespace torch::indexing;
  torch::Tensor edge_ids = visits.nonzero();
  torch::Tensor edges = corners.index({ edge_ids }).sum(1) / 2.;
  int n_edges = edges.size(0); // number of undirected edges / edge ctrls
  torch::Tensor not_border = (visits == 2).index({ edge_ids.index({Slice(), 0}), edge_ids.index({Slice(), 1}) });
  torch::Tensor border = ~not_border;
  torch::Tensor not_border_id = edge_ids.index({ not_border });
  torch::Tensor neighbor_f0 = connectivity.index({ not_border_id.index({Slice(), 0 }), not_border_id.index({Slice(), 1 }) });
  torch::Tensor neighbor_f1 = connectivity.index({ not_border_id.index({Slice(), 1 }), not_border_id.index({Slice(), 0 }) });
  torch::Tensor edge_neighbor = torch::stack({ neighbor_f0, neighbor_f1 }, 1);

  // corner neighbors
  std::vector<torch::Tensor> corner_edge_adj; // tensors of adjacent edge ids for corner id
  for (int i = 0; i < n_corners; i++) {
    corner_edge_adj.push_back(torch::empty({ 0 }, torch::kLong));
  }
  for (int i = 0; i < n_edges; i++) {
    int start = edge_ids.index({ i, 0 }).item().toInt();
    int end = edge_ids.index({ i, 1 }).item().toInt();
    auto start_idx = corner_edge_adj.at(start);
    auto end_idx = corner_edge_adj.at(end);
    corner_edge_adj.at(start) = torch::cat({ start_idx, torch::tensor({i}) });
    corner_edge_adj.at(end) = torch::cat({ end_idx, torch::tensor({i}) });
  }
  std::vector<std::vector<int>> corner_center_adj(n_corners); // list of adjacent center ids for corner id
  for (int i = 0; i < F.size(0); i++) {
    for (int j = 0; j < 4; j++) {
      int start = F.index({ i, j }).item().toInt();
      corner_center_adj.at(start).push_back(i);
    }
  }

  // count corner ctrl conditions
  std::map<int, torch::Tensor> corner_dc; // all corners with a given dependency count
  std::list dns = {0, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // implemented dependency number
  for (int dn : dns) {
    corner_dc.emplace(std::make_pair(dn, torch::empty(0, torch::kLong)));
  }
  for (int i = 0; i < n_corners; i++) {
    // calculate number of dependencies
    int c = corner_center_adj.at(i).size(); // number of adjacent patches
    if (c == 1) c = 0;
    //else {} // simplification which ignores borders: ajacent patches == number of conditions (TODO implement else)

    if (std::find(dns.begin(), dns.end(), c) == dns.end()) {
      std::cout << "Input mesh connectivity is not supported (connectivity of " << c << " quads at a corner)." << std::endl;
      return false;
    }
    auto vals = corner_dc.at(c);
    torch::Tensor new_vals = torch::cat({ vals, torch::tensor({i}) }, 0);
    corner_dc.insert_or_assign(c, new_vals);
  }
  // extract corner condition indices
  std::map<int, torch::Tensor> corner_dps; // (ctrl idx x dependent ctrls idx) for given dependency count
  torch::Tensor edge_nrs = torch::arange(n_corners, n_corners + n_edges);
  for (int dn : dns) {
    if (dn == 0) continue;
    int n = corner_dc.at(dn).size(0);
    torch::Tensor dps = torch::empty({ n, dn }, torch::kLong);

    for (int i = 0; i < n; i++) {
      int corner_id = corner_dc.at(dn).index({ i }).item().toInt();
      int neighbors = corner_center_adj.at(corner_id).size(); // adjacent patches
      int borders = border.index({ corner_edge_adj.at(corner_id) }).sum().item().toInt(); // adjacent border edges

      if (neighbors == 2 && borders == 2) { // like plane border
        torch::Tensor mask = border.index({ corner_edge_adj.at(corner_id) });
        dps.index_put_({ i }, corner_edge_adj.at(corner_id).index({ mask }) + n_corners);
      }
      else if (neighbors == 4 && borders == 0) { // like plane center
        dps.index_put_({ i }, corner_edge_adj.at(corner_id) + n_corners);
      }
      else if (neighbors == 3 && borders == 0) { // like cube corner
        dps.index_put_({ i }, corner_edge_adj.at(corner_id) + n_corners);
      }

      else if (neighbors > 4 && borders == 0) { // other (not G1)
        dps.index_put_({ i }, corner_edge_adj.at(corner_id) + n_corners);
        std::cout << "Warning: Found corner with " << neighbors << ". Result will not be G1 continuous!" << std::endl;
      }
      else {
        std::cout << "Input mesh connectivity is not implemented (found corner with " << neighbors << " connected patches "
          " and " << borders << " border edges)." << std::endl;
        return false;
      }
    }
    corner_dps.emplace(std::make_pair(dn, dps));
  }

  // patch indices
  torch::Tensor patches = torch::empty({ n_centers, 9 }, torch::kLong);
  patches.index_put_({ Slice(), 0 }, faces.index({ Slice(), 0 }));
  patches.index_put_({ Slice(), 6 }, faces.index({ Slice(), 1 }));
  patches.index_put_({ Slice(), 8 }, faces.index({ Slice(), 2 }));
  patches.index_put_({ Slice(), 2 }, faces.index({ Slice(), 3 }));
  patches.index_put_({ Slice(), 4 }, torch::arange(n_edges + n_corners, n_corners + n_edges + n_centers));
  torch::Tensor half_edges = (connectivity + 1).nonzero();
  int n_he = half_edges.size(0);
  torch::Tensor he_to_e = torch::empty({ n_corners, n_corners }); // half edge id to edge id map
  for (int e = 0; e < n_edges; e++) {
    int start = edge_ids.index({ e, 0 }).item().toInt();
    int end = edge_ids.index({ e, 1 }).item().toInt();
    he_to_e.index_put_({ start, end }, e);
    he_to_e.index_put_({ end, start }, e);
  }
  for (int e = 0; e < n_he; e++) {
    int start = half_edges.index({ e, 0 }).item().toInt();
    int end = half_edges.index({ e, 1 }).item().toInt();
    int e_id = he_to_e.index({ start, end }).item().toInt();
    int center = connectivity.index({ start, end }).item().toInt();
    if (start == patches.index({ center, 0 }).item().toInt()) { // left edge
      patches.index_put_({ center,  3 }, e_id + n_corners);
    }
    else if (start == patches.index({ center, 6 }).item().toInt()) { // bottom edge
      patches.index_put_({ center,  7 }, e_id + n_corners);
    }
    else if (start == patches.index({ center, 8 }).item().toInt()) { // right edge
      patches.index_put_({ center,  5 }, e_id + n_corners);
    }
    else { // top edge
      patches.index_put_({ center,  1 }, e_id + n_corners);
    }
  }

  // extract edge condition indices
  torch::Tensor edge_to_not_border_idx = torch::full_like(not_border, -1, torch::kLong);
  edge_to_not_border_idx.index_put_({ not_border }, torch::arange(not_border.sum().item().toInt())); // idx conversion
  std::vector<std::set<int>> edge_dps_half_v(edge_neighbor.size(0));
  std::vector<std::set<int>> edge_dps_quarter_v(edge_neighbor.size(0));
  for (int p = 0; p < patches.size(0); p++) {
    auto patch = patches.index({ p });
    int center_ctrl = patch.index({ 4 }).item().toInt();
    
    // for top edge
    int top_edge = patch.index({ 1 }).item().toInt() - n_corners;
    top_edge = edge_to_not_border_idx.index({ top_edge }).item().toInt();
    if (top_edge != -1) {
      int first_corner = patch.index({ 0 }).item().toInt();
      int second_corner = patch.index({ 2 }).item().toInt();
      int first_edge = patch.index({ 3 }).item().toInt();
      int second_edge = patch.index({ 5 }).item().toInt();

      edge_dps_half_v.at(top_edge).insert({ center_ctrl, first_corner, second_corner });
      edge_dps_quarter_v.at(top_edge).insert({ first_edge, second_edge });
    }

    // for right edge
    int right_edge = patch.index({ 5 }).item().toInt() - n_corners;
    right_edge = edge_to_not_border_idx.index({ right_edge }).item().toInt();
    if (right_edge != -1) {
      int first_corner = patch.index({ 2 }).item().toInt();
      int second_corner = patch.index({ 8 }).item().toInt();
      int first_edge = patch.index({ 1 }).item().toInt();
      int second_edge = patch.index({ 7 }).item().toInt();

      edge_dps_half_v.at(right_edge).insert({ center_ctrl, first_corner, second_corner });
      edge_dps_quarter_v.at(right_edge).insert({ first_edge, second_edge });
    }

    // for bottom edge
    int bottom_edge = patch.index({ 7 }).item().toInt() - n_corners;
    bottom_edge = edge_to_not_border_idx.index({ bottom_edge }).item().toInt();
    if (bottom_edge != -1) {
      int first_corner = patch.index({ 8 }).item().toInt();
      int second_corner = patch.index({ 6 }).item().toInt();
      int first_edge = patch.index({ 5 }).item().toInt();
      int second_edge = patch.index({ 3 }).item().toInt();

      edge_dps_half_v.at(bottom_edge).insert({ center_ctrl, first_corner, second_corner });
      edge_dps_quarter_v.at(bottom_edge).insert({ first_edge, second_edge });
    }

    // for left edge
    int left_edge = patch.index({ 3 }).item().toInt() - n_corners;
    left_edge = edge_to_not_border_idx.index({ left_edge }).item().toInt();
    if (left_edge != -1) {
      int first_corner = patch.index({ 6 }).item().toInt();
      int second_corner = patch.index({ 0 }).item().toInt();
      int first_edge = patch.index({ 7 }).item().toInt();
      int second_edge = patch.index({ 1 }).item().toInt();

      edge_dps_half_v.at(left_edge).insert({ center_ctrl, first_corner, second_corner });
      edge_dps_quarter_v.at(left_edge).insert({ first_edge, second_edge });
    }
  }
  // convert edge conditions to tensors
  torch::Tensor edge_dps_half = torch::empty({ edge_neighbor.size(0), 4 }, torch::kLong);
  torch::Tensor edge_dps_quarter = torch::empty({ edge_neighbor.size(0), 4 }, torch::kLong);
  for (int n = 0; n < edge_neighbor.size(0); n++) {
    int i = 0;
    for (auto it = edge_dps_half_v.at(n).begin(); it != edge_dps_half_v.at(n).end(); it++) {
      edge_dps_half.index_put_({ n,i }, *it);
      i++;
    }
    i = 0;
    for (auto it = edge_dps_quarter_v.at(n).begin(); it != edge_dps_quarter_v.at(n).end(); it++) {
      edge_dps_quarter.index_put_({ n,i }, *it);
      i++;
    }
  }

  // ctrls which are constant
  int n_all = n_corners + n_edges + n_centers;
  torch::Tensor const_ctrls = torch::full({ n_all }, false);
  const_ctrls.index_put_({ corner_dc.at(0) }, true);
  const_ctrls.index_put_({ Slice(n_corners, n_corners + n_edges) }, border);

  // ctrls which are controllable in ui
  torch::Tensor controllable_ctrls = const_ctrls.clone();
  controllable_ctrls.index_put_({ Slice(n_corners + n_edges, n_all)}, true);


  // boundary ctrls needed for optional kappa-boundary behavior
  std::vector<long> vkappa_boundary_0, vkappa_boundary_1, vkappa_boundary_2;
  for (int n = 0; n < n_centers; n++) {
    auto patch = patches.index({ n });
    // top edge
    if (controllable_ctrls.index({ patch.index({ 1 }).item().toLong() }).item().toBool()) {
      vkappa_boundary_0.push_back(patch.index({ 0 }).item().toLong());
      vkappa_boundary_1.push_back(patch.index({ 1 }).item().toLong());
      vkappa_boundary_2.push_back(patch.index({ 2 }).item().toLong());
    }

    // bottom edge
    if (controllable_ctrls.index({ patch.index({ 7 }).item().toLong() }).item().toBool()) {
      vkappa_boundary_0.push_back(patch.index({ 6 }).item().toLong());
      vkappa_boundary_1.push_back(patch.index({ 7 }).item().toLong());
      vkappa_boundary_2.push_back(patch.index({ 8 }).item().toLong());
    }

    // left edge
    if (controllable_ctrls.index({ patch.index({ 3 }).item().toLong() }).item().toBool()) {
      vkappa_boundary_0.push_back(patch.index({ 0 }).item().toLong());
      vkappa_boundary_1.push_back(patch.index({ 3 }).item().toLong());
      vkappa_boundary_2.push_back(patch.index({ 6 }).item().toLong());
    }

    // right edge
    if (controllable_ctrls.index({ patch.index({ 5 }).item().toLong() }).item().toBool()) {
      vkappa_boundary_0.push_back(patch.index({ 2 }).item().toLong());
      vkappa_boundary_1.push_back(patch.index({ 5 }).item().toLong());
      vkappa_boundary_2.push_back(patch.index({ 8 }).item().toLong());
    }
  }
  // convert to tensors
  int n_kappa = vkappa_boundary_0.size();
  torch::Tensor kappa_boundary_0 = torch::empty({ n_kappa });
  torch::Tensor kappa_boundary_1 = torch::empty({ n_kappa });
  torch::Tensor kappa_boundary_2 = torch::empty({ n_kappa });
  for (int n = 0; n < vkappa_boundary_0.size(); n++) {
    kappa_boundary_0.index_put_({ n }, vkappa_boundary_0.at(n));
    kappa_boundary_1.index_put_({ n }, vkappa_boundary_1.at(n));
    kappa_boundary_2.index_put_({ n }, vkappa_boundary_2.at(n));
  }


  // connectivity of out_ctrls
  Eigen::MatrixXi out_connect(2 * n_edges + 4 * n_centers, 2);
  for (int i = 0; i < n_edges; i++) {
    int e = i + n_corners;
    int start = edge_ids.index({ i,0 }).item().toInt();
    int end = edge_ids.index({ i,1 }).item().toInt();
    out_connect.row(2 * i) = Eigen::Vector2i(e, start);
    out_connect.row(2 * i + 1) = Eigen::Vector2i(e, end);
  }
  int n2_e = 2 * n_edges;
  for (int i = 0; i < n_centers; i++) {
    int c = i + n_corners + n_edges;
    int t = patches.index({ i,1 }).item().toInt();
    int l = patches.index({ i,3 }).item().toInt();
    int b = patches.index({ i,7 }).item().toInt();
    int r = patches.index({ i,5 }).item().toInt();
    out_connect.row(n2_e + 4 * i) = Eigen::Vector2i(c, t);
    out_connect.row(n2_e + 4 * i + 1) = Eigen::Vector2i(c, l);
    out_connect.row(n2_e + 4 * i + 2) = Eigen::Vector2i(c, b);
    out_connect.row(n2_e + 4 * i + 3) = Eigen::Vector2i(c, r);
  }

  // connectivity of in_ctrls
  torch::Tensor in_connect = torch::zeros({ n_all, n_all }, torch::kLong);
  for (int i = 0; i < n_edges; i++) {
    int start = edge_ids.index({ i,0 }).item().toInt();
    int end = edge_ids.index({ i,1 }).item().toInt();

    if (not_border.index({ i }).item().toBool()) { // inner edge
      int n1 = connectivity.index({ start, end }).item().toInt();
      int n2 = connectivity.index({ end, start }).item().toInt();
      int e_0 = n1 + n_corners + n_edges;
      int e_1 = n2 + n_corners + n_edges;
      if (e_1 < e_0) {
        int tmp = e_1;
        e_1 = e_0;
        e_0 = tmp;
      }
      in_connect.index_put_({ e_0, e_1 }, 1);
    }
    else { // outer edge
      int e = i + n_corners;
      int n = connectivity.index({ start, end }).item().toInt();
      if (controllable_ctrls.index({ start }).item().toBool()) {
        in_connect.index_put_({ start, e }, 1); // start < e always
      }
      if (controllable_ctrls.index({ end }).item().toBool()) {
        in_connect.index_put_({ end, e }, 1); // end < e always
      }
      if (n == -1) { // wrong half edge, take opposite
        n = connectivity.index({ end, start }).item().toInt();
      }
      in_connect.index_put_({ e,  n + n_corners + n_edges }, 1); // e < n + n_corners + n_edges always
    }
  }
  for (int i = 0; i < corner_dc.at(2).size(0); i++) { // connectivity for non-controllable border ctrls
    int e_0 = corner_dps.at(2).index({ i, 0 }).item().toInt();
    int e_1 = corner_dps.at(2).index({ i, 1 }).item().toInt();
    if (e_1 < e_0) {
      int tmp = e_1;
      e_1 = e_0;
      e_0 = tmp;
    }
    in_connect.index_put_({ e_0, e_1 }, 1);
  }
  // convert idx of out_ctrls to idx of in_ctrls
  in_connect = in_connect.nonzero();
  torch::Tensor in_to_out = torch::arange(0, controllable_ctrls.size(0)).index({ controllable_ctrls });
  torch::Tensor range = torch::arange(0, in_to_out.size(0));
  torch::Tensor out_to_in = torch::full({ n_all }, -1, torch::kLong).index_put({ in_to_out }, range);
  in_connect = out_to_in.index({ in_connect });

  // store all ctrls and index tensors
  state.out_ctrl_pts = torch::cat({ corners, edges, centers }); // note the order!
  state.in_ctrl_pts = state.out_ctrl_pts.index({ controllable_ctrls });
  state.out_ctrl_connect = out_connect;
  state.in_ctrl_connect = utils::torchToEigen<int>(in_connect.to(torch::kFloat)); //TODO torchToEigen only supports kFloat as input
  state.n_patches = patches.size(0);
  this->edge_ctrls = torch::arange(n_corners, n_corners + n_edges).index({ not_border });
  this->corner_ctrls = corner_dc;
  this->center_ctrls = torch::arange(n_corners + n_edges, n_all);
  this->const_ctrls = torch::arange(0, n_all).index({ const_ctrls });
  this->controllable = torch::arange(0, n_all).index({ controllable_ctrls });
  this->corner_dps = corner_dps;
  this->edge_dps_half = edge_dps_half;
  this->edge_dps_quarter = edge_dps_quarter;
  this->patches = patches;
  this->kappa_boundary_0 = kappa_boundary_0.to(torch::kLong);
  this->kappa_boundary_1 = kappa_boundary_1.to(torch::kLong);
  this->kappa_boundary_2 = kappa_boundary_2.to(torch::kLong);
  this->kappa_boundary = torch::stack({ this->kappa_boundary_0, this->kappa_boundary_1, this->kappa_boundary_2 }, 1);


  // precompute the system matrix (all equations except for the centers)
  torch::Tensor A = torch::empty({ n_all, n_all });
  torch::Tensor half = torch::full({ n_all, n_all }, .5);
  torch::Tensor quarter = torch::full({ n_all, n_all }, .25);
  
  // corner dependencies
  for (int dn : dns) {
    if (dn < 2) continue;
    int n = this->corner_dps.at(dn).size(0);
    torch::Tensor coeffs = torch::zeros({ n, n_all });
    coeffs.scatter_(1, this->corner_dps.at(dn), 1. / dn);
    torch::Tensor range = torch::arange(0, n);
    coeffs.index_put_({ range, this->corner_ctrls.at(dn) }, -1);
    A.index_put_({ this->corner_ctrls.at(dn) }, coeffs);
  }
  
  // edge dependencies
  torch::Tensor edges_A = torch::zeros({ this->edge_ctrls.size(0), n_all });
  //edges_A.scatter_(1, this->edge_dps_half.index({ Slice(), Slice(0,2) }), -half);
  edges_A.scatter_(1, this->edge_dps_half.index({ Slice(), Slice(2,4) }), half);
  //edges_A.scatter_(1, this->edge_dps_quarter, quarter);
  range = torch::arange(0, this->edge_ctrls.size(0));
  edges_A.index_put_({ range, this->edge_ctrls }, -1.);
  A.index_put_({ this->edge_ctrls }, edges_A);

  torch::Tensor const_A = torch::zeros({ this->const_ctrls.size(0), n_all }); // constant ctrls
  range = torch::arange(0, this->const_ctrls.size(0));
  const_A.index_put_({ range, this->const_ctrls }, 1.);
  A.index_put_({ this->const_ctrls }, const_A);

  torch::Tensor B = torch::zeros({ n_all, 3 });
  B.index_put_({ this->controllable }, state.in_ctrl_pts);
  pre_A = A;
  pre_B = B;

  return true;
}


void SurfaceGenerator::updateMeshStats(Eigen::MatrixXd& pred_pts) {
  using namespace std::chrono;
  auto time_start = high_resolution_clock::now();

  // only if we turned the visualization on in the ui
  bool show_texture = viewer->core().is_set(viewer->data(0).show_texture);
  bool line_vis = viewer->core().is_set(viewer->data(3).show_overlay);
  bool bezier_vis = viewer->core().is_set(viewer->data(1).is_visible);


  // stats visualized by a mesh
  Eigen::MatrixXd bezier_pts_eig;
  if (bezier_vis || (show_texture && state.selct_col_meaning == color_meaning::bezier_error)) {
    torch::Tensor bezier_pts = util_surf.eval(state.in_ctrl_pts, state.uv_samples);
    bezier_pts_eig = utils::torchToEigen<double>(bezier_pts);
    viewer->data(1).set_vertices(bezier_pts_eig);
  }


  // compute curvature approximation
  Eigen::MatrixXd PD1, PD2;
  Eigen::VectorXd PV1, PV2;
  if ((show_texture && state.selct_col_meaning != color_meaning::bezier_error) ||
      (line_vis && state.selct_line_meaning == line_meaning::princ_curv)) {
    igl::principal_curvature(pred_pts, state.faces, PD1, PD2, PV1, PV2);
  }

  // calculate texture if not from external image
  if (show_texture && state.selct_col_meaning != color_meaning::image_texture) {
    Eigen::VectorXd color_val;
    viewer->data(0).show_texture = false; // for some reason set_data() ignores the colormap argument if show_texture != 0

    switch (state.selct_col_meaning) {
    case color_meaning::bezier_error:
      color_val = (pred_pts - bezier_pts_eig).rowwise().norm();
      viewer->data(0).set_data(color_val, 0., state.color_scale * 0.1, igl::COLOR_MAP_TYPE_JET, 32);
      break;

    case color_meaning::gauss_curv: {
      color_val = PV1.cwiseProduct(PV2);
      viewer->data(0).set_data(color_val, -10. * state.color_scale, 10. * state.color_scale, igl::COLOR_MAP_TYPE_VIRIDIS, 32);
      break;
    }

    case color_meaning::true_gauss_curv: {
      auto ctrls = pred_bezier_ctrl_grid.view({ -1, state.ctrl_len, state.ctrl_len, 3 });
      auto gauss_curv = util_surf.gaussian_curvature(ctrls, state.uv_samples.unsqueeze(0));
      Eigen::MatrixXd gauss_curv_eig = utils::torchToEigen<double>(gauss_curv.flatten(0,1));
      viewer->data(0).set_data(gauss_curv_eig, -10. * state.color_scale, 10. * state.color_scale, igl::COLOR_MAP_TYPE_VIRIDIS, 32);
      break;
    }

    case color_meaning::mean_curv: {
      color_val = 0.5 * (PV1 + PV2);
      viewer->data(0).set_data(color_val, -3. * state.color_scale, 3. * state.color_scale, igl::COLOR_MAP_TYPE_VIRIDIS, 32);
      break;
    }

    case color_meaning::true_mean_curv: {
      auto ctrls = pred_bezier_ctrl_grid.view({ -1,3,3,3 });
      auto mean_curv = util_surf.mean_curvature(ctrls, state.uv_samples.unsqueeze(0));
      Eigen::MatrixXd mean_curv_eig = utils::torchToEigen<double>(mean_curv.flatten(0, 1));
      viewer->data(0).set_data(mean_curv_eig, -3. * state.color_scale, 3. * state.color_scale, igl::COLOR_MAP_TYPE_VIRIDIS, 32);
      break;
    }

    case color_meaning::u_curv: { // only selectable in ui if model predicts bezier ctrls
      auto t = torch::linspace(0, 1, state.sample_len, state.device);
      auto curv_pts_u = util_surf.curves(pred_bezier_ctrl_grid, t.unsqueeze(0), false).squeeze(0);
      auto curvature = util_surf.curve.curvature(curv_pts_u, t);
      color_val = utils::torchToEigen<double>(curvature.transpose(0,1).flatten());
      viewer->data(0).set_data(color_val, -state.color_scale, state.color_scale, igl::COLOR_MAP_TYPE_VIRIDIS, 32);
      break;
    }

    case color_meaning::v_curv: { // only selectable in ui if model predicts bezier ctrls
      auto t = torch::linspace(0, 1, state.sample_len, state.device);
      auto curv_pts_u = util_surf.curves(pred_bezier_ctrl_grid, t.unsqueeze(0), true).squeeze(0);
      auto curvature = util_surf.curve.curvature(curv_pts_u, t);
      color_val = utils::torchToEigen<double>(curvature.flatten());
      viewer->data(0).set_data(color_val, -state.color_scale, state.color_scale, igl::COLOR_MAP_TYPE_VIRIDIS, 32);
      break;
    }

    case color_meaning::abs_min_curv: { // only selectable in ui if model predicts bezier ctrls
      auto abs_PV1 = PV1.cwiseAbs();
      auto abs_PV2 = PV2.cwiseAbs();
      color_val = (abs_PV1.array() > abs_PV2.array()).select(abs_PV2, abs_PV1);
      viewer->data(0).set_data(color_val, -3. * state.color_scale, 3. * state.color_scale, igl::COLOR_MAP_TYPE_VIRIDIS, 32);
      break;
    }
    }
  }


  // stats visualized by lines
  if (line_vis) {
    viewer->data(3).clear_edges();

    switch (state.selct_line_meaning) {
    case line_meaning::princ_curv: {
      const double len = igl::avg_edge_length(pred_pts, state.faces) * 0.6;
      viewer->data(3).add_edges(pred_pts + PD1 * len, pred_pts - PD1 * len, line_colorA);
      viewer->data(3).add_edges(pred_pts + PD2 * len, pred_pts - PD2 * len, line_colorB);
      break;
    }

    case line_meaning::line_curv_maxs: { // this option is not selectable in ui if degree != 3 and model does not predict bezier ctrls
      using namespace torch::indexing;
      auto t = torch::linspace(0, 1, state.sample_len, state.device);

      for (int p = 0; p < state.n_patches; p++) {
        // maxs in u
        auto ctrl_grid = pred_bezier_ctrl_grid.index({ p }).view({ 1,3,3,3 });
        torch::Tensor curv_pts_u = util_surf.curves(ctrl_grid, t.unsqueeze(0), false).squeeze(0);
        auto max_pts_u = util_surf.curve.max_curvature(curv_pts_u);

        Eigen::MatrixXd u_start = utils::torchToEigen<double>(max_pts_u.index({ Slice(0,-1) }));
        Eigen::MatrixXd u_end = utils::torchToEigen<double>(max_pts_u.index({ Slice(1) }));
        viewer->data(3).add_edges(u_start, u_end, line_colorA);

        // maxs in v
        torch::Tensor curv_pts_v = util_surf.curves(ctrl_grid, t.unsqueeze(0), true).squeeze(0);
        auto max_pts_v = util_surf.curve.max_curvature(curv_pts_v);
        Eigen::MatrixXd v_start = utils::torchToEigen<double>(max_pts_v.index({ Slice(0,-1) }));
        Eigen::MatrixXd v_end = utils::torchToEigen<double>(max_pts_v.index({ Slice(1) }));
        viewer->data(3).add_edges(v_start, v_end, line_colorB);
      }
      break;
    }

    case line_meaning::normals: {
      Eigen::MatrixXd normals = viewer->data(0).V_normals * 0.05;
      viewer->data(3).add_edges(pred_pts, pred_pts + normals, line_colorB);
      break;
    }

    case line_meaning::tangents: {
      torch::Tensor ctrls = state.out_ctrl_pts.index({ patches }).view({ -1, state.ctrl_len, state.ctrl_len, 3 });
      auto jacobians = util_surf.jacobian(ctrls, state.uv_samples.unsqueeze(0).expand({ state.n_patches, -1, -1 }));
      using namespace torch::indexing;
      auto tangent_u = utils::torchToEigen<double>(jacobians.index({Ellipsis, 0 }).flatten(0,1));
      auto tangent_v = utils::torchToEigen<double>(jacobians.index({Ellipsis, 1 }).flatten(0,1));
      viewer->data(3).add_edges(pred_pts, pred_pts + 0.05 * tangent_u, line_colorA);
      viewer->data(3).add_edges(pred_pts, pred_pts + 0.05 * tangent_v, line_colorB);
      break;
    }
    }
  }
  
  auto time_stop = high_resolution_clock::now();
  state.stats_time = duration_cast<microseconds>(time_stop - time_start).count();
}


void SurfaceGenerator::updateTextureCoordinates() {
  state.out_uv_faces = state.faces; // TODO not the best triangulation in all cases
  int n_patch_faces = state.sample_len * state.sample_len;
  state.out_uv_coords = Eigen::MatrixXd(state.n_patches * n_patch_faces, 2);

  for (int p = 0; p < state.n_patches; p++) {

    Eigen::VectorXd a = state.in_uv_coords.row(state.in_uv_faces(p, 0));
    Eigen::VectorXd b = state.in_uv_coords.row(state.in_uv_faces(p, 1));
    Eigen::VectorXd c = state.in_uv_coords.row(state.in_uv_faces(p, 2));
    Eigen::VectorXd d = state.in_uv_coords.row(state.in_uv_faces(p, 3));

    Eigen::VectorXd a_pow = a.array().pow(2);
    Eigen::VectorXd b_pow = b.array().pow(2);
    Eigen::VectorXd c_pow = c.array().pow(2);
    Eigen::VectorXd d_pow = d.array().pow(2);

    // find delaunay triangulation
    Eigen::Matrix3d delaunay_test;
    delaunay_test << a(0) - d(0), a(1) - d(1), (a_pow(0) - d_pow(0)) + (a_pow(1) - d_pow(1)),
      b(0) - d(0), b(1) - d(1), (b_pow(0) - d_pow(0)) + (b_pow(1) - d_pow(1)),
      c(0) - d(0), c(1) - d(1), (c_pow(0) - d_pow(0)) + (c_pow(1) - d_pow(1));
    bool abc = delaunay_test.determinant() <= 0;

    // triangles
    Eigen::VectorXd t0_o, t0_d0, t0_d1, t1_o, t1_d0, t1_d1;
    if (abc) {
      t0_o = a;
      t0_d0 = (b - a) / (state.sample_len - 1);
      t0_d1 = (c - b) / (state.sample_len - 1);

      t1_o = a;
      t1_d0 = (c - d) / (state.sample_len - 1);
      t1_d1 = (d - a) / (state.sample_len - 1);
    }
    else {
      t0_o = a;
      t0_d0 = (b - a) / (state.sample_len - 1);
      t0_d1 = (d - a) / (state.sample_len - 1);

      t1_o = d + (b - c);
      t1_d0 = (c - d) / (state.sample_len - 1);
      t1_d1 = (c - b) / (state.sample_len - 1);
    }

    for (int ui = 0; ui < state.sample_len; ui++) {
      for (int vi = 0; vi < state.sample_len; vi++) {
        // choose triangle
        bool is_t0 = false;
        if (abc) {
          if (ui > vi) is_t0 = true;
        }
        else {
          if (ui < state.sample_len - vi) is_t0 = true;
        }

        // calc subdivided points
        if (is_t0)
          state.out_uv_coords.row(n_patch_faces * p + ui * state.sample_len + vi) = t0_o + ui * t0_d0 + vi * t0_d1;
        else
          state.out_uv_coords.row(n_patch_faces * p + ui * state.sample_len + vi) = t1_o + ui * t1_d0 + vi * t1_d1;
      }
    }
  }
}


void SurfaceGenerator::setImageTexture(
  const Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic>& R,
  const Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic>& G,
  const Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic>& B,
  const Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic>& A) {
  viewer->data(0).set_uv(state.out_uv_coords);
  viewer->data(0).set_colors(Eigen::RowVector3d::Ones(3)); // otherwise old and new colors will be combined
  viewer->data(0).set_texture(R, G, B, A);
  viewer->data(0).show_texture = true;
  state.selct_col_meaning = color_meaning::image_texture;
}
