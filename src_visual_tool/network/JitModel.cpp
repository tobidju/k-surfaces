#include "JitModel.h"
#include <torch/torch.h>
#include <torch/script.h>

using namespace network;

JitModel::JitModel(std::string path) {
	jitModule = torch::jit::load(path);
  this->path = path;

  // set ctrl len and prediction mode
  auto attrs = jitModule.named_attributes();
  for (auto a : attrs) {
    if (a.name == "ctrl_len") ctrl_len = a.value.toTensor().item().toInt();
    if (a.name == "pred_mode") pred_mode = a.value.toTensor().item().toInt();
    if (a.name == "pred_bezier_ctrls") pred_mode = 1; // compatibility with old models
  }
  if (ctrl_len == -1) ctrl_len = 4; // compatibility with old models which do not define ctrl_len
  if (pred_mode == 0) pred_mode = 0; // compatibility with old models which do not define ctrl_len

  auto params = jitModule.parameters();
  for (auto p : params) {
    if (p.requires_grad()) param_count += p.numel();
  }
}


torch::Tensor JitModel::predict(torch::Tensor& uv, torch::Tensor& ctrl_feat) {
  torch::NoGradGuard no_grad;
  torch::Tensor prediction;
  inputs.clear();

  // create inputs
  if (pred_mode == 1) { // predicts bezier ctls
    auto features = torch::ivalue::Tuple::create({ uv, ctrl_feat });
    inputs.push_back(features);

    int num = jitModule.get_method("forward").num_inputs(); // compatibility with old models which need a "enable_ctrl_pred" input
    if (num == 3) {
      auto enable_ctrl_pred = torch::tensor(true);
      inputs.push_back(enable_ctrl_pred);
    }
  }
  else { // predicts surf pts or uv pts
    auto features = torch::ivalue::Tuple::create({ uv, ctrl_feat });
    inputs.push_back(features);
  }

  // predict
  prediction = jitModule.forward(inputs).toTensor();
  return prediction;
}


