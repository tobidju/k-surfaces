#pragma once
#include <igl/opengl/glfw/Viewer.h>
//#include <igl/opengl/glfw/imgui/ImGuiWidget.h>
#include <igl/opengl/glfw/imgui/ImGuiPlugin.h>
#include <functional>
#include "CtrlptPlugin.h"
#include "../network/SurfaceGenerator.h"

namespace ui {

  class SurfaceWidget : public igl::opengl::glfw::imgui::ImGuiWidget {
  public:
    SurfaceWidget(network::SurfaceGenerator* surfGen, CtrlptPlugin* ctrlPlugin) : surfGen(surfGen), ctrlPlugin(ctrlPlugin) { };
    void init(igl::opengl::glfw::Viewer* _viewer, igl::opengl::glfw::imgui::ImGuiPlugin* _plugin) override;
    void shutdown() override;
    void draw() override;


  private:
    float menu_scaling() {
      return plugin->hidpi_scaling() / plugin->pixel_ratio();
    }

    network::SurfaceGenerator* surfGen;
    CtrlptPlugin* ctrlPlugin;
    float noise[3] = {.5,.5,.5}; // noise added to ctrls on add noise click
  };

}