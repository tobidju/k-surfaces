#include "SurfaceWidget.h"
#include <imgui.h>
//#include <igl/opengl/glfw/imgui/ImGuiHelpers.h>
#include <igl/file_dialog_open.h>
#include <igl/file_dialog_save.h>
#include <igl/stb/read_image.h>
#include "../state.h"

extern struct State state;
using namespace ui;

void SurfaceWidget::init(igl::opengl::glfw::Viewer* _viewer, igl::opengl::glfw::imgui::ImGuiPlugin* _plugin)
{
  viewer = _viewer; plugin = _plugin;
}

void SurfaceWidget::shutdown() { }

void SurfaceWidget::draw()
{
  // Helper for setting viewport specific mesh options
  auto make_checkbox = [&](const char* label, unsigned int& option)
  {
    return ImGui::Checkbox(label,
      [&]() { return viewer->core().is_set(option); },
      [&](bool value) { return viewer->core().set(option, value); }
    );
  };


  // start the widget
  ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f), ImGuiCond_FirstUseEver);
  ImGui::SetNextWindowSize(ImVec2(0.0f, 0.0f), ImGuiCond_FirstUseEver);
  bool _viewer_menu_visible = true;
  ImGui::Begin(
    "Controls", &_viewer_menu_visible,
    ImGuiWindowFlags_NoSavedSettings
    | ImGuiWindowFlags_AlwaysAutoResize
  );
  ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.4f);



  // ---------- Surface options
  if (ImGui::CollapsingHeader("Surface", ImGuiTreeNodeFlags_DefaultOpen))
  {
    // Network selection
    ImGui::PushItemWidth(100 * menu_scaling());
    if (ImGui::Combo("Network", &state.selct_model, state.model_names)) {
      int new_ctrl_len = surfGen->getCtrlLen(state.selct_model);
      int new_pred_mode = surfGen->getPredictionMode(state.selct_model);
      if (
        (new_pred_mode == 2 && state.pred_mode != 2) || // we changed to pred_mode 2
        (new_pred_mode < 2 && state.pred_mode == 2) || // we changed from pred_mode 2 to another
        (state.ctrl_len != new_ctrl_len) // we changed the ctrl len
        ) {
        ctrlPlugin->clear(false); // discard ctrls
      }
      else {
        ctrlPlugin->clear(true); // keep ctls
      }

      std::vector<std::string>        model_names_with_default_color;
      std::vector<Eigen::RowVector3f> model_default_colors;
      model_names_with_default_color.push_back("K-Surface.pt\0");
      model_names_with_default_color.push_back("BezierSpline_interpolating.pt\0");
      model_default_colors.push_back(Eigen::RowVector3f(233, 196, 106) / 255.);
      model_default_colors.push_back(Eigen::RowVector3f(42, 157, 143)  / 255.);
      // std::cout << "Model name: " << state.model_names[state.selct_model] << std::endl;
      for (int i=0; i< model_names_with_default_color.size(); i++) {
        if (model_names_with_default_color[i] == state.model_names[state.selct_model]) {
            state.default_mesh_color = model_default_colors[i];
        }
      }
      surfGen->updateSurface();
    }

    // resolution
    if (ImGui::SliderInt("Sample length", &state.sample_len, 3, 64, "%d", ImGuiSliderFlags_AlwaysClamp)) { // we need at least sample_len == 5 for curvature calculation
      ctrlPlugin->clear(true);
    }
    if (state.pred_mode == 2) {
      if (ImGui::DragInt("Global-Local Iters", &state.iterations, 0.1, 1, 50, "%d", ImGuiSliderFlags_AlwaysClamp)) {
        ctrlPlugin->clear(true);
      }
    }

    // Display
    if (ImGui::Checkbox("Flat shading", &(viewer->data().face_based))) {
      viewer->data(0).dirty = igl::opengl::MeshGL::DIRTY_ALL;
    }

    if (ImGui::ColorEdit3("Default Mesh Color", state.default_mesh_color.data(), ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_PickerHueWheel)) {
      surfGen->updateSurface();
    }

    if (state.out_uv_coords.size() > 0) { // if there are uv texture coordinates available
      if (ImGui::Button("Load Texture", ImVec2(-1, 0))) {
        // set texture
        std::string fname = igl::file_dialog_open();
        Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic> R, G, B, A;
        igl::stb::read_image(fname, R, G, B, A);
        surfGen->setImageTexture(R, G, B, A);
      }
    }


    if (ImGui::Checkbox("Colored patches", &state.color_patches)) {
      surfGen->updateSurface();
    }
    if (ImGui::Checkbox("Invert normals", &(viewer->data().invert_normals))) {
      viewer->data(0).dirty |= igl::opengl::MeshGL::DIRTY_NORMAL;
    }
    make_checkbox("Wireframe", viewer->data(0).show_lines);
    if (state.device == torch::kCUDA) {
      ImGui::Checkbox("Sync cuda", &state.sync_cuda);
    }

    ImGui::Spacing();
  }


  // ----------- Ctrl_pts options
  if (ImGui::CollapsingHeader("Control Grid", ImGuiTreeNodeFlags_DefaultOpen)) {
    if (state.pred_mode == 2) {
      if (ImGui::Checkbox("Interpolate boundary", &state.interpolate_boundary)) {
        surfGen->updateSurface();
      }
    }

    make_checkbox("Show control grid", viewer->data().show_overlay);
    make_checkbox("Control grid depth", viewer->data().show_overlay_depth);

    if (state.pred_mode > 0) {
      make_checkbox("Show bezier control points", viewer->data(2).show_overlay);
    }

    if (ImGui::ColorEdit3("Input ctrl grid color", ctrlPlugin->in_overlay_color.data(), ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_PickerHueWheel)) {
      ctrlPlugin->update_overlay();
    }
    if (ImGui::InputFloat2("Point/Line thickness", ctrlPlugin->gridThickness, "%.2f")) {
      ctrlPlugin->update_overlay();
    }

    // ctrl and spline templates
    if (state.pred_mode == 2) {
      if (ImGui::ListBox("splines", &state.selct_spline, state.spline_template_names)) {
        std::string path = state.spline_template_path + state.spline_template_names[state.selct_spline] + ".obj";
        ctrlPlugin->load_spline_from_file(path);
      }
    }
    if (state.n_patches == 1) {
      if (ImGui::ListBox("grid templates", &state.selct_ctrl_templ, state.ctrl_template_names)) {
        ctrlPlugin->set_to_template(state.selct_ctrl_templ);
      }
    }


    // Reset
    if (ImGui::Button("Reset control grid", ImVec2(-1, 0))) {
      ctrlPlugin->clear(false);
    }

    ImGui::InputFloat3("Noise values", noise, "%.2f");
    if (ImGui::Button("Add noise to controls", ImVec2(-1, 0))) {
      auto noise_tensor = torch::rand_like(state.in_ctrl_pts);
      noise_tensor *= torch::tensor({ { noise[0], noise[1], noise[2] } }, noise_tensor.device());
      noise_tensor -= torch::tensor({ { noise[0], noise[1], noise[2] } }, noise_tensor.device()) / 2;
      state.in_ctrl_pts += noise_tensor;
      surfGen->updateSurface();
      ctrlPlugin->update_overlay();
    }

    if (ImGui::Button("Randomize controls", ImVec2(-1, 0))) {
      auto ctrls = torch::randn_like(state.in_ctrl_pts);
      int64_t arr[1] = { 1 };
      c10::IntArrayRef dim_arr = c10::IntArrayRef(arr, 1); // why does vector_norm not accept int like other torch func?
      ctrls /= torch::linalg::vector_norm(ctrls, 2, dim_arr, true, c10::nullopt);
      auto d = torch::rand({ ctrls.size(0), 1 }, ctrls.device()).pow(1 / 3.);
      state.in_ctrl_pts = ctrls * d;
      surfGen->updateSurface();
      ctrlPlugin->update_overlay();
    }

    ImGui::Spacing();
  }


  // ------------ options for saving and loading
  if (ImGui::CollapsingHeader("Saving and loading", ImGuiTreeNodeFlags_DefaultOpen)) {
    // Saving the surface mesh
    if (ImGui::Button("Save as .obj", ImVec2(-1, 0))) {
      //viewer->open_dialog_save_mesh();
      std::string fname = igl::file_dialog_save();
      if (fname.length() == 0) return;

      Eigen::MatrixXd cn;
      Eigen::MatrixXi ni;
      igl::writeOBJ(fname, viewer->data().V, viewer->data().F, cn, ni, state.out_uv_coords, state.out_uv_faces);
    }

    // Saving and loading ctrl grids from file
    if (ImGui::Button("Save input ctrls as .pt", ImVec2(-1, 0))) {
      std::string fname = igl::file_dialog_save();
      ctrlPlugin->save_to_file(fname, true);
    }
    if (ImGui::Button("Save bezier ctrls as .pt", ImVec2(-1, 0))) {
      std::string fname = igl::file_dialog_save();
      ctrlPlugin->save_to_file(fname, false);
    }
    if (ImGui::Button("Load input ctrls from .pt", ImVec2(-1, 0))) {
      std::string fname = igl::file_dialog_open();
      ctrlPlugin->load_from_file(fname);
    }
    //if (state.n_patches == 1) {
    //}
    if (state.pred_mode == 2) {
      if (ImGui::Button("Load spline template from .obj", ImVec2(-1, 0))) {
        std::string fname = igl::file_dialog_open();
        ctrlPlugin->load_spline_from_file(fname);
      }
    }

    ImGui::Spacing();
  }


  // ------------ mesh stats display
  if (ImGui::CollapsingHeader("Mesh Stats Display", ImGuiTreeNodeFlags_DefaultOpen)) {
    //if (state.pred_mode < 2) {
    //  if (make_checkbox("Draw reference bezier surface", viewer->data(1).is_visible)) {
    //    surfGen->updateSurface();
    //  }
    //}

    // mesh color options
    if (make_checkbox("Show mesh color", viewer->data(0).show_texture)) {
      surfGen->updateSurface();
    }

    if (state.pred_mode < 1) {
      if (ImGui::RadioButton("Bezier error", state.selct_col_meaning == color_meaning::bezier_error)) {
        state.selct_col_meaning = color_meaning::bezier_error;
        surfGen->updateSurface();
      }
    }

    if (state.pred_mode > 0) {
      if (ImGui::RadioButton("True gaussian curvature", state.selct_col_meaning == color_meaning::true_gauss_curv)) {
        state.selct_col_meaning = color_meaning::true_gauss_curv;
        surfGen->updateSurface();
      }

      if (ImGui::RadioButton("True mean curvature", state.selct_col_meaning == color_meaning::true_mean_curv)) {
        state.selct_col_meaning = color_meaning::true_mean_curv;
        surfGen->updateSurface();
      }

      if (ImGui::RadioButton("Absolute min curvature", state.selct_col_meaning == color_meaning::abs_min_curv)) {
        state.selct_col_meaning = color_meaning::abs_min_curv;
        surfGen->updateSurface();
      }
    }
    else {
      if (ImGui::RadioButton("Approx. gaussian curvature", state.selct_col_meaning == color_meaning::gauss_curv)) {
        state.selct_col_meaning = color_meaning::gauss_curv;
        surfGen->updateSurface();
      }

      if (ImGui::RadioButton("Approx. mean curvature", state.selct_col_meaning == color_meaning::mean_curv)) {
        state.selct_col_meaning = color_meaning::mean_curv;
        surfGen->updateSurface();
      }
    }

    if (state.pred_mode == 1) {
      if (ImGui::RadioButton("u curvature", state.selct_col_meaning == color_meaning::u_curv)) {
        state.selct_col_meaning = color_meaning::u_curv;
        surfGen->updateSurface();
      }
      if (ImGui::RadioButton("v curvature", state.selct_col_meaning == color_meaning::v_curv)) {
        state.selct_col_meaning = color_meaning::v_curv;
        surfGen->updateSurface();
      }
    }
    // max value for error color visualization
    ImGui::PushItemWidth(80 * menu_scaling());
    if (ImGui::DragFloat("Color scaling", &state.color_scale, 0.001f)) {
      surfGen->updateSurface();
    }

    // line visualization options
    if (make_checkbox("Show lines for stats", viewer->data(3).show_overlay)) {
      surfGen->updateSurface();
    }
    if (ImGui::RadioButton("Principal Curvature", state.selct_line_meaning == line_meaning::princ_curv)) {
      state.selct_line_meaning = line_meaning::princ_curv;
      surfGen->updateSurface();
    }
    if (state.pred_mode != 0 && state.ctrl_len == 3) {
      if (ImGui::RadioButton("Max curvature in u and v", state.selct_line_meaning == line_meaning::line_curv_maxs)) {
        state.selct_line_meaning = line_meaning::line_curv_maxs;
        surfGen->updateSurface();
      }
    }
    if (ImGui::RadioButton("Vertex normals", state.selct_line_meaning == line_meaning::normals)) {
      state.selct_line_meaning = line_meaning::normals;
      surfGen->updateSurface();
    }
    if (state.pred_mode == 2) {
      if (ImGui::RadioButton("Tangents", state.selct_line_meaning == line_meaning::tangents)) {
        state.selct_line_meaning = line_meaning::tangents;
        surfGen->updateSurface();
      }
    }
    ImGui::DragFloat("Line width", &viewer->data(3).line_width, 0.1f, 0.f, 10.f, "%.1f");
    make_checkbox("Stats line depth", viewer->data(3).show_overlay_depth);

    ImGui::Spacing();
  }


  // ------------- Viewing options
  if (ImGui::CollapsingHeader("Viewing Options", ImGuiTreeNodeFlags_DefaultOpen))
  {
    if (ImGui::Button("Center object", ImVec2(-1, 0)))
    {
      viewer->core().align_camera_center(viewer->data().V, viewer->data().F);
    }
    if (ImGui::Button("Snap canonical view", ImVec2(-1, 0)))
    {
      viewer->snap_to_canonical_quaternion();
    }

    // Zoom
    ImGui::PushItemWidth(80 * menu_scaling());
    ImGui::DragFloat("Zoom", &(viewer->core().camera_zoom), 0.05f, 0.1f, 20.0f);

    // Select rotation type
    int rotation_type = static_cast<int>(viewer->core().rotation_type);
    static Eigen::Quaternionf trackball_angle = Eigen::Quaternionf::Identity();
    static bool orthographic = true;
    if (ImGui::Combo("Camera Type", &rotation_type, "Trackball\0Two Axes\0002D Mode\0\0"))
    {
      using RT = igl::opengl::ViewerCore::RotationType;
      auto new_type = static_cast<RT>(rotation_type);
      if (new_type != viewer->core().rotation_type)
      {
        if (new_type == RT::ROTATION_TYPE_NO_ROTATION)
        {
          trackball_angle = viewer->core().trackball_angle;
          orthographic = viewer->core().orthographic;
          viewer->core().trackball_angle = Eigen::Quaternionf::Identity();
          viewer->core().orthographic = true;
        }
        else if (viewer->core().rotation_type == RT::ROTATION_TYPE_NO_ROTATION)
        {
          viewer->core().trackball_angle = trackball_angle;
          viewer->core().orthographic = orthographic;
        }
        viewer->core().set_rotation_type(new_type);
      }
    }

    // Orthographic view
    ImGui::Checkbox("Orthographic view", &(viewer->core().orthographic));
    ImGui::ColorEdit4("Background color", viewer->core(0).background_color.data(),
      ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_PickerHueWheel);
    ImGui::PopItemWidth();
    ImGui::Spacing();
  }

  ImGui::PopItemWidth();
  ImGui::End();
}
