#include "CtrlptPlugin.h"
#include <Eigen/Core>
#include <igl/readOBJ.h>
#include <GLFW/glfw3.h>
#include <torch/torch.h>
#include "../state.h"
#include "../shapes/uv/uv.h"
#include "../utils/utils.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>

extern struct State state;
using namespace ui;


void CtrlptPlugin::init(igl::opengl::glfw::Viewer* viewer) {
	this->viewer = viewer;

	// in overlay setup
	viewer->data().point_size = gridThickness[0] * overlay_scale();
	viewer->data().line_width = gridThickness[1] * overlay_scale(); // libigl says line_width is not support for Windows, but it works
	Eigen::MatrixXd ctrl_pts = utils::torchToEigen<double>(state.in_ctrl_pts);
	viewer->data().add_points(ctrl_pts, in_overlay_color.cast<double>());

	auto edge_start = ctrl_pts(state.in_ctrl_connect.col(0), Eigen::placeholders::all);
	auto edge_end = ctrl_pts(state.in_ctrl_connect.col(1), Eigen::placeholders::all);
	viewer->data().add_edges(edge_start, edge_end, in_overlay_color.cast<double>());

	// out overlay setup
	viewer->core().set(viewer->data(2).show_overlay, false);
	viewer->data(2).point_size = gridThickness[0] * overlay_scale();
	viewer->data(2).line_width = gridThickness[1] * overlay_scale();
}


 bool CtrlptPlugin::mouse_down(int button, int modifier){
	 if (button == this->mouse_button) {
		 is_mouse_down = true;
		 selct_ctrl_idx = -1;
	 }
	 return false;
 }


 bool CtrlptPlugin::mouse_move(int mouse_x, int mouse_y) {
	 if (is_mouse_down) {
		 auto viewCore = viewer->core();
		 if (selct_ctrl_idx == -1) {

			 // find nearest ctrlpt
			 Eigen::MatrixXf ctrlpts = utils::torchToEigen<float>(state.in_ctrl_pts);
			 Eigen::MatrixXf ctrlpts_h = ctrlpts.rowwise().homogeneous().transpose();
			 Eigen::MatrixXf ctrlpts_screen_h = viewCore.proj * viewCore.view * ctrlpts_h;
			 Eigen::MatrixXf ctrlpts_screen = ctrlpts_screen_h.colwise().hnormalized();
			 ctrlpts_screen.row(0) = (ctrlpts_screen.array().row(0) * .5 + .5) * viewCore.viewport(2) + viewCore.viewport(0);
			 ctrlpts_screen.row(1) = (ctrlpts_screen.array().row(1) * -.5 + .5) * viewCore.viewport(3) + viewCore.viewport(1);

			 Eigen::MatrixXf distances = (ctrlpts_screen.topRows(2).colwise() - Eigen::Vector2f(mouse_x, mouse_y)).colwise().norm();
			 int min_idx = utils::eigenToTorch(distances).argmin().item<int>(); // why does Eigen has no argmin?
			 float min_distance = gridThickness[0] * overlay_scale() + 10.; // additional padding makes dragging easier
			 if (distances(min_idx) > min_distance) { // too far away. Stop and continue with default trackball control
				 is_mouse_down = false;
				 return false;
			 }
			 selct_ctrl_idx = min_idx;
			 d_buf = ctrlpts_screen_h(2, selct_ctrl_idx) / ctrlpts_screen_h(3, selct_ctrl_idx);
		 }

		 // calculate new position
		 Eigen::MatrixXf grid_pts = utils::torchToEigen<float>(state.in_ctrl_pts); // TODO remove
		 float mx = (mouse_x / viewCore.viewport(2)) * 2 - 1 + (viewCore.viewport(0) / viewCore.viewport(2));
		 float my = (-mouse_y / viewCore.viewport(3)) * 2 + 1 + (viewCore.viewport(1) / viewCore.viewport(3));
		 Eigen::Vector4f new_screen(mx, my, d_buf, 1);
		 Eigen::RowVector4f new_pos_h = (viewCore.proj * viewCore.view).inverse() * new_screen;
		 Eigen::MatrixXf new_pos = new_pos_h.hnormalized();
		 grid_pts.row(selct_ctrl_idx) = new_pos;

		 // auto centering
		 if (state.auto_center) {
			 Eigen::RowVector3f mean = grid_pts.colwise().mean();
			 grid_pts = grid_pts.rowwise() - mean;
		 }

		 // update state and viewer
		 state.in_ctrl_pts = utils::eigenToTorch(grid_pts).to(state.in_ctrl_pts.device());
		 surfGen->updateSurface();
		 update_overlay();
		 state.selct_ctrl_templ = -1;
		 state.selct_spline = -1;
		 return true;
	 }
	 return false;
 }


 bool CtrlptPlugin::mouse_up(int button, int modifier) {
	 if (button == this->mouse_button && is_mouse_down) {
		 is_mouse_down = false;
		 return true;
	 }
	 return false;
 }


 void CtrlptPlugin::update_overlay() {
	 // update in_ctrls overlay
	 Eigen::MatrixXd ctrl_eig = utils::torchToEigen<double>(state.in_ctrl_pts);
	 viewer->data().set_points(ctrl_eig, in_overlay_color.cast<double>());
	 viewer->data().set_edges(ctrl_eig, state.in_ctrl_connect, in_overlay_color.cast<double>());
	 viewer->data().point_size = gridThickness[0] * overlay_scale();
	 viewer->data().line_width = gridThickness[1] * overlay_scale();

	 // update out_ctrls overlay
	 if (state.pred_mode > 0) {
		 Eigen::MatrixXd out_ctrls = utils::torchToEigen<double>(state.out_ctrl_pts);
		 viewer->data(2).set_points(out_ctrls, out_overlay_color.cast<double>());
		 viewer->data(2).set_edges(out_ctrls, state.out_ctrl_connect, out_overlay_color.cast<double>());
	 }
	 viewer->data(2).point_size = gridThickness[0] * overlay_scale();
	 viewer->data(2).line_width = gridThickness[1] * overlay_scale();
 }


 void CtrlptPlugin::clear(bool keep_ctrls) {
	 surfGen->clear(keep_ctrls);

	 viewer->data().clear_points();
	 viewer->data().clear_edges();
	 viewer->data(2).clear_edges(); //predicted ctrl pts overlays
	 viewer->data(2).clear_points();

	 // init in_ctrls overlay
	 Eigen::MatrixXd ctrl_pts = utils::torchToEigen<double>(state.in_ctrl_pts);
	 viewer->data().add_points(ctrl_pts, in_overlay_color.cast<double>());

	 auto edge_start = ctrl_pts(state.in_ctrl_connect.col(0), Eigen::placeholders::all);
	 auto edge_end = ctrl_pts(state.in_ctrl_connect.col(1), Eigen::placeholders::all);
	 viewer->data().add_edges(edge_start, edge_end, in_overlay_color.cast<double>());

	 // init out_ctrls overlay
	 if (state.pred_mode > 0) {
		 Eigen::MatrixXd out_ctrls = utils::torchToEigen<double>(state.out_ctrl_pts);
		 viewer->data(2).set_points(out_ctrls, out_overlay_color.cast<double>());
		 viewer->data(2).set_edges(out_ctrls, state.out_ctrl_connect, out_overlay_color.cast<double>());
	 }
 }


 void CtrlptPlugin::load_from_file(const std::string& fname) {
	 if (fname.length() == 0) return;

	 torch::Tensor ctrl_grid;
   // torch::load does not work for tensors saved with torch.save in python, see https://github.com/pytorch/pytorch/issues/20356
	 // TODO can we do better than try catch?
	 try { // if saved with torch::save in cpp
		 auto foo = torch::jit::load(fname);
		 ctrl_grid = *foo.parameters().begin();
	 }
	 catch (...) { // if saved with torch.save in python
		 std::ifstream inFile(fname, std::ios_base::binary);

		 inFile.seekg(0, std::ios_base::end);
		 size_t length = inFile.tellg();
		 inFile.seekg(0, std::ios_base::beg);

		 std::vector<char> buffer;
		 buffer.reserve(length);
		 std::copy(std::istreambuf_iterator<char>(inFile), std::istreambuf_iterator<char>(), std::back_inserter(buffer));
		 torch::IValue data = torch::pickle_load(buffer);
		 ctrl_grid = data.toTensor();
	 }

	 //using namespace torch::indexing;
	 if (ctrl_grid.dim() == 3) {
		 ctrl_grid = ctrl_grid.unsqueeze(0);
	 }

	 if (state.n_patches == 1 && ctrl_grid.size(0) > 1) { // if we found multiple ctrl grids but only have a single patch we take the first
		 ctrl_grid = ctrl_grid.index({ 0 }).unsqueeze(0);
	 }

	 if (ctrl_grid.size(0) == state.n_patches &&
		 ctrl_grid.size(1) == state.ctrl_len &&
		 ctrl_grid.size(2) == state.ctrl_len &&
		 ctrl_grid.size(3) == 3) {
		 ctrl_grid = ctrl_grid.to(state.device);
		 surfGen->setCtrlsFromGrids(ctrl_grid, true);
		 surfGen->updateSurface();
		 update_overlay();
		 return;
	 }
	 std::cout << "Ctrl grid in file is not compatible with current model." << std::endl;
 }


 void CtrlptPlugin::save_to_file(const std::string& fname, bool in_ctrls) {
	 if (fname.length() == 0) return;

	 auto ctrls = surfGen->getCtrlsAsGrids(in_ctrls);
	 torch::save(ctrls, fname); // this is more like torch.jit.save than torch.save
 }


 void CtrlptPlugin::load_spline_from_file(const std::string& fname) {
	 if (fname.length() == 0) return;

	 Eigen::MatrixXf V;
	 Eigen::MatrixXd TC, CN;
	 Eigen::MatrixXi F, FTC, FN;

	 igl::readOBJ(fname, V, TC, CN, F, FTC, FN);

	 if (F.cols() != 4) {
		 std::cout << "Wrong input. " << fname << " is not a quad mesh.";
		 return;
	 }
	 torch::Tensor VT = utils::eigenToTorch(V);
	 torch::Tensor FT = utils::eigenToTorch(F); // TODO does not support long?

	 using namespace torch::indexing;
	 torch::Tensor tmp = VT.index({ Slice(), 1 }).clone(); // swap Y and Z axis
	 VT.index_put_({ Slice(), 1 }, -VT.index({ Slice(), 2 }));
	 VT.index_put_({ Slice(), 2 }, tmp);
	 VT -= VT.mean(0);
	 VT /= VT.abs().max() * 2;

	 bool success = surfGen->processSplineGrid(VT, FT);

	 if (success) {
		 if (TC.size() > 0) {
			 state.in_uv_coords = TC;
			 state.in_uv_faces = FTC;
		 }
		 else {
			 state.in_uv_coords = Eigen::MatrixXd(0, 0);
			 state.in_uv_faces = Eigen::MatrixXi(0, 0);
		 }
		 surfGen->clear(true); // but keep the processed spline grid
		 update_overlay();
	 }
 }


 float CtrlptPlugin::overlay_scale() { // taken/modified from ImGuiPlugin.cpp
	 // Computes scaling factor for hidpi devices
	 float xscale, yscale;
	 GLFWwindow* window = glfwGetCurrentContext();
	 glfwGetWindowContentScale(window, &xscale, &yscale);
	 float hdpi_scale = 0.5 * (xscale + yscale);

	 // Computes pixel ratio for hidpi devices
	 int buf_size[2];
	 int win_size[2];
	 glfwGetFramebufferSize(window, &buf_size[0], &buf_size[1]);
	 glfwGetWindowSize(window, &win_size[0], &win_size[1]);
	 float pixel_ratio = (float)buf_size[0] / (float)win_size[0];
	 return hdpi_scale / pixel_ratio;
 }


 void CtrlptPlugin::set_to_template(int i) {
	 if (state.n_patches != 1) return;
	 if (state.pred_mode == 2 && i == -1) return; // pred_mode 2 needs a more advanced reset!

	 using namespace torch::indexing;
	 torch::Tensor ctrl_pts = surfGen->getCtrlsAsGrids(true).flatten(0,2);
	 ctrl_pts = ctrl_pts.to(torch::kCPU);

	 switch (i) {
	 case -1: // reset
		 ctrl_pts = shapes::uv::uniformGrid(state.ctrl_len, -.5, .5, true);
		 state.selct_ctrl_templ = -1;
		 break;

	 case 0: { // Paraboloid
		 ctrl_pts = shapes::uv::uniformGrid(state.ctrl_len, -.5, .5, true);
		 auto xy = shapes::uv::uniformGrid(state.ctrl_len, -1., 1.);
		 auto offset = .5 * xy.pow(2.);
		 offset = offset.sum(1);
		 ctrl_pts.index_put_({ Slice(), 2 }, offset);
		 break;
	 }

	 case 1: { // high diagonal
		 ctrl_pts = shapes::uv::uniformGrid(state.ctrl_len, -.5, .5, true);
		 ctrl_pts.index_put_({ Slice(0, None, state.ctrl_len + 1), 2 }, 2);
		 break;
	 }

	 case 2: { // shift first v row
		 ctrl_pts = shapes::uv::uniformGrid(state.ctrl_len, -.5, .5, true);
		 ctrl_pts.index_put_({ Slice(1, None, state.ctrl_len), 1 }, -1);
		 ctrl_pts.index_put_({ Slice(1, None, state.ctrl_len), 2 }, .5);
		 break;
	 }

	 case 3: { // twisted
		 ctrl_pts = shapes::uv::uniformGrid(state.ctrl_len, -.5, .5, true);
		 for (int i = 0; i < state.ctrl_len; i++) {
			 float theta = i * igl::PI / (state.ctrl_len - 1);
			 auto rotmat = torch::tensor({
				 {cos(theta), 0.f, -sin(theta)},
				 {0.f, 1.f, 0.f},
				 {sin(theta), 0.f, cos(theta)}
			 });
			 auto rot = ctrl_pts.index({ Slice(i, None, state.ctrl_len) });
			 rot = torch::matmul(rot, rotmat);
			 ctrl_pts.index_put_({ Slice(i, None, state.ctrl_len) }, rot);
		 }
		 break;
	 }

	 case 4: { // saddle
		 ctrl_pts = shapes::uv::uniformGrid(state.ctrl_len, -.5, .5, true);
		 torch::Tensor z_val = -torch::linspace(-1, 1, state.ctrl_len).pow(2);
		 z_val += z_val.index({ 0 }).item();
		 // u = min
		 auto row = ctrl_pts.index({ Slice(0, None, state.ctrl_len), 2 });
		 ctrl_pts.index_put_({ Slice(0, None, state.ctrl_len) , 2 }, row - z_val);
		 // u = max
		 row = ctrl_pts.index({ Slice(state.ctrl_len -1, None, state.ctrl_len), 2 });
		 ctrl_pts.index_put_({ Slice(state.ctrl_len -1, None, state.ctrl_len) , 2 }, row - z_val);
		 // v = min
		 row = ctrl_pts.index({ Slice(0, state.ctrl_len), 2 });
		 ctrl_pts.index_put_({ Slice(0, state.ctrl_len) , 2 }, row + z_val);
		 // v = max
		 int idx = pow(state.ctrl_len, 2) - state.ctrl_len;
		 row = ctrl_pts.index({ Slice(idx, None), 2 });
		 ctrl_pts.index_put_({ Slice(idx, None) , 2 }, row + z_val);
		 break;
	 }

	 case 5: { // 90 deg X rotation
		 float rad = igl::PI / 2.;
		 auto rotation = torch::tensor({
		 {1.f, 0.f, 0.f},
		 {0.f, (float) cos(rad), (float) sin(rad)},
		 {0.f, (float) -sin(rad), (float) cos(rad)} });
		 ctrl_pts = ctrl_pts.matmul(rotation);
		 break;
	 }

	 case 6: { // 90 deg Y rotation
		 float rad = igl::PI / 2.;
		 auto rotation = torch::tensor({
		 {(float) cos(rad), 0.f, (float) sin(rad)},
		 {0.f, 1.f, 0.f},
		 {(float) -sin(rad), 0.f, (float) cos(rad)} });
		 ctrl_pts = ctrl_pts.matmul(rotation);
		 break;
	 }

	 case 7: { // 90 deg Z rotation
		 float rad = igl::PI / 2.;
		 auto rotation = torch::tensor({
		 {(float) cos(rad), (float) sin(rad), 0.f},
		 {(float) -sin(rad), (float) cos(rad), 0.f},
		 {0.f, 0.f, 1.f} });
		 ctrl_pts = ctrl_pts.matmul(rotation);
		 break;
	 }

	 case 8: { // transposed
		 auto ctrl_grid = ctrl_pts.view({ state.ctrl_len, state.ctrl_len, 3 });
		 ctrl_pts = ctrl_grid.transpose(0, 1).flatten(0, 1);
		 break;
	 }

	 default:
		 break;
	 }

	 ctrl_pts -= ctrl_pts.mean(0);
	 ctrl_pts = ctrl_pts.view({ -1, state.ctrl_len, state.ctrl_len, 3 }).to(state.device);
	 surfGen->setCtrlsFromGrids(ctrl_pts, true);
	 surfGen->updateSurface();
	 update_overlay();
 }
