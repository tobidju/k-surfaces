#pragma once
#include <Eigen/Core>
#include <torch/script.h>
#include <igl/opengl/glfw/ViewerPlugin.h>
#include <igl/opengl/glfw/Viewer.h>
#include <functional>
#include "../network/SurfaceGenerator.h"

typedef torch::jit::script::Module Module;

namespace ui {

	class CtrlptPlugin : public igl::opengl::glfw::ViewerPlugin {

	public:
		CtrlptPlugin(network::SurfaceGenerator* surfGen) : surfGen(surfGen){
			plugin_name = "CtrlptPlugin";
		};

		void init(igl::opengl::glfw::Viewer* viewer) override;
		bool mouse_down(int button, int modifier) override;
		bool mouse_up(int button, int modifier) override;
		bool mouse_move(int mouse_x, int mouse_y) override;
		void update_overlay();

		// sets ctrl_pts to a template
		// -1 = reset, 0 = "Paraboloid", 1 = "High diagonal", etc see state.h and all other = center current only
		void set_to_template(int id);

		void clear(bool keep_ctrls); // call this if the model, uv resolution or ctrl_len has changed (if keep_ctrls == true, the ctrls should not be cleared); also clears the surfGen

		void load_from_file(const std::string& fname); // load first ctrl grid from .pt file as in_ctrls (only for state.n_patches == 1)
		void save_to_file(const std::string& fname, bool in_ctrls = true); // save ctrl grids to .pt file (in_ctrls - whether in or out_ctrls should be saved)
		void load_spline_from_file(const std::string& fname); // loads and generates ctrls for a spline from a quad mesh in fname

		// options
		int mouse_button = 0; // left mouse button
		Eigen::RowVector3f in_overlay_color = Eigen::RowVector3f(0.337, 0.518, 1.); // imgui wants floats, libigl doubles
		Eigen::RowVector3f out_overlay_color = Eigen::RowVector3f(.6, .6, .6);
		float gridThickness[2] = { 12., 0.5 }; // first is point size, second is line width


	private:
		float overlay_scale();

		network::SurfaceGenerator* surfGen; // to call updateSurface()
		// helpers
		bool is_mouse_down = false;
		int selct_ctrl_idx;

		float d_buf;
	};

}