#include "StatsWidget.h"
#include <imgui.h>
//#include <igl/opengl/glfw/imgui/ImGuiHelpers.h>
#include <igl/file_dialog_open.h>
#include <igl/file_dialog_save.h>
#include "../state.h"

extern struct State state;
using namespace ui;

void StatsWidget::init(igl::opengl::glfw::Viewer* _viewer, igl::opengl::glfw::imgui::ImGuiPlugin* _plugin)
{
  viewer = _viewer; plugin = _plugin;
}

void StatsWidget::shutdown() { }

void StatsWidget::draw()
{
  // start the widget
  float widget_size = 250.f;
  ImGui::SetNextWindowPos(ImVec2((screen_width() - widget_size) * menu_scaling(), 0.0f), ImGuiCond_FirstUseEver);
  ImGui::SetNextWindowSize(ImVec2(widget_size * menu_scaling(), 0.0f), ImGuiCond_FirstUseEver);
  bool _viewer_menu_visible = true;
  ImGui::Begin(
    "Statistics", &_viewer_menu_visible,
    ImGuiWindowFlags_NoSavedSettings
    | ImGuiWindowFlags_AlwaysAutoResize
    | ImGuiWindowFlags_NoBackground
  );
  ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.4f);
  ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.,0.,0.,1.));


  if (state.device == torch::kCPU) {
    ImGui::Text("Device: CPU");
  }
  else {
    ImGui::Text("Device: CUDA");
  }
  switch (state.pred_mode )
  {
  case 0:
    ImGui::Text("Prediction: surface points");
    break;
  case 1:
    ImGui::Text("Prediction: Bezier control grid");
    break;
  case 2:
    ImGui::Text("Prediction: uv coordinates");
    break;
  default:
    break;
  }
  ImGui::Text("Trainable params: %d", state.param_count);
  ImGui::Text("Patches: %d", state.n_patches);
  ImGui::Spacing();

  ImGui::Text("Timings (ms.):");
  ImGui::BulletText("Total prediction: %.2f", state.total_time * time_scale);
  ImGui::BulletText("Avg. local step: %.2f", state.local_time * time_scale);
  ImGui::BulletText("Avg. global step: %.2f", state.global_time * time_scale);
  ImGui::BulletText("Stats calculation: %.2f", state.stats_time * time_scale);

  ImGui::PopStyleColor();
  ImGui::PopItemWidth();
  ImGui::End();
}
