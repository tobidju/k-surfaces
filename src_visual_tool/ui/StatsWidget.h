#pragma once
#include <igl/opengl/glfw/Viewer.h>
//#include <igl/opengl/glfw/imgui/ImGuiWidget.h>
#include <igl/opengl/glfw/imgui/ImGuiPlugin.h>
#include <functional>
#include <GLFW/glfw3.h>
#include "CtrlptPlugin.h"

namespace ui {

  class StatsWidget : public igl::opengl::glfw::imgui::ImGuiWidget {
  public:
    StatsWidget() { };
    void init(igl::opengl::glfw::Viewer* _viewer, igl::opengl::glfw::imgui::ImGuiPlugin* _plugin) override;
    void shutdown() override;
    void draw() override;


  private:
    float menu_scaling() {
      return plugin->hidpi_scaling() / plugin->pixel_ratio();
    }

    float screen_width() {
      int win_size[2];
      GLFWwindow* window = glfwGetCurrentContext();
      glfwGetWindowSize(window, &win_size[0], &win_size[1]);
      return (float)win_size[0];
    }

    float time_scale = 0.001f; // scale time from microseconds to milliseconds
  };

}