#include <torch/torch.h>
#include <Eigen/Core>
#include <iostream>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/opengl/glfw/imgui/ImGuiPlugin.h>
#include "ui/CtrlptPlugin.h"
#include "ui/SurfaceWidget.h"
#include "ui/StatsWidget.h"
#include "network/SurfaceGenerator.h"
#include "state.h"

State state = State(); // global state. All options are saved here. See State.h

int main(int argc, char *argv[])
{
  // create viewer (most setup of the viewer is done by SurfaceGenerator)
  igl::opengl::glfw::Viewer viewer;
  viewer.core().background_color = Eigen::RowVector4f(1.0, 1.0, 1., 0.); //Eigen::RowVector4f(.25, .25, .25, 0.);
  viewer.core().set_rotation_type(igl::opengl::ViewerCore::RotationType::ROTATION_TYPE_TRACKBALL);

  // surface generation
  network::SurfaceGenerator surfGen(&viewer);

  // ctrl point overlays
  ui::CtrlptPlugin ctrlPlugin(&surfGen);
  viewer.plugins.push_back(&ctrlPlugin);

  // imgui widgets
  igl::opengl::glfw::imgui::ImGuiPlugin imguiPlugin;
  viewer.plugins.push_back(&imguiPlugin);
  ui::SurfaceWidget surfWidget(&surfGen, &ctrlPlugin);
  ui::StatsWidget statsWidget;
  imguiPlugin.widgets.push_back(&statsWidget);
  imguiPlugin.widgets.push_back(&surfWidget);

  viewer.launch(false, "visualization tool");
  return 0;
}
