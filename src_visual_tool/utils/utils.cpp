#include "utils.h"

torch::Tensor utils::cuberoot(torch::Tensor value) {
	return value.sign() * value.abs().pow(1. / 3.);
}

torch::Tensor utils::cubic_roots(torch::Tensor a, torch::Tensor b, torch::Tensor c, torch::Tensor d) {
	// TODO solve square and linears if not cubic
	auto p = (3 * a * c - b.pow(2)) / (3 * a.pow(2));
	auto q = (2 * b.pow(3) - 9 * a * b * c + 27 * a.pow(2) * d) / (27 * a.pow(3));

	// three real roots
	q.unsqueeze_(1); p.unsqueeze_(1);
	auto b_unsq = b.unsqueeze(1);
	auto a_unsq = a.unsqueeze(1);
	auto k = torch::tensor({ {0.f,1.f,2.f} }, a.device());
	auto t_multi = 2 * torch::sqrt(-p / 3) * torch::cos(1. / 3. * torch::arccos(3 * q / (2 * p) * torch::sqrt(-3 / p)) - 2 * M_PI * k / 3.) - b_unsq / (3 * a_unsq);

	// single root
	auto t_single = cuberoot(-q / 2 + torch::sqrt(q.pow(2) / 4 + p.pow(3) / 27)) + cuberoot(-q / 2 - torch::sqrt(q.pow(2) / 4 + p.pow(3) / 27)) - b_unsq / (3 * a_unsq);

	// select root
	auto t = torch::where(t_multi.isnan(), t_single.expand({ -1, 3 }), t_multi);
	return t;
}
