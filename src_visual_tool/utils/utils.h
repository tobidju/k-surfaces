#pragma once
#include <torch/torch.h>
#include <Eigen/Core>
#include <math.h>

namespace utils {

	template <typename T>
	torch::Tensor eigenToTorch(Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& in) {
		T* data = in.data();
		auto dtype = at::typeMetaToScalarType(caffe2::TypeMeta::Make<T>());
		at::Tensor out = torch::from_blob(data, { in.cols(), in.rows() }, dtype);
		return out.transpose(0, 1).clone(); // always copy?
	};


	template <typename T>
	Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> torchToEigen(const at::Tensor& in) { // TODO not only float Tensors?
		if (in.dim() > 2) {
			throw std::invalid_argument("Can not convert a tensor with more than 2 dims to an Eigen Matrix");
		}
		torch::Tensor in_cpu = in.contiguous().to(at::kCPU);
		float* data = in_cpu.data_ptr<float>();
		if (in.dim() == 1) {
			Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> out(data, in_cpu.size(0), 1);
			return out.cast<T>(); // TODO not always copy
		}
		Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> out(data, in_cpu.size(0), in_cpu.size(1));
		return out.cast<T>(); // TODO not always copy
	};


	torch::Tensor cuberoot(torch::Tensor value);

	torch::Tensor cubic_roots(torch::Tensor a, torch::Tensor b, torch::Tensor c, torch::Tensor d);
}
