#pragma once
#include <torch/torch.h>

// see curve.py for documentation

namespace shapes {
	namespace curve {
		class BezierCurv {
		public:
			BezierCurv(int degree) : degree{ degree } { };

			torch::Tensor eval(torch::Tensor ctrl_pts, torch::Tensor t);

			// ctrl_pts is of shape(curves to evaluate x 3 (== ctrl_len) x 3), limit = true limits the params to [0,1]
			torch::Tensor max_curvature(torch::Tensor ctrl_pts, bool params = false, bool limit = true, float eps = 1e-5);
			torch::Tensor curvature(torch::Tensor ctrl_pts, torch::Tensor t);
			torch::Tensor target_max_curvature(torch::Tensor c0, torch::Tensor c2, torch::Tensor target); // equivalent to python version but returns t instead of c1 and c0 is (nx3)

		private:
			torch::Tensor bernstein_basis(torch::Tensor u, int derivative = 0);

			int degree;
		};
	}
}