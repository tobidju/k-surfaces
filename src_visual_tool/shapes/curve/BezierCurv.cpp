#include "BezierCurv.h"
#include "../../utils/utils.h"

using namespace shapes::curve;
using namespace torch::indexing;

torch::Tensor BezierCurv::eval(torch::Tensor ctrl_pts, torch::Tensor t) {
	torch::Tensor basis = bernstein_basis(t);
	torch::Tensor curv_pts;
	if (t.dim() == 1) {
		curv_pts = torch::einsum("nu,cud->cnd", { basis, ctrl_pts });
	}
	else {
		curv_pts = torch::einsum("cnu,cud->cnd", { basis, ctrl_pts });
	}

	return curv_pts;
}



torch::Tensor BezierCurv::max_curvature(torch::Tensor ctrl_pts, bool params, bool limit, float eps) {
	assert(degree == 3);

	auto c0 = ctrl_pts.index({ Slice(), 0 });
	auto c1 = ctrl_pts.index({ Slice(), 1 });
	auto c2 = ctrl_pts.index({ Slice(), 2 });

	auto num = torch::einsum("ni,ni->n", { c0 - c1, c0 - 2 * c1 + c2 });
	auto de_num = torch::pow(c0 - 2 * c1 + c2, 2).sum(1);
	auto t = num / (de_num + eps);

	// limit to [0,1] range
	if (limit) {
		t.index_put_({ t < 0. }, 0.);
		t.index_put_({ t > 1. }, 1.);
	}

	if (params) {
		return t;
	}
	auto pts = eval(ctrl_pts, t.unsqueeze(1));
	return pts.squeeze(1);
}


torch::Tensor BezierCurv::target_max_curvature(torch::Tensor c0, torch::Tensor c2, torch::Tensor target) {
	torch::Tensor a = torch::norm(c2 - c0, 2, 1).pow(2);
	torch::Tensor b = torch::einsum("ni,ni->n", { 3 * (c2 - c0), c0 - target });
	torch::Tensor c = torch::einsum("ni,ni->n", { 3 * c0 - 2 * target - c2, c0 - target });
	torch::Tensor d = -torch::norm(c0 - target, 2, 1).pow(2);

	torch::Tensor t = utils::cubic_roots(a, b, c, d);
	t = torch::gather(t, 1, (t - .5).abs().argmin(1).unsqueeze(1));
	return t.squeeze(1);
}



torch::Tensor BezierCurv::bernstein_basis(torch::Tensor t, int derivative) {
	auto device = t.device();
	torch::Tensor t_basis;
	if (t.dim() == 1) {
		t_basis = t.unsqueeze(-1).expand({ -1, degree });
	}
	else {
		t_basis = t.unsqueeze(-1).expand({ -1, -1, degree });
	}
	//torch::Tensor t_exp = torch::arange(0, degree, device);
	auto t_exp = torch::cat({ torch::zeros(derivative, t.device()), torch::arange(0, degree - derivative, t.device()) });
	torch::Tensor monomials = torch::pow(t_basis, t_exp);
	for (int i = 0; i < derivative; i++) {
		monomials = monomials * (torch::arange(0, degree, t.device()) - i);
	}

	torch::Tensor bernstein_matrix;
	if (degree == 3) {
		bernstein_matrix = torch::tensor({
			{1., -2., 1.},
			{0., 2., -2.},
			{0., 0., 1.},
			}, device);
	}
	else if (degree == 4) {
		bernstein_matrix = torch::tensor({
			{1., -3., 3., -1.},
			{0., 3., -6., 3.},
			{0., 0., 3., -3.},
			{0., 0., 0., 1.}
			}, device);
	}

	torch::Tensor basis = torch::einsum("...v,uv->...u", { monomials, bernstein_matrix });
	return basis;
}


torch::Tensor BezierCurv::curvature(torch::Tensor ctrl_pts, torch::Tensor t) {
	auto b_r_t = bernstein_basis(t, 1);
	auto b_r_tt = bernstein_basis(t, 2);
	torch::Tensor r_t, r_tt;
	if (t.dim() == 1) {
		r_t = torch::einsum("nu,cud->cnd", { b_r_t, ctrl_pts });
		r_tt = torch::einsum("nu,cud->cnd", { b_r_tt, ctrl_pts });
	}
	else {
		r_t = torch::einsum("cnu,cud->cnd", { b_r_t, ctrl_pts });
		r_tt = torch::einsum("cnu,cud->cnd", { b_r_tt, ctrl_pts });
	}
	auto num = torch::norm(torch::cross(r_t, r_tt, -1), 2, -1);
	auto de_num = torch::norm(r_t, 2, -1).pow(3);
	auto curv = num / de_num;
	return curv.nan_to_num();
}
