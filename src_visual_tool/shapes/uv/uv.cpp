#include "uv.h"
#include <torch/torch.h>
#include <cmath>

using namespace shapes;

torch::Tensor uv::uniformGrid(int length, float min, float max, bool embed3d) {
	auto grid_axis = torch::linspace(min, max, length);
	auto grid = torch::meshgrid({ grid_axis, grid_axis }, "xy");
	torch::Tensor grid_pts;
	if (embed3d) {
		auto zeros = torch::zeros(pow(length, 2));
		grid_pts = torch::stack({ grid[1].flatten(), grid[0].flatten(), zeros });
	}
	else {
		grid_pts = torch::stack({ grid[1].flatten(), grid[0].flatten()});
	}

	return torch::transpose(grid_pts, 0, 1);
}


torch::Tensor uv::gridFaces(int length) {
	using namespace torch::indexing;
	auto indices = torch::arange(0, pow(length, 2)).view({ length, length });
	auto top_left = indices.index({ Slice(None, length -1), Slice(0, length -1) }).flatten();
	auto top_right = top_left + 1;
	auto bottom_left = indices.index({ Slice(1, length), Slice(None, length -1) }).flatten();
	auto bottom_right = bottom_left + 1;
	auto upper_tris = torch::vstack({ top_left, bottom_left, top_right }).transpose(0, 1);
	auto lower_tris = torch::vstack({ top_right, bottom_left, bottom_right }).transpose(0, 1);
	auto tris = torch::cat({ upper_tris, lower_tris });
	return tris;
}


torch::Tensor uv::gridEdges(int length) {
	using namespace torch::indexing;
	auto indices = torch::arange(0, pow(length, 2)).view({ length, length });
	auto h_starts = indices.index({Slice(), Slice(0, length - 1) }).flatten();
	auto h_ends = h_starts + 1;
	auto v_starts = indices.index({ Slice(0, length - 1) }).flatten();
	auto v_ends = v_starts + length;
	auto v_edges = torch::vstack({ v_starts, v_ends }).transpose(0, 1);
	auto h_edges = torch::vstack({ h_starts, h_ends }).transpose(0, 1);
	auto edges = torch::cat({ v_edges, h_edges });
	return edges;
}
