#pragma once
#include <torch/torch.h>

namespace shapes {
	namespace uv {
		// generates (length^2 x 2) grid points in 2D
		// if embed3d is true the grid is embedded in 3D (length^2 x 3)
		torch::Tensor uniformGrid(int length, float min = 0., float max = 1., bool embed3d = false);

		// generates face indices for a grid generated by uniformGrid of shape ((length-1)**2 * 2 = number of triangles x 3)
		torch::Tensor gridFaces(int length);

		// generates quad edge indices for a grid generated by uniformGrid of shape (number of edges x 2)
		torch::Tensor gridEdges(int length);
	}
}