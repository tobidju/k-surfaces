#pragma once
#include <torch/torch.h>

namespace shapes {
	namespace surface {

		class TenPrdSurf {
		public:
			TenPrdSurf(int degree, int ctrl_length) : degree{ degree }, ctrl_length{ ctrl_length } { };

			// calculates surf pts ((s x) n x 3) given ctrl_pts tensor(s) ((s x) ctrl_len x ctrl_len x 3) and uv tensor of shape ((s x) n x 2)
			torch::Tensor eval(torch::Tensor ctrl_grids, torch::Tensor uv);
			torch::Tensor basis(torch::Tensor u, int derivative = 0);
			torch::Tensor monomials(torch::Tensor t, int derivative = 0);
			virtual torch::Tensor basis_matrix(const torch::Device& device) = 0;
			torch::Tensor basis_uv(torch::Tensor uv);
			torch::Tensor jacobian(torch::Tensor ctrl_grids, torch::Tensor uv);
			torch::Tensor hessian(torch::Tensor ctrl_grids, torch::Tensor uv);
			torch::Tensor normal(torch::Tensor ctrl_grids, torch::Tensor uv);
			void fundamental_I(torch::Tensor& ctrl_grids, torch::Tensor& uv, torch::Tensor& E_out, torch::Tensor& F_out, torch::Tensor& G_out);
			void fundamental_II(torch::Tensor& ctrl_grids, torch::Tensor& uv, torch::Tensor& L_out, torch::Tensor& M_out, torch::Tensor& N_out);
			torch::Tensor gaussian_curvature(torch::Tensor ctrl_grids, torch::Tensor uv);
			torch::Tensor mean_curvature(torch::Tensor ctrl_grids, torch::Tensor uv);

		protected:
			int degree;
			int ctrl_length;
		};

	}
}
