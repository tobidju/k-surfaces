#include "TenPrdSurf.h"

using namespace shapes::surface;
using namespace torch::indexing;

torch::Tensor TenPrdSurf::eval(torch::Tensor ctrl_pts, torch::Tensor uv) {
	torch::Tensor surf;
	torch::Tensor basis_u = basis(uv.index({Ellipsis, 0 }));
	torch::Tensor basis_v = basis(uv.index({Ellipsis, 1 }));

	torch::Tensor ctrl_grids = ctrl_pts.view({ -1, ctrl_length, ctrl_length, 3 });
	if (uv.dim() == 2) {
		surf = torch::einsum("nu,suvd,nv->snd", { basis_u, ctrl_grids, basis_v });
	}
	else {
		surf = torch::einsum("snu,suvd,snv->snd", { basis_u, ctrl_grids, basis_v });
	}
	if (ctrl_pts.dim() == 2) {
		surf.squeeze_(0);
	}

	return surf;
}


torch::Tensor TenPrdSurf::monomials(torch::Tensor t, int derivative) {
	auto device = t.device();

	auto size = t.sizes();
	std::vector<int64_t> new_shape_list(size.begin(), size.end());
	new_shape_list.push_back(degree);
	c10::IntArrayRef new_shape(new_shape_list);

	auto t_basis = t.unsqueeze(-1).expand(new_shape);
	auto t_exp = torch::cat({ torch::zeros(derivative, device), torch::arange(0, degree - derivative, device) });

	auto monoms = torch::pow(t_basis, t_exp);
	for (int d = 0; d < derivative; d++) {
		monoms = monoms * (torch::arange(0, degree, device) - d);
	}
	return monoms;
}



torch::Tensor TenPrdSurf::basis(torch::Tensor t, int derivative) {
	auto monoms = monomials(t, derivative);
	auto bmat = basis_matrix(t.device());
	auto b = torch::einsum("...v,uv->...u", { monoms, bmat });
	return b;
}


torch::Tensor TenPrdSurf::basis_uv(torch::Tensor uv) {
	auto basis_u = basis(uv.index({ Slice(), 0 }));
	auto basis_v = basis(uv.index({ Slice(), 1 }));
	return torch::einsum("nu,nv->nuv", { basis_u, basis_v });
}


torch::Tensor TenPrdSurf::jacobian(torch::Tensor ctrl_grids, torch::Tensor uv) {
	using namespace torch::indexing;
	auto basis_u = basis(uv.index({ Ellipsis, 0 }));
	auto basis_v = basis(uv.index({ Ellipsis, 1 }));
	auto b_grad_u = basis(uv.index({ Ellipsis, 0 }), 1);
	auto b_grad_v = basis(uv.index({ Ellipsis, 1 }), 1);

	auto grad_u = torch::einsum("snu,suvd,snv->snd", { b_grad_u, ctrl_grids, basis_v });
	auto grad_v = torch::einsum("snu,suvd,snv->snd", { basis_u, ctrl_grids, b_grad_v });
	return torch::stack({ grad_u, grad_v }, 3);
}


torch::Tensor TenPrdSurf::hessian(torch::Tensor ctrl_grids, torch::Tensor uv) {
	using namespace torch::indexing;
	auto basis_u = basis(uv.index({ Ellipsis, 0 }));
	auto basis_v = basis(uv.index({ Ellipsis, 1 }));
	auto b_r_u = basis(uv.index({ Ellipsis, 0 }), 1);
	auto b_r_v = basis(uv.index({ Ellipsis, 1 }), 1);
	auto b_r_vv = basis(uv.index({ Ellipsis, 1 }), 2);
	auto b_r_uu = basis(uv.index({ Ellipsis, 1 }), 2);

	auto r_uu = torch::einsum("snu,suvd,snv->snd", { b_r_uu, ctrl_grids, basis_v });
	auto r_vv = torch::einsum("snu,suvd,snv->snd", { basis_u, ctrl_grids, b_r_vv });
	auto r_uv = torch::einsum("snu,suvd,snv->snd", { b_r_u, ctrl_grids, b_r_v });

	auto hes = torch::stack({ r_uu, r_uv, r_uv, r_vv }, 2);
	int n = uv.size(1);
	return hes.view({ -1, n, 2, 2, 3 }).permute({ 0, 1, 4, 2, 3 });
}



torch::Tensor TenPrdSurf::normal(torch::Tensor ctrl_grids, torch::Tensor uv) {
	using namespace torch::indexing;
	auto ctrls = ctrl_grids.view({ -1, degree, degree, 3 });
	auto jacobians = jacobian(ctrls, uv);
	auto norm = torch::cross(jacobians.index({ Ellipsis, 0 }), jacobians.index({ Ellipsis, 1 }), 2);
	norm = norm / torch::norm(norm, 2, 2, true);
	return norm;
}


void TenPrdSurf::fundamental_I(torch::Tensor& ctrl_grids, torch::Tensor& uv, torch::Tensor& E_out, torch::Tensor& F_out, torch::Tensor& G_out) {
	using namespace torch::indexing;
	auto jacobians = jacobian(ctrl_grids, uv);
	auto EG = jacobians.pow(2).sum(2);
	auto F = torch::einsum("snd,snd->sn", { jacobians.index({Ellipsis, 0}), jacobians.index({Ellipsis, 1}) });
	E_out = EG.index({ Ellipsis, 0 });
	F_out = F;
	G_out = EG.index({ Ellipsis, 1 });
}



void TenPrdSurf::fundamental_II(torch::Tensor& ctrl_grids, torch::Tensor& uv, torch::Tensor& L_out, torch::Tensor& M_out, torch::Tensor& N_out) {
	using namespace torch::indexing;
	auto normals = normal(ctrl_grids, uv);

	auto u = uv.index({ Ellipsis, 0 });
	auto v = uv.index({ Ellipsis, 1 });
	auto basis_u = basis(u);
	auto basis_v = basis(v);
	auto b_r_u = basis(u, 1);
	auto b_r_v = basis(v, 1);
	auto b_r_vv = basis(v, 2);
	auto b_r_uu = basis(u, 2);

	auto r_uu = torch::einsum("snu,suvd,snv->snd", { b_r_uu, ctrl_grids, basis_v });
	auto r_vv = torch::einsum("snu,suvd,snv->snd", { basis_u, ctrl_grids, b_r_vv });
	auto r_uv = torch::einsum("snu,suvd,snv->snd", { b_r_u, ctrl_grids, b_r_v });
	L_out = torch::einsum("snd,snd->sn", { r_uu, normals });
	M_out = torch::einsum("snd,snd->sn", { r_uv, normals });
	N_out = torch::einsum("snd,snd->sn", { r_vv, normals });
}


torch::Tensor TenPrdSurf::gaussian_curvature(torch::Tensor ctrl_grids, torch::Tensor uv) {
	torch::Tensor E, F, G, L, M, N;
	fundamental_I(ctrl_grids, uv, E, F, G);
	fundamental_II(ctrl_grids, uv, L, M, N);
	auto K = (L * N - M.pow(2)) / (E * G - F.pow(2));
	return K;
}


torch::Tensor TenPrdSurf::mean_curvature(torch::Tensor ctrl_grids, torch::Tensor uv) {
	torch::Tensor E, F, G, L, M, N;
	fundamental_I(ctrl_grids, uv, E, F, G);
	fundamental_II(ctrl_grids, uv, L, M, N);
	auto K = (L * G - 2 * M * F + N * E) / (2 * (E * G - F.pow(2)));
	return -K; // maybe sign error?
}
