#pragma once
#include <torch/torch.h>
#include "TenPrdSurf.h"
#include "../curve/BezierCurv.h"

// see surface.py for documentation
// the main difference is that all methods take a single ctrl grid or pts instead of ctrl grids of multiple surfaces as input
// e.g. there is no "surfaces to evaluate" dimension in all inputs

namespace shapes {
	namespace surface {
		class BezierSurf : public TenPrdSurf {
		public:
			BezierSurf(int degree) : TenPrdSurf(degree, degree), curve{degree} { };

			torch::Tensor basis_matrix(const torch::Device& device) override;
			torch::Tensor curves(torch::Tensor ctrl_grids, torch::Tensor ts, bool t_is_u = true ); // t_is_u determines whether t is used as u or v parameter
			torch::Tensor interpolate_at_uv(torch::Tensor ctrl_grids, torch::Tensor uv);

			shapes::curve::BezierCurv curve;
		};
	}
}
