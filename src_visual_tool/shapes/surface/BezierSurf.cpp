#include "BezierSurf.h"

using namespace shapes::surface;

torch::Tensor BezierSurf::basis_matrix(const torch::Device& device) {

	torch::Tensor bernstein_matrix;
	if (degree == 2) {
		bernstein_matrix = torch::tensor({
			{1., -1.},
			{0., 1.}
			}, device);

	} else if (degree == 3) {
		bernstein_matrix = torch::tensor({
			{1., -2., 1.},
			{0., 2., -2.},
			{0., 0., 1.},
			}, device);

	} else if (degree == 4) {
		bernstein_matrix = torch::tensor({
			{1., -3., 3., -1.},
			{0., 3., -6., 3.},
			{0., 0., 3., -3.},
			{0., 0., 0., 1.}
			}, device);

	} else if (degree == 5) {
		bernstein_matrix = torch::tensor({
			{1., -4., 6., -4., 1.},
			{0., 4., -12., 12., -4.},
			{0., 0., 6., -12., 6.},
			{0., 0., 0., 4., -4.},
			{0., 0., 0., 0., 1.}
			}, device);

	} else if (degree == 6) {
		bernstein_matrix = torch::tensor({
			{1., -5., 10., -10., 5., -1.},
			{0., 5., -20., 30., -20., 5.},
			{0., 0., 10., -30., 30., -10.},
			{0., 0., 0., 10., -20., 10.},
			{0., 0., 0., 0., 5., -5.},
			{0., 0., 0., 0., 0., 1.}
			}, device);
	}

	return bernstein_matrix;
}


torch::Tensor BezierSurf::curves(torch::Tensor ctrl_grids, torch::Tensor ts, bool t_is_u) {
	torch::Tensor ctrls;
	torch::Tensor t = ts.repeat_interleave(degree, 0);

	if (!t_is_u) { // v is given
		ctrls = ctrl_grids.flatten(0,1);
	}
	else { // u is given
		ctrls = ctrl_grids.transpose(1, 2).flatten(0,1);
	}

	torch::Tensor curv_pts = curve.eval(ctrls, t);
	curv_pts = curv_pts.view({ ctrl_grids.size(0),3,-1,3 }).transpose(1, 2);
	return curv_pts;
}


torch::Tensor BezierSurf::interpolate_at_uv(torch::Tensor ctrl_grids, torch::Tensor uv) {
	using namespace torch::indexing;
	auto p = ctrl_grids.index({ Slice(), 1, 1 }).clone();
	auto new_ctrls = ctrl_grids.clone();

	new_ctrls.index_put_({ Slice(), 1, 1 }, 0.);
	auto b = eval(new_ctrls, uv.unsqueeze(1)).squeeze(1);

	auto u = uv.index({ Ellipsis, 0 });
	auto v = uv.index({ Ellipsis, 1 });
	new_ctrls.index_put_({ Slice(), 1, 1 }, (p - b) / (4 * u * (1 - u) * v * (1 - v)).unsqueeze(1));
	return new_ctrls;
}
