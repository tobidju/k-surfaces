if(TARGET igl::core)
    return()
endif()

include(FetchContent)
FetchContent_Declare(
    libigl
    GIT_REPOSITORY https://github.com/libigl/libigl.git
    GIT_TAG 64c2740 # commit from 25.09.2023
)
FetchContent_MakeAvailable(libigl)